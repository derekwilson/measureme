#Measure Me

MeasureMe is a low friction personal measurement tool. It can be used up easily create a diary of subjective measurements such as pain, energy and sleep quality. 

MeasureMeAndroid - A native java app for Android devices

##AndroidStudio

This project has been migrated to AndroidStudio and Gradle, the project is in a folder called MeasureMeAndroidStudio

You will need to download AndroidStudio from here

http://developer.android.com/sdk/index.html

You will also need the Java SE JDK

http://www.oracle.com/technetwork/java/javase/downloads/jdk7-downloads-1880260.html

###Running the tests

Type 

```gradlew test```

or

```gradlew build```

Test reports are in .\roboelectric-tests\build\reports\tests\index.html. Tests can also be run from AndroidStudio

###Building for release

```gradlew assembleRelease```

When building a release version then you will be prompted for the keystore password - if you dont know it then you cannot build.

##Eclipse

The old (up to v1.0.7) versions were an eclipse project you will need to install ADT bundle from here

The old eclipse project is in a folder called MeasureMeAndroid

http://developer.android.com/sdk/index.html

You will also need the Java SE JDK

http://www.oracle.com/technetwork/java/javase/downloads/jdk7-downloads-1880260.html

ANT is used to control the build configuration you can get ANT from here

https://ant.apache.org/bindownload.cgi

To switch configurations type (this will rewrite the templated files only)

```ant config_dev```
or
```ant config_prod``` 

To perform the builds type

```ant dev```
or 
```ant prod```

(for prod builds you will need to supply your own keystore details in ant.properties)

To run the unit tests type

```ant test-report-junit```

Test reports are in ./test-report-html/index.html

The continuous integration build is

```ant ci```

This will clean the app, build the app, clean the tests and run the tests

Projects can also be compiled and run in Eclipse

Into your workspace you will need to import

```
.\AndroidSupport\support\v7\appcompat
.\MeasureMeAndroid
.\MeasureMeAndroid\tests
```
Run MeasureMe as an android app

Run MeasureMeTests as a Junit test project - you can use the Eclipse test runner

Note you appear to need to clean when swapping from Eclipe to Ant and back

To Build for Release

Start a command window and enter

```
cd C:\Data\Code\MeasureMe\MeasureMeAndroid
setenv.cmd
ant ci
copybuild.cmd
cd ..\AndroidSupport\deploy
sign.cmd
```
(to do the signing you will need the private keystore and password)

 