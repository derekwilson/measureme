package net.derekwilson.measureme.logging;

import org.slf4j.Logger;

public interface ILoggerFactory {
	Logger getCurrentApplicationLogger();
}
