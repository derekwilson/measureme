package net.derekwilson.measureme.data.repository;

import java.util.ArrayList;
import java.util.List;

import com.google.inject.Inject;

import net.derekwilson.measureme.data.interfaces.IAlarmMeasurementRepository;
import net.derekwilson.measureme.data.interfaces.IAlarmRepository;
import net.derekwilson.measureme.data.schema.AlarmTable;
import net.derekwilson.measureme.model.Alarm;
import net.derekwilson.measureme.model.Interval;
import net.derekwilson.measureme.model.Measurement;
import net.derekwilson.measureme.utility.ISystemTime;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

public class AlarmRepository extends BaseRepository implements IAlarmRepository {
	
	@Inject
    private ISystemTime systemTime;

	@Inject
    private IAlarmMeasurementRepository alarmMeasurementRepo;

	private String[] allColumns = {
			AlarmTable.COLUMN_ID,
			AlarmTable.COLUMN_START,
			AlarmTable.COLUMN_INTERVAL,
			AlarmTable.COLUMN_INTERVAL_STRIDE,
			AlarmTable.COLUMN_DAYMASK,
			AlarmTable.COLUMN_HOURMASK,
			AlarmTable.COLUMN_ACTIVE,
            AlarmTable.COLUMN_NAME,
			};

	@Inject
	public AlarmRepository(Context context) {
		super(context);
	}

	public Alarm create(Alarm newItem) {
		ContentValues values = alarmToContentValues(newItem);
		
		// TODO need a transaction
		long insertId = getDatabase().insert(AlarmTable.TABLE_ALARM, null,values);
		for(Measurement measure : newItem.getMeasurements()){
			alarmMeasurementRepo.create(insertId, measure.getId());
		}

		_logger.getCurrentApplicationLogger().debug("Alarms created with id {}.", insertId);
		return(getById(insertId));
	}

	public Alarm update(Alarm item) {
		ContentValues values = alarmToContentValues(item);
		_logger.getCurrentApplicationLogger().debug("Alarms updated with id {}.", item.getId());
		getDatabase().update(AlarmTable.TABLE_ALARM, values, AlarmTable.COLUMN_ID + " = " + item.getId(), null);
		return(getById(item.getId()));
	}

	@Override
	public Alarm updateWithMeasurement(Alarm item) {
		// TODO need a transaction
		alarmMeasurementRepo.deleteByAlarmId(item.getId());
		ContentValues values = alarmToContentValues(item);
		_logger.getCurrentApplicationLogger().debug("Alarms updated with id {}.", item.getId());
		getDatabase().update(AlarmTable.TABLE_ALARM, values, AlarmTable.COLUMN_ID + " = " + item.getId(), null);
		for(Measurement measure : item.getMeasurements()){
			alarmMeasurementRepo.create(item.getId(), measure.getId());
		}
		return(getById(item.getId()));
	}

	public void delete(Alarm item) {
		// TODO need a transaction
		alarmMeasurementRepo.deleteByAlarmId(item.getId());
		deleteById(item.getId());
	}

	public void deleteById(long id) {
		_logger.getCurrentApplicationLogger().debug("Alarms deleted with id {}.", id);

		// TODO need a transaction
		alarmMeasurementRepo.deleteByAlarmId(id);
		getDatabase().delete(AlarmTable.TABLE_ALARM, AlarmTable.COLUMN_ID + " = " + id, null);
	}

	public void deleteAll() {
		_logger.getCurrentApplicationLogger().debug("Delete all alarms.");

		alarmMeasurementRepo.deleteAll();
		getDatabase().delete(AlarmTable.TABLE_ALARM, null, null);
	}

	@Override
	public Alarm getById(long id) {
		// the default load is shallow - without the scale values
		return getById(id,false);
	}

	public Alarm getById(long id, boolean getScale) {
		Alarm alarm = null;
		Cursor cursor = getDatabase().query(AlarmTable.TABLE_ALARM, allColumns, AlarmTable.COLUMN_ID + " = " + id, null, null, null, null);
		try {
			cursor.moveToFirst();
			alarm = cursorToAlarm(cursor);
		}
		finally {
			if (cursor != null) {
				cursor.close();
			}
		}
		alarm.setMeasurements(alarmMeasurementRepo.getByAlarmId(alarm.getId(), getScale));
		return alarm;
	}

	@Override
	public List<Alarm> getAll() {
		// the default load is shallow - without the scale values
		return getAll(false);
	}

	// can only use this as long as there is only a small number of rows in the table
	public List<Alarm> getAll(boolean getScales) {
		List<Alarm> alarms = new ArrayList<Alarm>();

		Cursor cursor = getDatabase().query(AlarmTable.TABLE_ALARM, allColumns, null, null, null, null, null);

		try {
			cursor.moveToFirst();
			while (!cursor.isAfterLast()) {
				Alarm alarm = cursorToAlarm(cursor);
				// do not get the scale values when we are getting a collection of alarms
				List<Measurement> measures = alarmMeasurementRepo.getByAlarmId(alarm.getId(), getScales);
				if (measures.size() > 0) {
					// Don't add the alarm if we cannot find the measure
					alarm.setMeasurements(measures);
					alarms.add(alarm);
				}
				else {
					_logger.getCurrentApplicationLogger().warn("Cannot find the measures for an alarm id {}.", alarm.getId());
				}
				cursor.moveToNext();
			}
		}
		finally {
			// make sure to close the cursor
			if (cursor != null) {
				cursor.close();
			}
		}
		
		return alarms;
	}

	private ContentValues alarmToContentValues(Alarm alarm) {
		ContentValues values = new ContentValues();
		values.put(AlarmTable.COLUMN_START, alarm.getStartTimeInMillis());
		values.put(AlarmTable.COLUMN_INTERVAL, alarm.getInterval().getValue());
		values.put(AlarmTable.COLUMN_INTERVAL_STRIDE, alarm.getInterval().getStride());
		values.put(AlarmTable.COLUMN_DAYMASK, alarm.getDayMask());
		values.put(AlarmTable.COLUMN_HOURMASK, alarm.getHourMask());
		values.put(AlarmTable.COLUMN_ACTIVE, (alarm.getActive() ? 1 : 0));
        values.put(AlarmTable.COLUMN_NAME, alarm.getName());
		return values;
	}
	
	private Alarm cursorToAlarm(Cursor cursor) {
		Alarm alarm = new Alarm(systemTime.getCurrentTime());
		alarm.setId(cursor.getLong((cursor.getColumnIndex(AlarmTable.COLUMN_ID))));
		alarm.setStartTimeFromLong(cursor.getLong((cursor.getColumnIndex(AlarmTable.COLUMN_START))));
		int interval = cursor.getInt((cursor.getColumnIndex(AlarmTable.COLUMN_INTERVAL)));
		int intervalStride = cursor.getInt((cursor.getColumnIndex(AlarmTable.COLUMN_INTERVAL_STRIDE)));
		alarm.setInterval(new Interval(interval,intervalStride));
		alarm.setDayMask(cursor.getLong((cursor.getColumnIndex(AlarmTable.COLUMN_DAYMASK))));
		alarm.setHourMask(cursor.getLong((cursor.getColumnIndex(AlarmTable.COLUMN_HOURMASK))));
		alarm.setActive((cursor.getInt((cursor.getColumnIndex(AlarmTable.COLUMN_ACTIVE))) == 0 ? false : true));
        alarm.setName(cursor.getString(cursor.getColumnIndex(AlarmTable.COLUMN_NAME)));
		return alarm;
	}
}

