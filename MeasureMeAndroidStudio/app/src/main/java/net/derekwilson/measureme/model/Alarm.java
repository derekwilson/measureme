package net.derekwilson.measureme.model;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import net.derekwilson.measureme.model.Measurement.RenderType;

public class Alarm {

	private final long MILLISECONDS_IN_A_YEAR =  (1000 * 60 * 60 * 24) * 365;
	
	private long id;
    private String name;
	private Calendar starttime;
	private Interval interval;
	private boolean active;
	private List<Measurement> measurements;
	private long daymask;
	private long hourmask;

	public Alarm(Calendar currentTime) {
		this.starttime = currentTime;
		this.starttime.set(Calendar.MILLISECOND, 0);
		this.starttime.set(Calendar.SECOND, 0);
		this.starttime.set(Calendar.MINUTE, 0);
		measurements = new ArrayList<Measurement>();
		this.interval = new Interval(1,Interval.STRIDE_DAY);
		this.active = true;
		this.daymask = 0x07F;			// allow every day
		//this.hourmask = 0x0FFFFFF;		// allow every hour
		this.hourmask = 0x07FFFC0;		// allow 6am to 11pm
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Calendar getStartTime() {
		return starttime;
	}

	public long getStartTimeInMillis() {
		return starttime.getTimeInMillis();
	}

	public String getStartTimeFormatted() {
		SimpleDateFormat format = new SimpleDateFormat("EEE d MMM 'at' HH:mm");
		return format.format(starttime.getTime());
	}

	public void setStartTime(Calendar starttime) {
		this.starttime = starttime;
	}

	public void setStartTimeFromLong(long milliseconds) {
		this.starttime.setTimeInMillis(milliseconds);
	}
	
	public Interval getInterval() {
		return interval;
	}

	public void setInterval(Interval interval) {
		this.interval = interval;
	}

	public boolean getActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public List<Measurement> getMeasurements() {
		return measurements;
	}

	public void setMeasurements(List<Measurement> measurements) {
		this.measurements = measurements;
	}

	public boolean hasMultipleMeasurements() {
		if (this.measurements == null) {
			return false;
		}
		return this.measurements.size() > 1;
	}

	public boolean hasSliderMeasurement() {
		for(Measurement measure : this.measurements) {
			if (measure.getRenderType() == RenderType.Slider4 || measure.getRenderType() == RenderType.Slider7) {
				return true;
			}
		}
		return false;
	}
	
	public boolean needsMultipleInputs() {
		return hasMultipleMeasurements() || hasSliderMeasurement();
	}
	
	public String getMeasurementsAsValueString() {
		boolean first = true;
		StringBuilder retValue = new StringBuilder();

		for(Measurement measure : this.measurements) {
			if (first) {
				first = false;
			}
			else {
				retValue.append(",");
			}
			retValue.append(measure.getId());
		}
		return retValue.toString();
	}
	
	public String getMeasurementsNames() {
		boolean first = true;
		StringBuilder retValue = new StringBuilder();

		for(Measurement measure : this.measurements) {
			if (first) {
				first = false;
			}
			else {
				retValue.append(", ");
			}
			retValue.append(measure.getName());
		}
		return retValue.toString();
	}
	
	public long getIntervalMilliseconds() {
		return this.interval.getMilliseconds();
	}

	public boolean isTimeMaskedOut(long timeInMillisecinds) {
		Calendar thisTime = Calendar.getInstance();
		thisTime.setTimeInMillis(timeInMillisecinds);
		int day = thisTime.get(Calendar.DAY_OF_WEEK);
		if (!_getMaskAsValueBool(daymask,day-1)) {
			return true;
		}
		int hour = thisTime.get(Calendar.HOUR_OF_DAY);
		if (!_getMaskAsValueBool(hourmask,hour-1)) {
			return true;
		}
		return false;
	}
	
	public long getNextTriggerMilliseconds(Calendar currentTime) {
		long returnValue = getStartTimeInMillis();

		if (getIntervalMilliseconds() < 0) {
			// the algorithm needs a positive value here
			return -1;
		}
		
		// in the past
		if (returnValue < currentTime.getTimeInMillis()) {
			// find the next time after the current time
			while (returnValue < currentTime.getTimeInMillis()) {
				returnValue += getIntervalMilliseconds();
			}
		}

		// now we need to step over any masked out days or hours
		if (daymask == 0 || hourmask == 0) {
			// the alarm is effectively disabled
			return -1;
		}

		while (isTimeMaskedOut(returnValue)) {
			if (returnValue - currentTime.getTimeInMillis() > MILLISECONDS_IN_A_YEAR) {
				// if we cannot find a fire time in a year then stop
				return -1;
			}
			// stride on
			returnValue += getIntervalMilliseconds();
		}
		
		return returnValue;
	}

    public boolean isManualAlarm(Calendar currentTime) {
        return getNextTriggerMilliseconds(currentTime) == -1;
    }

    public boolean hasName() {
        return (getName() != null && !getName().isEmpty());
    }

	public String getNextTriggerTimeFormatted(Calendar currentTime) {
		long trigger = getNextTriggerMilliseconds(currentTime);
		if (trigger == -1) {
			return "";
		}
    	Date triggerTime = new Date(trigger);
		SimpleDateFormat format = new SimpleDateFormat("EEE d MMM 'at' HH:mm");
		return format.format(triggerTime);
	}

	public long getDayMask() {
		return daymask;
	}

	public void setDayMask(long daymask) {
		this.daymask = daymask;
	}

	private static boolean _getMaskAsValueBool(long maskValue, int bit) {
		if (bit < 0) {
			return false;
		}

		long mask = 0x01;
		mask = mask << bit;
		
		return (maskValue & mask) != 0x00;
	}
	
	private static String _getMaskAsValueString(long maskValue, String[] values) {
		long mask = 0x01;
		boolean first = true;
		StringBuilder retValue = new StringBuilder();
		for (int position=0; position<values.length; position++) {
			if ((mask & maskValue) != 0x00) {
				if (first) {
					first = false;
				}
				else {
					retValue.append(",");
				}
				retValue.append(values[position]);
			}
			mask = mask << 1;
		}
		return retValue.toString();
	}

	private static long _setMaskFromSelectedArray(boolean[] selected) {
		long newMaskValue = 0x00;
		for (int index = 0; index < selected.length; index++) { 
			if (selected[index]) {
				// set the bit correct bit value
				long setBit = 0x01 << index;
				newMaskValue = newMaskValue | setBit; 
			}
		}
		return newMaskValue;
	}	

	public String getDayMaskAsValueString(String[] values) {
		return _getMaskAsValueString(daymask,values);
	}

	public void setDayMaskFromSelectedArray(boolean[] selected) {
		daymask = _setMaskFromSelectedArray(selected);
	}	

	public long getHourMask() {
		return hourmask;
	}

	public void setHourMask(long hourmask) {
		this.hourmask = hourmask;
	}

	public String getHourMaskAsValueString(String[] values) {
		return _getMaskAsValueString(hourmask,values);
	}

	public void setHourMaskFromSelectedArray(boolean[] selected) {
		hourmask = _setMaskFromSelectedArray(selected);
	}	

	// Will be used by the ArrayAdapter in the ListView
	@Override
	public String toString() {
		SimpleDateFormat format = new SimpleDateFormat("dd/MMM/yyyy 'at' HH:mm");
		return format.format(this.starttime.getTime());
	}

}
