package net.derekwilson.measureme.data.schema;

import android.database.sqlite.SQLiteDatabase;

public class MeasurementScaleTable {
	// Database table
	public static final String TABLE_MEASUREMENT_SCALE = "measurementScale";
	public static final String COLUMN_ID = "_id";
	public static final String COLUMN_MEASUREMENT_ID = "measurementId";
	public static final String COLUMN_DISPLAY = "displayText";
	public static final String COLUMN_VALUE = "value";
	public static final String COLUMN_ORDER = "displayOrder";
	public static final String COLUMN_CREATED = "created";


	// Database creation SQL statement
	private static final String DATABASE_CREATE = "create table " 
		+ TABLE_MEASUREMENT_SCALE
		+ "(" 
		+ COLUMN_ID + " integer primary key autoincrement, " 
		+ COLUMN_MEASUREMENT_ID + " integer not null CONSTRAINT fk_measurmentScale_measurment REFERENCES " + MeasurementTable.TABLE_MEASUREMENT + "(" + MeasurementTable.COLUMN_ID  + "), " 
		+ COLUMN_DISPLAY + " varchar(255) not null," 
		+ COLUMN_VALUE + " int not null," 
		+ COLUMN_ORDER + " int not null," 
		+ COLUMN_CREATED + " DATETIME DEFAULT CURRENT_TIMESTAMP"
		+ ");";

	public static void onCreate(SQLiteDatabase database) {
		database.execSQL(DATABASE_CREATE);
	}

	public static void onUpgrade(SQLiteDatabase database, int oldVersion,int newVersion) {
		// schema changes go here
	}	

	public static void onInitialiseData(SQLiteDatabase database) {
		// pain
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(1,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.PAIN_MEASURE +"')," +
						"'No pain',0,0)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE + 
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(2,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.PAIN_MEASURE +"')," +
						"'Mild pain I can cope with',1,1)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE + 
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(3,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.PAIN_MEASURE +"')," +
						"'Moderate pain that affects my life',2,2)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE + 
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(4,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.PAIN_MEASURE +"')," +
						"'Severe disabling pain',3,3)");
		
		// energy
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(5,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.ENERGY_MEASURE +"')," +
						"'Sleepy / Sluggish',0,0)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(6,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.ENERGY_MEASURE +"')," +
						"'Just right',1,1)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(7,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.ENERGY_MEASURE +"')," +
						"'Wired but coping',2,2)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(8,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.ENERGY_MEASURE +"')," +
						"'Overwhelmed',3,3)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(9,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.ENERGY_MEASURE +"')," +
						"'Shut down',4,4)");

		// falling asleep
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(10,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.FALLING_ASLEEP_MEASURE +"')," +
						"'Immediate',0,0)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(11,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.FALLING_ASLEEP_MEASURE +"')," +
						"'Within 20 Minutes',1,1)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(12,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.FALLING_ASLEEP_MEASURE +"')," +
						"'Up to an hour',2,2)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(13,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.FALLING_ASLEEP_MEASURE +"')," +
						"'Over an hour',3,3)");

		// sleep
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(14,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.SLEEP_MEASURE +"')," +
						"'Continuous',0,0)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(15,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.SLEEP_MEASURE +"')," +
						"'Interrupted once',1,1)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(16,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.SLEEP_MEASURE +"')," +
						"'Interrupted twice',2,2)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(17,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.SLEEP_MEASURE +"')," +
						"'Interrupted three times',3,3)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(18,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.SLEEP_MEASURE +"')," +
						"'More interrupted',4,4)");

		// wake up
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(19,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.WAKE_UP_MEASURE +"')," +
						"'Run over by a train',0,0)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(20,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.WAKE_UP_MEASURE +"')," +
						"'Groggy slow to wake up',1,1)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(21,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.WAKE_UP_MEASURE +"')," +
						"'Rested & refreshed',2,2)");

		// pain NRS 11
		// pain
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(22,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.PAIN_MEASURE_NRS11 +"')," +
						"'0, No pain',0,0)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(23,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.PAIN_MEASURE_NRS11 +"')," +
						"'1, Mild pain, nagging',1,1)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(24,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.PAIN_MEASURE_NRS11 +"')," +
						"'2, Mild pain, nagging',2,2)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(25,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.PAIN_MEASURE_NRS11 +"')," +
						"'3, Mild pain, nagging',3,3)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(26,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.PAIN_MEASURE_NRS11 +"')," +
						"'4, Moderate pain, interferes',4,4)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(27,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.PAIN_MEASURE_NRS11 +"')," +
						"'5, Moderate pain, interferes',5,5)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(28,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.PAIN_MEASURE_NRS11 +"')," +
						"'6, Moderate pain, interferes',6,6)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(29,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.PAIN_MEASURE_NRS11 +"')," +
						"'7, Severe pain, disabling',7,7)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(30,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.PAIN_MEASURE_NRS11 +"')," +
						"'8, Severe pain, disabling',8,8)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(31,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.PAIN_MEASURE_NRS11 +"')," +
						"'9, Severe pain, disabling',9,9)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(32,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.PAIN_MEASURE_NRS11 +"')," +
						"'10, Severe pain, disabling',10,10)");

		// Units Consumed
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(33,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.UNITS_MEASURE +"')," +
						"'0',0,0)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(34,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.UNITS_MEASURE +"')," +
						"'0.5',1,1)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(35,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.UNITS_MEASURE +"')," +
						"'1',2,2)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(36,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.UNITS_MEASURE +"')," +
						"'1.5',3,3)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(37,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.UNITS_MEASURE +"')," +
						"'2',4,4)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(38,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.UNITS_MEASURE +"')," +
						"'2.5',5,5)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(39,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.UNITS_MEASURE +"')," +
						"'3',6,6)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(40,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.UNITS_MEASURE +"')," +
						"'3.5',7,7)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(41,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.UNITS_MEASURE +"')," +
						"'4',8,8)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(42,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.UNITS_MEASURE +"')," +
						"'4.5',9,9)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(43,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.UNITS_MEASURE +"')," +
						"'5',10,10)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(44,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.UNITS_MEASURE +"')," +
						"'5.5',11,11)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(45,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.UNITS_MEASURE +"')," +
						"'6',12,12)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(46,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.UNITS_MEASURE +"')," +
						"'6.5',13,13)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(47,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.UNITS_MEASURE +"')," +
						"'7',14,14)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(48,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.UNITS_MEASURE +"')," +
						"'7.5',15,15)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(49,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.UNITS_MEASURE +"')," +
						"'8',16,16)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(50,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.UNITS_MEASURE +"')," +
						"'8.5',17,17)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(51,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.UNITS_MEASURE +"')," +
						"'9',18,18)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(52,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.UNITS_MEASURE +"')," +
						"'9.5',19,19)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(53,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.UNITS_MEASURE +"')," +
						"'10',20,20)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(54,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.UNITS_MEASURE +"')," +
						"'11',22,21)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(55,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.UNITS_MEASURE +"')," +
						"'12',24,22)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(56,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.UNITS_MEASURE +"')," +
						"'13',26,23)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(57,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.UNITS_MEASURE +"')," +
						"'14',28,24)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(58,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.UNITS_MEASURE +"')," +
						"'15',30,25)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(59,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.UNITS_MEASURE +"')," +
						"'16',32,26)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(60,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.UNITS_MEASURE +"')," +
						"'17',34,27)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(61,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.UNITS_MEASURE +"')," +
						"'18',36,28)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(62,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.UNITS_MEASURE +"')," +
						"'19',38,29)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(63,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.UNITS_MEASURE +"')," +
						"'20+',40,30)");

		// SWL
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(64,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.SWL_MEASURE +"')," +
						"'In most ways my life is close to my ideal.',7,0)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(65,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.SWL_MEASURE +"')," +
						"'The conditions of my life are excellent.',7,1)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(66,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.SWL_MEASURE +"')," +
						"'I am satisfied with my life.',7,2)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(67,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.SWL_MEASURE +"')," +
						"'So far I have gotten the important things I want in life.',7,3)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(68,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.SWL_MEASURE +"')," +
						"'If I could live my life over, I would change almost nothing.',7,4)");

		// Bristol Stool Scale
		database.execSQL("INSERT or REPLACE INTO " +
				TABLE_MEASUREMENT_SCALE +
				"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
				" VALUES(69,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT +
				" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.STOOL_MEASURE_BRISTOL +"')," +
				"'None',0,0)");
		database.execSQL("INSERT or REPLACE INTO " +
				TABLE_MEASUREMENT_SCALE +
				"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
				" VALUES(70,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT +
				" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.STOOL_MEASURE_BRISTOL +"')," +
				"'Type 1: Separate hard lumps, like nuts (hard to pass)',1,1)");
		database.execSQL("INSERT or REPLACE INTO " +
				TABLE_MEASUREMENT_SCALE +
				"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
				" VALUES(71,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT +
				" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.STOOL_MEASURE_BRISTOL +"')," +
				"'Type 2: Sausage-shaped, but lumpy',2,2)");
		database.execSQL("INSERT or REPLACE INTO " +
				TABLE_MEASUREMENT_SCALE +
				"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
				" VALUES(72,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT +
				" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.STOOL_MEASURE_BRISTOL +"')," +
				"'Type 3: Like a sausage but with cracks on its surface',3,3)");
		database.execSQL("INSERT or REPLACE INTO " +
				TABLE_MEASUREMENT_SCALE +
				"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
				" VALUES(73,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT +
				" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.STOOL_MEASURE_BRISTOL +"')," +
				"'Type 4: Like a sausage or snake, smooth and soft',4,4)");
		database.execSQL("INSERT or REPLACE INTO " +
				TABLE_MEASUREMENT_SCALE +
				"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
				" VALUES(74,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT +
				" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.STOOL_MEASURE_BRISTOL +"')," +
				"'Type 5: Soft blobs with clear cut edges (passed easily)',5,5)");
		database.execSQL("INSERT or REPLACE INTO " +
				TABLE_MEASUREMENT_SCALE +
				"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
				" VALUES(75,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT +
				" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.STOOL_MEASURE_BRISTOL +"')," +
				"'Type 6: Fluffy pieces with ragged edges, a mushy stool',6,6)");
		database.execSQL("INSERT or REPLACE INTO " +
				TABLE_MEASUREMENT_SCALE +
				"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
				" VALUES(76,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT +
				" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.STOOL_MEASURE_BRISTOL +"')," +
				"'Type 7: Watery, no solid pieces, entirely liquid',7,7)");

        // Depression - Zung
        database.execSQL("INSERT or REPLACE INTO " +
                TABLE_MEASUREMENT_SCALE +
                "(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
                " VALUES(77,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT +
                " where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.DEPRESSION_MEASURE_ZUNG +"')," +
                "'I feel down hearted and blue.',4,0)");
        database.execSQL("INSERT or REPLACE INTO " +
                TABLE_MEASUREMENT_SCALE +
                "(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
                " VALUES(78,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT +
                " where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.DEPRESSION_MEASURE_ZUNG +"')," +
                "'Morning is when I feel the best.',4,1)");
        database.execSQL("INSERT or REPLACE INTO " +
                TABLE_MEASUREMENT_SCALE +
                "(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
                " VALUES(79,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT +
                " where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.DEPRESSION_MEASURE_ZUNG +"')," +
                "'I have crying spells or feel like it.',4,2)");
        database.execSQL("INSERT or REPLACE INTO " +
                TABLE_MEASUREMENT_SCALE +
                "(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
                " VALUES(80,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT +
                " where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.DEPRESSION_MEASURE_ZUNG +"')," +
                "'I have trouble sleeping at night.',4,3)");
        database.execSQL("INSERT or REPLACE INTO " +
                TABLE_MEASUREMENT_SCALE +
                "(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
                " VALUES(81,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT +
                " where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.DEPRESSION_MEASURE_ZUNG +"')," +
                "'I eat as much as I used to.',4,4)");
        database.execSQL("INSERT or REPLACE INTO " +
                TABLE_MEASUREMENT_SCALE +
                "(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
                " VALUES(82,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT +
                " where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.DEPRESSION_MEASURE_ZUNG +"')," +
                "'I still enjoy sex.',4,5)");
        database.execSQL("INSERT or REPLACE INTO " +
                TABLE_MEASUREMENT_SCALE +
                "(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
                " VALUES(83,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT +
                " where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.DEPRESSION_MEASURE_ZUNG +"')," +
                "'I notice that I am losing weight.',4,6)");
        database.execSQL("INSERT or REPLACE INTO " +
                TABLE_MEASUREMENT_SCALE +
                "(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
                " VALUES(84,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT +
                " where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.DEPRESSION_MEASURE_ZUNG +"')," +
                "'I have trouble with constipation.',4,7)");
        database.execSQL("INSERT or REPLACE INTO " +
                TABLE_MEASUREMENT_SCALE +
                "(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
                " VALUES(85,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT +
                " where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.DEPRESSION_MEASURE_ZUNG +"')," +
                "'My heart beats faster than usual.',4,8)");
        database.execSQL("INSERT or REPLACE INTO " +
                TABLE_MEASUREMENT_SCALE +
                "(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
                " VALUES(86,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT +
                " where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.DEPRESSION_MEASURE_ZUNG +"')," +
                "'I get tired for no reason.',4,9)");
        database.execSQL("INSERT or REPLACE INTO " +
                TABLE_MEASUREMENT_SCALE +
                "(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
                " VALUES(87,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT +
                " where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.DEPRESSION_MEASURE_ZUNG +"')," +
                "'My mind is as clear as it used to be.',4,10)");
        database.execSQL("INSERT or REPLACE INTO " +
                TABLE_MEASUREMENT_SCALE +
                "(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
                " VALUES(88,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT +
                " where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.DEPRESSION_MEASURE_ZUNG +"')," +
                "'I find it easy to do the things I used to.',4,11)");
        database.execSQL("INSERT or REPLACE INTO " +
                TABLE_MEASUREMENT_SCALE +
                "(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
                " VALUES(89,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT +
                " where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.DEPRESSION_MEASURE_ZUNG +"')," +
                "'I am restless and cannot keep still.',4,12)");
        database.execSQL("INSERT or REPLACE INTO " +
                TABLE_MEASUREMENT_SCALE +
                "(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
                " VALUES(90,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT +
                " where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.DEPRESSION_MEASURE_ZUNG +"')," +
                "'I feel hopeful about the future.',4,13)");
        database.execSQL("INSERT or REPLACE INTO " +
                TABLE_MEASUREMENT_SCALE +
                "(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
                " VALUES(91,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT +
                " where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.DEPRESSION_MEASURE_ZUNG +"')," +
                "'I am more irritable than usual.',4,14)");
        database.execSQL("INSERT or REPLACE INTO " +
                TABLE_MEASUREMENT_SCALE +
                "(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
                " VALUES(92,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT +
                " where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.DEPRESSION_MEASURE_ZUNG +"')," +
                "'I find it easy to make decisions.',4,15)");
        database.execSQL("INSERT or REPLACE INTO " +
                TABLE_MEASUREMENT_SCALE +
                "(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
                " VALUES(93,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT +
                " where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.DEPRESSION_MEASURE_ZUNG +"')," +
                "'I feel that I am useful and needed.',4,16)");
        database.execSQL("INSERT or REPLACE INTO " +
                TABLE_MEASUREMENT_SCALE +
                "(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
                " VALUES(94,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT +
                " where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.DEPRESSION_MEASURE_ZUNG +"')," +
                "'My life is pretty full.',4,17)");
        database.execSQL("INSERT or REPLACE INTO " +
                TABLE_MEASUREMENT_SCALE +
                "(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
                " VALUES(95,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT +
                " where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.DEPRESSION_MEASURE_ZUNG +"')," +
                "'I feel that others would be better off if I were dead.',4,18)");
        database.execSQL("INSERT or REPLACE INTO " +
                TABLE_MEASUREMENT_SCALE +
                "(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
                " VALUES(96,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT +
                " where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.DEPRESSION_MEASURE_ZUNG +"')," +
                "'I still enjoy the things I used to do.',4,19)");

        // Anxiety - Zung
        database.execSQL("INSERT or REPLACE INTO " +
                TABLE_MEASUREMENT_SCALE +
                "(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
                " VALUES(97,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT +
                " where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.ANXIETY_MEASURE_ZUNG +"')," +
                "'I feel more nervous and anxious than usual.',4,0)");
        database.execSQL("INSERT or REPLACE INTO " +
                TABLE_MEASUREMENT_SCALE +
                "(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
                " VALUES(98,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT +
                " where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.ANXIETY_MEASURE_ZUNG +"')," +
                "'I feel afraid for no reason at all.',4,1)");
        database.execSQL("INSERT or REPLACE INTO " +
                TABLE_MEASUREMENT_SCALE +
                "(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
                " VALUES(99,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT +
                " where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.ANXIETY_MEASURE_ZUNG +"')," +
                "'I get upset easily or feel panicky.',4,2)");
        database.execSQL("INSERT or REPLACE INTO " +
                TABLE_MEASUREMENT_SCALE +
                "(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
                " VALUES(100,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT +
                " where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.ANXIETY_MEASURE_ZUNG +"')," +
                "'I feel like I am falling apart and going to pieces.',4,3)");
        database.execSQL("INSERT or REPLACE INTO " +
                TABLE_MEASUREMENT_SCALE +
                "(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
                " VALUES(101,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT +
                " where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.ANXIETY_MEASURE_ZUNG +"')," +
                "'I feel that everything is all right and nothing bad will happen.',4,4)");
        database.execSQL("INSERT or REPLACE INTO " +
                TABLE_MEASUREMENT_SCALE +
                "(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
                " VALUES(102,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT +
                " where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.ANXIETY_MEASURE_ZUNG +"')," +
                "'My arms and legs shake and tremble.',4,5)");
        database.execSQL("INSERT or REPLACE INTO " +
                TABLE_MEASUREMENT_SCALE +
                "(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
                " VALUES(103,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT +
                " where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.ANXIETY_MEASURE_ZUNG +"')," +
                "'I am bothered by headaches neck and back pain.',4,6)");
        database.execSQL("INSERT or REPLACE INTO " +
                TABLE_MEASUREMENT_SCALE +
                "(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
                " VALUES(104,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT +
                " where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.ANXIETY_MEASURE_ZUNG +"')," +
                "'I feel weak and get tired easily.',4,7)");
        database.execSQL("INSERT or REPLACE INTO " +
                TABLE_MEASUREMENT_SCALE +
                "(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
                " VALUES(105,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT +
                " where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.ANXIETY_MEASURE_ZUNG +"')," +
                "'I feel calm and can sit still easily.',4,8)");
        database.execSQL("INSERT or REPLACE INTO " +
                TABLE_MEASUREMENT_SCALE +
                "(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
                " VALUES(106,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT +
                " where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.ANXIETY_MEASURE_ZUNG +"')," +
                "'I can feel my heart beating fast.',4,9)");
        database.execSQL("INSERT or REPLACE INTO " +
                TABLE_MEASUREMENT_SCALE +
                "(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
                " VALUES(107,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT +
                " where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.ANXIETY_MEASURE_ZUNG +"')," +
                "'I am bothered by dizzy spells.',4,10)");
        database.execSQL("INSERT or REPLACE INTO " +
                TABLE_MEASUREMENT_SCALE +
                "(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
                " VALUES(108,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT +
                " where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.ANXIETY_MEASURE_ZUNG +"')," +
                "'I have fainting spells or feel like it.',4,11)");
        database.execSQL("INSERT or REPLACE INTO " +
                TABLE_MEASUREMENT_SCALE +
                "(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
                " VALUES(109,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT +
                " where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.ANXIETY_MEASURE_ZUNG +"')," +
                "'I can breathe in and out easily.',4,12)");
        database.execSQL("INSERT or REPLACE INTO " +
                TABLE_MEASUREMENT_SCALE +
                "(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
                " VALUES(110,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT +
                " where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.ANXIETY_MEASURE_ZUNG +"')," +
                "'I get numbness and tingling in my fingers and toes.',4,13)");
        database.execSQL("INSERT or REPLACE INTO " +
                TABLE_MEASUREMENT_SCALE +
                "(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
                " VALUES(111,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT +
                " where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.ANXIETY_MEASURE_ZUNG +"')," +
                "'I am bothered by stomach aches or indigestion.',4,14)");
        database.execSQL("INSERT or REPLACE INTO " +
                TABLE_MEASUREMENT_SCALE +
                "(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
                " VALUES(112,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT +
                " where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.ANXIETY_MEASURE_ZUNG +"')," +
                "'I have to empty my bladder often.',4,15)");
        database.execSQL("INSERT or REPLACE INTO " +
                TABLE_MEASUREMENT_SCALE +
                "(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
                " VALUES(113,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT +
                " where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.ANXIETY_MEASURE_ZUNG +"')," +
                "'My hands are usually dry and warm.',4,16)");
        database.execSQL("INSERT or REPLACE INTO " +
                TABLE_MEASUREMENT_SCALE +
                "(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
                " VALUES(114,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT +
                " where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.ANXIETY_MEASURE_ZUNG +"')," +
                "'My face gets hot and blushes.',4,17)");
        database.execSQL("INSERT or REPLACE INTO " +
                TABLE_MEASUREMENT_SCALE +
                "(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
                " VALUES(115,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT +
                " where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.ANXIETY_MEASURE_ZUNG +"')," +
                "'I fall asleep easily and get a good nights rest.',4,18)");
        database.execSQL("INSERT or REPLACE INTO " +
                TABLE_MEASUREMENT_SCALE +
                "(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
                " VALUES(116,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT +
                " where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.ANXIETY_MEASURE_ZUNG +"')," +
                "'I have nightmares.',4,19)");

        // Blood Pressure 1 - Systolic
        database.execSQL("INSERT or REPLACE INTO " +
                TABLE_MEASUREMENT_SCALE +
                "(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
                " VALUES(117,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT +
                " where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.BLOOD_PRESSURE_1_SYSTOLIC +"')," +
                "'Less than 120',0,0)");
        database.execSQL("INSERT or REPLACE INTO " +
                TABLE_MEASUREMENT_SCALE +
                "(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
                " VALUES(118,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT +
                " where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.BLOOD_PRESSURE_1_SYSTOLIC +"')," +
                "'120 to 139',1,1)");
        database.execSQL("INSERT or REPLACE INTO " +
                TABLE_MEASUREMENT_SCALE +
                "(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
                " VALUES(119,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT +
                " where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.BLOOD_PRESSURE_1_SYSTOLIC +"')," +
                "'140 to 159',2,2)");
        database.execSQL("INSERT or REPLACE INTO " +
                TABLE_MEASUREMENT_SCALE +
                "(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
                " VALUES(120,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT +
                " where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.BLOOD_PRESSURE_1_SYSTOLIC +"')," +
                "'160 to 179',3,3)");
        database.execSQL("INSERT or REPLACE INTO " +
                TABLE_MEASUREMENT_SCALE +
                "(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
                " VALUES(121,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT +
                " where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.BLOOD_PRESSURE_1_SYSTOLIC +"')," +
                "'Higher than 180',4,4)");

        // Blood Pressure 2 - Diastolic
        database.execSQL("INSERT or REPLACE INTO " +
                TABLE_MEASUREMENT_SCALE +
                "(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
                " VALUES(122,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT +
                " where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.BLOOD_PRESSURE_2_DIASTOLIC +"')," +
                "'Less than 80',0,0)");
        database.execSQL("INSERT or REPLACE INTO " +
                TABLE_MEASUREMENT_SCALE +
                "(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
                " VALUES(123,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT +
                " where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.BLOOD_PRESSURE_2_DIASTOLIC +"')," +
                "'80 to 89',0,0)");
        database.execSQL("INSERT or REPLACE INTO " +
                TABLE_MEASUREMENT_SCALE +
                "(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
                " VALUES(124,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT +
                " where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.BLOOD_PRESSURE_2_DIASTOLIC +"')," +
                "'90 to 99',0,0)");
        database.execSQL("INSERT or REPLACE INTO " +
                TABLE_MEASUREMENT_SCALE +
                "(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
                " VALUES(125,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT +
                " where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.BLOOD_PRESSURE_2_DIASTOLIC +"')," +
                "'100 to 109',0,0)");
        database.execSQL("INSERT or REPLACE INTO " +
                TABLE_MEASUREMENT_SCALE +
                "(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
                " VALUES(126,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT +
                " where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.BLOOD_PRESSURE_2_DIASTOLIC +"')," +
                "'Higher than 110',0,0)");
    }
}
