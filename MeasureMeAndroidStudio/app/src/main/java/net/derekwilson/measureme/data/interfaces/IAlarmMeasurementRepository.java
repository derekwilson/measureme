package net.derekwilson.measureme.data.interfaces;

import java.util.List;

import net.derekwilson.measureme.model.Measurement;

public interface IAlarmMeasurementRepository {
	List<Measurement> getByAlarmId(long id, boolean getScale);
	long create(long alarmId, long measurementId);
	void deleteByAlarmId(long id);
	void deleteAll();
}
