package net.derekwilson.measureme.data.interfaces;

import net.derekwilson.measureme.model.Measurement;
import net.derekwilson.measureme.model.RecordedMeasurement;

public interface IRecordedMeasurementWriter {
	String getFileFullPathname(Measurement measure);
	boolean Write(RecordedMeasurement data);
}
