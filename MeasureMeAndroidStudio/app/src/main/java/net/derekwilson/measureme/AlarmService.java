package net.derekwilson.measureme;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import net.derekwilson.measureme.activity.main.MainActivity;
import net.derekwilson.measureme.data.interfaces.IAlarmRepository;
import net.derekwilson.measureme.data.interfaces.IRepositoryAction;
import net.derekwilson.measureme.logging.ILoggerFactory;
import net.derekwilson.measureme.model.Alarm;
import net.derekwilson.measureme.utility.ISystemTime;

import com.google.inject.Inject;

import roboguice.service.RoboIntentService;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentFilter;

public class AlarmService extends RoboIntentService {
	@Inject
    private ILoggerFactory logger;

	@Inject
    private ISystemTime systemTime;

	@Inject
    private IAlarmRepository alarmRepo;

	@Inject
	private AlarmManager alarmManager;
	
	public static final String TAG = "MeasureMe.AlarmService";
	public static final String CREATE = "CREATE";
	public static final String CANCEL = "CANCEL";
	public final Calendar calendar = Calendar.getInstance();
	
	private IntentFilter matcher;
	 
    public AlarmService() {
        super(TAG);
        matcher = new IntentFilter();
        matcher.addAction(CREATE);
        matcher.addAction(CANCEL);
    }
	 
    @Override
    protected void onHandleIntent(Intent intent) {
    	String action = intent.getAction();
        final long alarmId = intent.getLongExtra(MainActivity.ACTIVITY_PARAM_ALARM_ID, 0);

		logger.getCurrentApplicationLogger().debug("AlarmService onHandleIntent {} {}", action, alarmId);

		if (alarmId == 0) {
			// no id passed so we are going to ignore
			logger.getCurrentApplicationLogger().warn("AlarmService onHandleIntent No ID passed");
		}

		final List<Alarm> alarms = new ArrayList<Alarm>();
		alarmRepo.executeInOpenContext(new IRepositoryAction () {
			@Override public void execute() {
				if (alarmId == -1) {
					List<Alarm> loaded_alarms = alarmRepo.getAll();
					alarms.addAll(loaded_alarms);
				}
				else {
					Alarm alarm = alarmRepo.getById(alarmId);
					if (alarm != null) {
						alarms.add(alarm);
					}
				}
			}
		});

        if (CREATE.equals(action)) {
			for(Alarm thisAlarm : alarms){
				enableAlarm(thisAlarm);
			}
			BroadcastAlarmsUpdated();
    	} else if (CANCEL.equals(action)) { 
    		if (alarmId == -1) {
    			for(Alarm thisAlarm : alarms){
    				disableAlarm(thisAlarm.getId());
    			}
    		}
    		else {
    			// the alarm may not be in the DB any more
    			disableAlarm(alarmId);
    		}
    		BroadcastAlarmsUpdated();
    	}

		logger.getCurrentApplicationLogger().debug("AlarmService complete: {}",action);
    }

    private PendingIntent GetAlarmOperation(long id) {
	    Intent intent = new Intent(this, AlarmReceiver.class);
	    intent.putExtra(MainActivity.ACTIVITY_PARAM_ALARM_ID, id);
	         
	    PendingIntent operation = PendingIntent.getBroadcast(this, (int) id, intent, PendingIntent.FLAG_UPDATE_CURRENT);
	    
	    return operation;
    }
    
	private void enableAlarm(Alarm alarm) {
	    
		PendingIntent operation = GetAlarmOperation(alarm.getId());

		long trigger = alarm.getNextTriggerMilliseconds(systemTime.getCurrentTime());
		if (trigger == -1) {
			logger.getCurrentApplicationLogger().debug("AlarmService enableAlarm: {}, trigger returned -1",alarm.getId());
			disableAlarm(alarm.getId());
			return;
		}
    	long interval = alarm.getIntervalMilliseconds();
    	
		logger.getCurrentApplicationLogger().debug("AlarmService enable: {} trigger {} interval {}",alarm.getId(),alarm.getNextTriggerTimeFormatted(systemTime.getCurrentTime()),interval);

		alarmManager.set(AlarmManager.RTC_WAKEUP, trigger, operation);
		//alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, trigger, interval, operation);
	}

	private void disableAlarm(long id) {
		PendingIntent operation = GetAlarmOperation(id);

		logger.getCurrentApplicationLogger().debug("AlarmService disable: {}",id);
        alarmManager.cancel(operation);
	}
	
	private void BroadcastAlarmsUpdated() {
		logger.getCurrentApplicationLogger().debug("AlarmService BroadcastAlarmsUpdated");
		Intent broadcastIntent = new Intent();
		broadcastIntent.setAction(MainActivity.BROADCAST_ALARMS_UPDATED);
		broadcastIntent.addCategory(Intent.CATEGORY_DEFAULT);
		sendBroadcast(broadcastIntent);
	}
}
