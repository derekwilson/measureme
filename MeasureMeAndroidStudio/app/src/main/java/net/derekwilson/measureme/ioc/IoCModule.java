package net.derekwilson.measureme.ioc;

import net.derekwilson.measureme.data.file.RecordedMeasurementWriter;
import net.derekwilson.measureme.data.interfaces.IAlarmMeasurementRepository;
import net.derekwilson.measureme.data.interfaces.IAlarmRepository;
import net.derekwilson.measureme.data.interfaces.IMeasurementRepository;
import net.derekwilson.measureme.data.interfaces.IMeasurementScaleRepository;
import net.derekwilson.measureme.data.interfaces.IRecordedMeasurementWriter;
import net.derekwilson.measureme.data.repository.AlarmMeasurementRepository;
import net.derekwilson.measureme.data.repository.AlarmRepository;
import net.derekwilson.measureme.data.repository.MeasurementRepository;
import net.derekwilson.measureme.data.repository.MeasurementScaleRepository;
import net.derekwilson.measureme.logging.ILoggerFactory;
import net.derekwilson.measureme.logging.SlfLoggerFactory;
import net.derekwilson.measureme.utility.ILogSender;
import net.derekwilson.measureme.utility.IPreferencesHelper;
import net.derekwilson.measureme.utility.ISystemTime;
import net.derekwilson.measureme.utility.LogSender;
import net.derekwilson.measureme.utility.PreferencesHelper;
import net.derekwilson.measureme.utility.SystemTime;

import com.google.inject.Binder;
import com.google.inject.Module;

public class IoCModule implements Module {
    @Override
    public void configure(Binder binder) {

    	// singletons
    	binder.bind(ILoggerFactory.class).toInstance(new SlfLoggerFactory());
    	binder.bind(ISystemTime.class).toInstance(new SystemTime());

    	// reops
    	//binder.bind(new TypeLiteral<IRepository<Alarm>>() {}).to(AlarmRepository.class);
    	binder.bind(IAlarmRepository.class).to(AlarmRepository.class);
    	binder.bind(IMeasurementRepository.class).to(MeasurementRepository.class);
    	binder.bind(IMeasurementScaleRepository.class).to(MeasurementScaleRepository.class);
    	binder.bind(IAlarmMeasurementRepository.class).to(AlarmMeasurementRepository.class);
    	
    	// writers
    	binder.bind(IRecordedMeasurementWriter.class).to(RecordedMeasurementWriter.class);
    	
    	// logic
    	binder.bind(IPreferencesHelper.class).to(PreferencesHelper.class);
    	binder.bind(ILogSender.class).to(LogSender.class);
    }
}
