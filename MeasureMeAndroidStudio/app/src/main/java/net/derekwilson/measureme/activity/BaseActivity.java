package net.derekwilson.measureme.activity;

import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;

import net.derekwilson.measureme.R;
import net.derekwilson.measureme.fragment.navigation.NavigationDrawerFragment;

public abstract class BaseActivity extends ActionBarActivity {

	private Toolbar toolbar;
	protected NavigationDrawerFragment navigationDrawerFragment;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(getLayoutResource());
		toolbar = (Toolbar) findViewById(R.id.toolbar);
		if (toolbar != null) {
			setSupportActionBar(toolbar);
			getSupportActionBar().setDisplayHomeAsUpEnabled(toolbarBackButtonNeeded);
		}
	}

	protected void initNavigationDrawer(int drawerLayoutId, int fragmentId) {
		navigationDrawerFragment = (NavigationDrawerFragment) getSupportFragmentManager().findFragmentById(fragmentId);
		navigationDrawerFragment.setup(fragmentId, (DrawerLayout) findViewById(drawerLayoutId), toolbar);
	}

	protected abstract int getLayoutResource();

	protected boolean toolbarBackButtonNeeded = false;

	protected void setActionBarIcon(int iconRes) {
		toolbar.setNavigationIcon(iconRes);
	}
}
