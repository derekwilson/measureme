package net.derekwilson.measureme;

import net.derekwilson.measureme.activity.main.MainActivity;
import net.derekwilson.measureme.activity.showAlarm.ShowAlarmActivity;
import net.derekwilson.measureme.logging.ILoggerFactory;
import net.derekwilson.measureme.utility.IPreferencesHelper;

import com.google.inject.Inject;

import roboguice.receiver.RoboBroadcastReceiver;

import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;

public class AlarmReceiver extends RoboBroadcastReceiver {
	@Inject
    private ILoggerFactory logger;

	@Inject
    private IPreferencesHelper preferences;

	@Inject
	private PowerManager powerManager;

	public static final String TAG = "MeasureMe.AlarmReceiver";

    // handleReceive is the RoboGuice equivalent of onReceive 
	@Override
    public void handleReceive(Context context, Intent intent) {

		PowerManager.WakeLock lock = acquire();
		try {
			logger.getCurrentApplicationLogger().debug("AlarmReceiver handleReceive");
			long id = intent.getLongExtra(MainActivity.ACTIVITY_PARAM_ALARM_ID, 0);
			logger.getCurrentApplicationLogger().debug("AlarmReceiver handleReceive {}", id);
			resetAlarm(id);
		    showAlarm(context,id);
		}
		finally {
			if (lock != null) {
				lock.release();
				logger.getCurrentApplicationLogger().debug("AlarmReceiver release wakelock");
			}
		}

    }

	private void resetAlarm(long id) {
		// if alarms are not enabled then there is no need to set them up again as we are using one shots
		if (preferences.getAlarmsEnabled()) {
			preferences.SetupAlarmInAlarmManager(id,true);
		}
	}
	
	private void showAlarm(Context context, long id) {
		Intent showAlarmIntent = new Intent(context, ShowAlarmActivity.class);
	    showAlarmIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
	    showAlarmIntent.putExtra(MainActivity.ACTIVITY_PARAM_ALARM_ID, id);
	    context.startActivity(showAlarmIntent);
	}

	// if FULL_WAKE_LOCK does not work then we set the correct window flag (FLAG_TURN_SCREEN_ON) in the activity
	// but we will try in case we are running on an old device
	@SuppressWarnings("deprecation")
	private PowerManager.WakeLock acquire() {
		PowerManager.WakeLock wakeLock;
		wakeLock = powerManager.newWakeLock(PowerManager.FULL_WAKE_LOCK |
        									PowerManager.ACQUIRE_CAUSES_WAKEUP |
        									PowerManager.ON_AFTER_RELEASE,
        									TAG);
        wakeLock.acquire();
        return wakeLock;
    }
}