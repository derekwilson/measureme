package net.derekwilson.measureme.data.interfaces;

public interface IRepositoryAction {
	void execute();
}
