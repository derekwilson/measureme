package net.derekwilson.measureme.activity.help;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import net.derekwilson.measureme.R;
import net.derekwilson.measureme.activity.BaseActivity;
import net.derekwilson.measureme.logging.ILoggerFactory;
import android.content.Context;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.widget.TextView;

import com.google.inject.Inject;

import roboguice.RoboGuice;

public class HelpActivity extends BaseActivity {

	@Inject
    private ILoggerFactory logger;

	//@InjectView(R.id.txtHelp)
	private TextView helpTextView;

	private void HookupControls() {
		// to get the ActionBar I have to give up the InjectView until RoboGuice 3
		// Inject is OK but its just too fiddley to get the view to work
		// we can override this for testing - cheesy but gets us out of a hole

		helpTextView = (TextView)findViewById(R.id.txtHelp);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		toolbarBackButtonNeeded = true;
		super.onCreate(savedInstanceState);

		// this will inject non view dependencies
		RoboGuice.getInjector(this.getApplicationContext()).injectMembers(this);
		logger.getCurrentApplicationLogger().debug("HelpActivity.onCreate()");
		HookupControls();

		// make links clickable
		helpTextView.setMovementMethod(LinkMovementMethod.getInstance());
		getHtmlFromFile(this.getApplicationContext(),"help/help.html",helpTextView);
	}

	@Override
	protected int getLayoutResource() {
		return R.layout.activity_help;
	}

	private void getHtmlFromFile(Context context, String filename, TextView view) {

		InputStream inputStream = null;
		BufferedReader reader = null;
	    StringBuffer buf = new StringBuffer();
		try {
			inputStream = context.getAssets().open(filename);
			logger.getCurrentApplicationLogger().debug("Opened {} Available {}", filename, inputStream.available());
			reader = new BufferedReader(new InputStreamReader(inputStream));

			String str;
			if (inputStream != null) {							
				while ((str = reader.readLine()) != null) {	
					buf.append(str);
				}				
			}		
		} catch (IOException e) {
			logger.getCurrentApplicationLogger().error("Error reading {}", filename, e);
		}
		finally {
			try {
				if (reader != null) {
					reader.close();
				}
				if (inputStream != null) {
					inputStream.close();
				}
			} catch (IOException e) {
				logger.getCurrentApplicationLogger().error("Error closing {}", filename, e);
			}
		}
		
		view.setText(Html.fromHtml(buf.toString()));
	}
}
