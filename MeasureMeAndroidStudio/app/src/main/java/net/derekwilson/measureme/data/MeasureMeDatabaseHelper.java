package net.derekwilson.measureme.data;

import net.derekwilson.measureme.data.interfaces.IRepositoryAction;
import net.derekwilson.measureme.data.schema.AlarmMeasurementTable;
import net.derekwilson.measureme.data.schema.AlarmTable;
import net.derekwilson.measureme.data.schema.MeasurementScaleTable;
import net.derekwilson.measureme.data.schema.MeasurementTable;
import net.derekwilson.measureme.logging.ILoggerFactory;
import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class MeasureMeDatabaseHelper extends SQLiteOpenHelper {
	private ILoggerFactory _logger;

	protected SQLiteDatabase _database;

	public static final String DATABASE_NAME = "measureme.db";
	private static final int DATABASE_VERSION = 23;

	// Schema changes
	//
	// 	Schema Version		Release	Notes
	//
	//	10					v1.0.0	Initial public release
	//	11							Added render type column to MeasurementTable
	//	12							Fixed sleep - wake up scale column_value (all were zero)
	//	13					v1.0.2	Renamed Units of Alcohol scale to be Units Consumed as its more generic 
	//	14					v1.0.3	Corrected time to times in "Interrupted three times" in Sleep Measure
	//	15					v1.0.4	Added Satisfaction With Life scale
	//	16					v1.0.7	Fixed clash between SWL and Units Consumed in scale values
	//	17					v1.1.2	Added Bristol stool scale
    //  18                  v1.1.3  Added Zung depression scale
    //  19                          Fixed typo
    //  20                  v1.1.4  Added Zung anxiety scale
    //  21                          Fixed typo
    //  22                  v1.2.2  Added name to AlarmTable, added blood pressure scales
    //  23                          Fixed error in the BP 2 scale ids
    //  24                          Fixed syntax error in create table script

	public MeasureMeDatabaseHelper(Context context, ILoggerFactory logger) {
	    super(context, DATABASE_NAME, null, DATABASE_VERSION);
		_logger = logger;
	}

	private void initialiseData(SQLiteDatabase database) {
		MeasurementTable.onInitialiseData(database);
		MeasurementScaleTable.onInitialiseData(database);
	}
	
	public static boolean isSchemaUpgradeThroughVersion(int oldVersion, int newVersion, int versionToCheckFor) {
		return 
				oldVersion < versionToCheckFor &&
				newVersion >= versionToCheckFor;
	}
	
	@Override
	public void onCreate(SQLiteDatabase database) {
		_logger.getCurrentApplicationLogger().debug("Creating database");
		AlarmTable.onCreate(database);
		MeasurementTable.onCreate(database);
		MeasurementScaleTable.onCreate(database);
		AlarmMeasurementTable.onCreate(database);
		
		initialiseData(database);
	}
	
	@Override
	public void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {
		_logger.getCurrentApplicationLogger().debug("Upgrading database from version {} to {}", oldVersion, newVersion);
		AlarmMeasurementTable.onUpgrade(database, oldVersion, newVersion);
		MeasurementScaleTable.onUpgrade(database, oldVersion, newVersion);
		MeasurementTable.onUpgrade(database, oldVersion, newVersion);
		AlarmTable.onUpgrade(database, oldVersion, newVersion);

		initialiseData(database);
	}
	
	public SQLiteDatabase getDatabase() {
		return _database;
	}
	
	public SQLiteDatabase open() throws SQLException {
		_database = getWritableDatabase();
		//database.setForeignKeyConstraintsEnabled(true); 
		if (!_database.isReadOnly()) {
			// Enable foreign key constraints
			_database.execSQL("PRAGMA foreign_keys=ON;");
		}
		return _database;
	}

	public void close() {
		_database.close();
	}

	// lack of generic delegates means that this function must be declared here rather than in the base class
	public void executeInOpenContext(IRepositoryAction action) {
		synchronized (this) {
			try {
				_logger.getCurrentApplicationLogger().debug("DB opened");
				open();
			    action.execute();
			}
			catch (Exception e) {
				_logger.getCurrentApplicationLogger().error("DB Error ", e);
			}
			finally {
				close();
				_logger.getCurrentApplicationLogger().debug("DB closed");
			}
		}
	}

	
}
