package net.derekwilson.measureme.fragment.navigation;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import net.derekwilson.measureme.R;

import java.util.List;

public class NavigationDrawerAdapter extends RecyclerView.Adapter<NavigationDrawerAdapter.ViewHolder> {

	private List<NavigationItem> data;
	private INavigationDrawerCallback navigationDrawerCallback;
	private int selectedPosition = -1;
	private int touchedPosition = -1;

	public NavigationDrawerAdapter(List<NavigationItem> listItems) {
		data = listItems;
	}

	public INavigationDrawerCallback getNavigationDrawerCallbacks() {
		return navigationDrawerCallback;
	}

	public void setNavigationDrawerCallbacks(INavigationDrawerCallback navigationDrawerCallbacks) {
		navigationDrawerCallback = navigationDrawerCallbacks;
	}

	@Override
	public NavigationDrawerAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
		View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_item_navigation_row, viewGroup, false);
		return new ViewHolder(v);
	}

	@Override
	public void onBindViewHolder(NavigationDrawerAdapter.ViewHolder viewHolder, final int i) {
		viewHolder.textView.setText(data.get(i).getText());
		viewHolder.textView.setCompoundDrawablesWithIntrinsicBounds(data.get(i).getDrawable(), null, null, null);

		viewHolder.itemView.setOnTouchListener(new View.OnTouchListener() {
												   @Override
												   public boolean onTouch(View v, MotionEvent event) {

													   switch (event.getAction()) {
														   case MotionEvent.ACTION_DOWN:
															   touchPosition(i);
															   return false;
														   case MotionEvent.ACTION_CANCEL:
															   touchPosition(-1);
															   return false;
														   case MotionEvent.ACTION_MOVE:
															   return false;
														   case MotionEvent.ACTION_UP:
															   touchPosition(-1);
															   return false;
													   }
													   return true;
												   }
											   }
		);
		viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
												   @Override
												   public void onClick(View v) {
													   if (navigationDrawerCallback != null)
														   navigationDrawerCallback.onItemSelected(i,data.get(i));
												   }
											   }
		);

		if (selectedPosition == i || touchedPosition == i) {
			viewHolder.itemView.setBackgroundColor(viewHolder.itemView.getContext().getResources().getColor(R.color.color_accent));
		} else {
			viewHolder.itemView.setBackgroundColor(Color.TRANSPARENT);
		}
	}

	private void touchPosition(int position) {
		int lastPosition = touchedPosition;
		touchedPosition = position;
		if (lastPosition >= 0)
			notifyItemChanged(lastPosition);
		if (position >= 0)
			notifyItemChanged(position);
	}

	public void selectPosition(int position) {
		int lastPosition = selectedPosition;
		selectedPosition = position;
		notifyItemChanged(lastPosition);
		notifyItemChanged(position);
	}

	@Override
	public int getItemCount() {
		return data != null ? data.size() : 0;
	}

	public static class ViewHolder extends RecyclerView.ViewHolder {
		public TextView textView;

		public ViewHolder(View itemView) {
			super(itemView);
			textView = (TextView) itemView.findViewById(R.id.item_name);
		}
	}
}
