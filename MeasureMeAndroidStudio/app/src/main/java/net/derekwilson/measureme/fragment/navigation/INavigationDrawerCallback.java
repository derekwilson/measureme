package net.derekwilson.measureme.fragment.navigation;

public interface INavigationDrawerCallback {
	void onItemSelected(int position, NavigationItem selectedItem);
}
