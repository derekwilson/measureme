package net.derekwilson.measureme.model;

import java.util.List;

public class DisplayValuePairs {
	private List<String> display;
	private List<String> value;

	public DisplayValuePairs() {
	}
	
	public List<String> getDisplay() {
		return display;
	}

	public void setDisplay(List<String> display) {
		this.display = display;
	}

	public List<String> getValue() {
		return value;
	}

	public void setValue(List<String> value) {
		this.value = value;
	}
}
