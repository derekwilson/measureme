package net.derekwilson.measureme.logging;

import net.derekwilson.measureme.MeasureMe;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class SlfLoggerFactory implements ILoggerFactory {

	@Override
	public Logger getCurrentApplicationLogger() {
		return LoggerFactory.getLogger(MeasureMe.class);
	}
}
