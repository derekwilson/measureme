package net.derekwilson.measureme.activity.settings;

import android.content.SharedPreferences;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.RingtonePreference;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.google.inject.Inject;

import net.derekwilson.measureme.BuildConfig;
import net.derekwilson.measureme.MeasureMe;
import net.derekwilson.measureme.MeasureMeBuildConfig;
import net.derekwilson.measureme.R;
import net.derekwilson.measureme.data.MeasureMeDatabaseHelper;
import net.derekwilson.measureme.logging.ILoggerFactory;
import net.derekwilson.measureme.utility.IPreferencesHelper;
import net.derekwilson.measureme.utility.PreferencesHelper;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;

import roboguice.RoboGuice;

public class PreferencesActivity extends PreferenceActivity implements SharedPreferences.OnSharedPreferenceChangeListener, Preference.OnPreferenceChangeListener {
	@Inject
	private ILoggerFactory logger;

	@Inject
	private IPreferencesHelper preferences;

	private Toolbar toolbar;

	private void setupToolbar() {
		ViewGroup root = (ViewGroup) findViewById(android.R.id.content);
		View content = root.getChildAt(0);
		LinearLayout toolbarContainer = (LinearLayout) View.inflate(this, R.layout.preferences_with_toolbar, null);

		root.removeAllViews();
		toolbarContainer.addView(content);
		root.addView(toolbarContainer);

		toolbar = (Toolbar) toolbarContainer.findViewById(R.id.toolbar);
		toolbar.setTitle(getTitle());
		toolbar.setNavigationIcon(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
		toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// this will inject non view dependencies
		RoboGuice.getInjector(this.getApplicationContext()).injectMembers(this);
		logger.getCurrentApplicationLogger().debug("SettingsActivity.onCreate()");

		setupToolbar();

		addPreferencesFromResource(R.xml.activity_settings);

		findPreference(PreferencesHelper.PREFS_VERSION).setSummary(
                "Version " + MeasureMe.getVersionName(getApplicationContext()) +
                        (MeasureMeBuildConfig.PRODUCTION ? " (prod)" : " (dev)")
        );
        findPreference(PreferencesHelper.PREFS_VERSION).setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                copyDb();
                return true;
            }
        });
	}

    private void copyDb() {
        if (BuildConfig.DEBUG) {
            try {
                File sd = Environment.getExternalStorageDirectory();
                File data = Environment.getDataDirectory();

                if (sd.canWrite()) {
                    String currentDBPath = "/data/data/" + getPackageName() + "/databases/" + MeasureMeDatabaseHelper.DATABASE_NAME;
                    String backupDBPath = "measureme_backup.db";
                    File currentDB = new File(currentDBPath);
                    File backupDB = new File(sd, backupDBPath);

                    if (currentDB.exists()) {
                        FileChannel src = new FileInputStream(currentDB).getChannel();
                        FileChannel dst = new FileOutputStream(backupDB).getChannel();
                        dst.transferFrom(src, 0, src.size());
                        src.close();
                        dst.close();
                    }
                }
            } catch (Exception e) {

            }
        }
    }

	@Override
	protected void onResume(){
		super.onResume();
		getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);

		// A patch to overcome OnSharedPreferenceChange not being called by RingtonePreference bug
		RingtonePreference pref = (RingtonePreference) findPreference(PreferencesHelper.PREFS_RINGTONE);
		pref.setOnPreferenceChangeListener(this);
		updateRingtoneDisplay(pref, preferences.getAlarmSound(true));
	}

	@Override
	protected void onPause() {
		super.onPause();
		getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
	}

	@Override
	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
		logger.getCurrentApplicationLogger().debug("onSharedPreferenceChanged key: {}", key);
		updatePreference(key);
	}

	@Override
	public boolean onPreferenceChange(Preference preference, Object newValue) {
		// just called for preference - and called before the preference is changed
		logger.getCurrentApplicationLogger().debug("onPreferenceChange: {}", (String) newValue);
		Ringtone ringtone = RingtoneManager.getRingtone(this, Uri.parse((String) newValue));
		updateRingtoneDisplay(preference, ringtone);
		return true;
	}

	private void updateRingtoneDisplay(Preference preference, Ringtone ringtone) {
		if (ringtone != null)
			preference.setSummary(ringtone.getTitle(this));
		else
			preference.setSummary("Silent");
	}

	private void updatePreference(String key){
		if (PreferencesHelper.PREFS_ALARMS_ENABLED.equals(key)) {
			preferences.RefreshAllAlarmsInAlarmManager();
		}
	}
}


