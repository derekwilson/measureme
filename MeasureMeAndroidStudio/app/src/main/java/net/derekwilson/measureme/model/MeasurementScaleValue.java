package net.derekwilson.measureme.model;

public class MeasurementScaleValue {
	private long id;
	private String displayText;
	private int value;
	private int displayOrder;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getDisplayText() {
		return displayText;
	}

	public void setDisplayText(String displayText) {
		this.displayText = displayText;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public int getDisplayOrder() {
		return displayOrder;
	}

	public void setDisplayOrder(int order) {
		this.displayOrder = order;
	}
}
