package net.derekwilson.measureme.data.interfaces;

import java.util.List;

import net.derekwilson.measureme.model.MeasurementScaleValue;

public interface IMeasurementScaleRepository extends IRepository<MeasurementScaleValue> {
	List<MeasurementScaleValue> getByMeasurementId(long id);
}

