package net.derekwilson.measureme.utility;

import android.media.Ringtone;
import android.net.Uri;

public interface IPreferencesHelper {
	void RefreshAllAlarmsInAlarmManager();
	void SetupAlarmInAlarmManager(long alarmId);
	void SetupAlarmInAlarmManager(long alarmId, boolean enabled);
	Uri getRingtone();
	boolean getAlarmsEnabled();
	boolean getAlarmSoundEnabled();
	boolean getAlarmVibrateEnabled();
	void AlarmVibrate();
	Ringtone getAlarmSound(boolean ignoreEnabled);
}
