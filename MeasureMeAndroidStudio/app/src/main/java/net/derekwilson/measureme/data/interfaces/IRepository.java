package net.derekwilson.measureme.data.interfaces;

import java.util.List;



public interface IRepository<T> extends IBaseRepository {
	T create(T newItem);
	T update(T item);
	void delete(T item);
	void deleteById(long id);
	void deleteAll();
	T getById(long id);
	List<T> getAll();
}

