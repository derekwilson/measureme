package net.derekwilson.measureme.fragment.navigation;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.inject.Inject;

import net.derekwilson.measureme.R;
import net.derekwilson.measureme.logging.ILoggerFactory;

import java.util.ArrayList;
import java.util.List;

import roboguice.RoboGuice;

public class NavigationDrawerFragment extends Fragment implements INavigationDrawerCallback {

	@Inject
	private ILoggerFactory logger;

	private INavigationDrawerCallback callback;
	private RecyclerView drawerList;
	private View fragmentContainerView;
	private DrawerLayout drawerLayout;
	private ActionBarDrawerToggle actionBarDrawerToggle;

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// this will inject non view dependencies
		RoboGuice.getInjector(getActivity().getApplicationContext()).injectMembers(this);

		View view = inflater.inflate(R.layout.fragment_navigation_drawer, container, false);
		drawerList = (RecyclerView) view.findViewById(R.id.drawerList);
		LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
		layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
		drawerList.setLayoutManager(layoutManager);
		drawerList.setHasFixedSize(true);

		final List<NavigationItem> navigationItems = getMenu();
		NavigationDrawerAdapter adapter = new NavigationDrawerAdapter(navigationItems);
		adapter.setNavigationDrawerCallbacks(this);
		drawerList.setAdapter(adapter);
		return view;
	}

	private List<NavigationItem> getMenu() {
		List<NavigationItem> items = new ArrayList<NavigationItem>();
		items.add(new NavigationItem(NavigationItem.NAVIGATION_SETTINGS, "Settings", getResources().getDrawable(R.drawable.ic_action_settings)));
        items.add(new NavigationItem(NavigationItem.NAVIGATION_HELP, "Help", getResources().getDrawable(R.drawable.ic_action_help)));
        items.add(new NavigationItem(NavigationItem.NAVIGATION_PRIVACY, "Privacy", getResources().getDrawable(R.drawable.ic_action_about)));
		return items;
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			callback = (INavigationDrawerCallback) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException("Activity must implement NavigationDrawerCallbacks.");
		}
	}

	public ActionBarDrawerToggle getActionBarDrawerToggle() {
		return actionBarDrawerToggle;
	}

	public void setActionBarDrawerToggle(ActionBarDrawerToggle actionBarDrawerToggle) {
		this.actionBarDrawerToggle = actionBarDrawerToggle;
	}

	public void setup(int fragmentId, DrawerLayout drawerLayout, Toolbar toolbar) {
		fragmentContainerView = getActivity().findViewById(fragmentId);
		this.drawerLayout = drawerLayout;
		this.drawerLayout.setStatusBarBackgroundColor(
				getResources().getColor(R.color.color_primary_dark));

		actionBarDrawerToggle = new ActionBarDrawerToggle(getActivity(), NavigationDrawerFragment.this.drawerLayout, toolbar, R.string.app_name, R.string.app_name) {
			@Override
			public void onDrawerClosed(View drawerView) {
				super.onDrawerClosed(drawerView);
			}

			@Override
			public void onDrawerOpened(View drawerView) {
				super.onDrawerOpened(drawerView);
			}
		};

		this.drawerLayout.post(new Runnable() {
			@Override
			public void run() {
				actionBarDrawerToggle.syncState();
			}
		});
		this.drawerLayout.setDrawerListener(actionBarDrawerToggle);
	}

	public void openDrawer() {
		drawerLayout.openDrawer(fragmentContainerView);
	}

	public void closeDrawer() {
		drawerLayout.closeDrawer(fragmentContainerView);
	}

	@Override
	public void onDetach() {
		super.onDetach();
		callback = null;
	}

	public boolean isDrawerOpen() {
		return drawerLayout != null && drawerLayout.isDrawerOpen(fragmentContainerView);
	}

	public DrawerLayout getDrawerLayout() {
		return drawerLayout;
	}

	public void setDrawerLayout(DrawerLayout drawerLayout) {
		this.drawerLayout = drawerLayout;
	}

	private void selectItem(int position, NavigationItem selectedItem) {
		if (drawerLayout != null) {
			drawerLayout.closeDrawer(fragmentContainerView);
		}
		if (callback != null) {
			callback.onItemSelected(position, selectedItem);
		}
		((NavigationDrawerAdapter) drawerList.getAdapter()).selectPosition(position);
	}

	@Override
	public void onItemSelected(int position, NavigationItem selectedItem) {
		logger.getCurrentApplicationLogger().debug("Navigation drawer selected - " + selectedItem.getText());
		selectItem(position, selectedItem);
		// make sure the item is deselected after we have triggered the action
		((NavigationDrawerAdapter) drawerList.getAdapter()).selectPosition(-1);
	}
}
