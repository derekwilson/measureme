package net.derekwilson.measureme.model;

import net.derekwilson.measureme.R;
import android.content.res.Resources;

public class Interval {
	// make these match up with the string array resource
	// should these be in the DB rather than resources ??
	public final static int STRIDE_MINUTES = 0;
	public final static int STRIDE_HOUR = 1;
	public final static int STRIDE_DAY = 2;
	public final static int STRIDE_WEEK = 3;
	public final static int STRIDE_MAX = 3;		// maximum value - update when adding new strides
	
	private int value;
	private int stride;

	public Interval() {
	}

	private void ValidateStride() {
		if (this.stride < 0 || this.stride > STRIDE_MAX) {
			this.stride = 0;
		}
	}
	
	public Interval(int value, int stride) {
		this.value = value;
		this.stride = stride;
		ValidateStride();
	}
	
	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public int getStride() {
		return stride;
	}

	public void setStride(int stride) {
		this.stride = stride;
		ValidateStride();
	}

	public long getMilliseconds() {
		long stride = 0;
		switch (this.getStride()) {
			case Interval.STRIDE_MINUTES:	// minute
				stride = (1000 * 60);
				break;
			case Interval.STRIDE_HOUR:	// hour
				stride = (1000 * 60) * 60;
				break;
			case Interval.STRIDE_DAY:	// day
				stride = ((1000 * 60) * 60) * 24;
				break;
			case Interval.STRIDE_WEEK:	// week
				stride = (((1000 * 60) * 60) * 24) * 7;
				break;
			default:
				return -1;
		}

		return stride * this.getValue();
	}
	
	public String getStrideText(Resources resources) {
		String text = resources.getStringArray(R.array.repeat_stride_array)[stride];
		if (value > 1) {
			// if it turns out we cannot pluralise like this we will need two arrays
			return text + "s";
		}
		return text;
	}
}
