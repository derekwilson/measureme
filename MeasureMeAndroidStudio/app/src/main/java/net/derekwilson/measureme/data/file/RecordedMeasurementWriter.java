package net.derekwilson.measureme.data.file;

import java.io.File;
import java.io.FileWriter;

import android.os.Environment;

import com.google.inject.Inject;

import net.derekwilson.measureme.MeasureMe;
import net.derekwilson.measureme.data.interfaces.IRecordedMeasurementWriter;
import net.derekwilson.measureme.logging.ILoggerFactory;
import net.derekwilson.measureme.model.Measurement;
import net.derekwilson.measureme.model.RecordedMeasurement;

public class RecordedMeasurementWriter implements IRecordedMeasurementWriter {
	private static final String FILE_PREFIX = "MM_";
	private static final String FILE_SUFFIX = ".txt";

	@Inject
    private ILoggerFactory logger;

	private String getSavePath(){
		return String.format(Environment.getExternalStorageDirectory() + "/" + MeasureMe.MEASUREME_FOLDER + "/");
	}

	public String getFileFullPathname(Measurement measure){
		// try and pick a name that will not change when we can edit mearurement definitions
		return String.format(getSavePath() + FILE_PREFIX + measure.getId() + FILE_SUFFIX);
	}

	private boolean prepareSdCard(RecordedMeasurement data) {
		if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED)) {
			File sDir = new File(getSavePath());
			if (!sDir.exists()) {
				sDir.mkdirs();
			}
			File sFile = new File(getFileFullPathname(data.getMeasurement()));
			if (!sFile.exists()) {
				writeTextFile(getFileFullPathname(data.getMeasurement()), data.getHeaders());
			}
		} else {
			logger.getCurrentApplicationLogger().error("There is no memory card available. This application requires a memory card to store the files on.");
			return false;
		}
		return true;
	}	

	private void writeTextFile(String fileName, String txt){
		try {
			FileWriter writer = new FileWriter(new File(fileName),true);
			writer.append(txt);
			writer.append("\n");
			writer.flush();
			writer.close();		
		} catch (Exception e) {
			logger.getCurrentApplicationLogger().error("Unable to write to {}", fileName,e);
		}
	}
	
	@Override
	public boolean Write(RecordedMeasurement data) {
		synchronized (this) {
			if (!prepareSdCard(data)) {
				return false;
			}

			writeTextFile(getFileFullPathname(data.getMeasurement()), data.toString());
			return true;
		}
	}

}
