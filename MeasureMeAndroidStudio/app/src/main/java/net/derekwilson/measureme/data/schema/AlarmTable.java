package net.derekwilson.measureme.data.schema;

import android.database.sqlite.SQLiteDatabase;

import net.derekwilson.measureme.data.MeasureMeDatabaseHelper;

public class AlarmTable {
	// Database table
	public static final String TABLE_ALARM = "alarm";
	public static final String COLUMN_ID = "_id";
	public static final String COLUMN_START = "start";
	public static final String COLUMN_INTERVAL = "interval";
	public static final String COLUMN_INTERVAL_STRIDE = "intervalStride";
	public static final String COLUMN_ACTIVE = "active";
	public static final String COLUMN_HOURMASK = "hourMask";
	public static final String COLUMN_DAYMASK = "dayMask";
    public static final String COLUMN_CREATED = "created";
    public static final String COLUMN_NAME = "name";

	// Database creation SQL statement
	private static final String DATABASE_CREATE = "create table " 
		+ TABLE_ALARM
		+ "(" 
		+ COLUMN_ID + " integer primary key autoincrement, " 
		+ COLUMN_START + " long not null, " 
		+ COLUMN_INTERVAL + " int not null," 
		+ COLUMN_INTERVAL_STRIDE + " int not null," 	// FK and put strides in the DB ??
		+ COLUMN_ACTIVE + " int not null," 
		+ COLUMN_HOURMASK + " long not null," 
		+ COLUMN_DAYMASK + " long not null," 
		+ COLUMN_CREATED + " DATETIME DEFAULT CURRENT_TIMESTAMP, "
        + COLUMN_NAME + " varchar(255) null"
        + ");";

	public static void onCreate(SQLiteDatabase database) {
		database.execSQL(DATABASE_CREATE);
	}

	public static void onUpgrade(SQLiteDatabase database, int oldVersion,int newVersion) {
        // version 22 - name added
        if (MeasureMeDatabaseHelper.isSchemaUpgradeThroughVersion(oldVersion, newVersion, 22)) {
            database.execSQL(
                    "ALTER TABLE " + TABLE_ALARM + " ADD COLUMN "
                            + COLUMN_NAME + "  varchar(255) null"
            );
        }
	}	
}
