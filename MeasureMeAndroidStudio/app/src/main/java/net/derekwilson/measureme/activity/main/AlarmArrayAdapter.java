package net.derekwilson.measureme.activity.main;

import java.util.Calendar;
import java.util.List;

import com.google.inject.Inject;

import net.derekwilson.measureme.R;
import net.derekwilson.measureme.activity.showAlarm.ShowAlarmActivity;
import net.derekwilson.measureme.logging.ILoggerFactory;
import net.derekwilson.measureme.model.Alarm;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

public class AlarmArrayAdapter extends ArrayAdapter<Alarm> {
	private final Context context;
	private final List<Alarm> values;
    private final ILoggerFactory logger;
    private final IManualTrigger trigger;

    public interface IManualTrigger {
        void onTriggerAlarm(Alarm alarm);
    }

	@Inject
	public AlarmArrayAdapter(Context context, List<Alarm> alarms, ILoggerFactory logger, IManualTrigger manualTrigger) {
		super(context, R.layout.list_item_alarm, alarms);
		this.context = context;
		this.values = alarms;
		this.logger = logger;
        this.trigger = manualTrigger;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = inflater.inflate(R.layout.list_item_alarm, parent, false);

		final Alarm thisAlarm = values.get(position);
		
		TextView label = (TextView) rowView.findViewById(R.id.alarm_row_label);
        StringBuilder labelText = new StringBuilder(50);
        if (thisAlarm.hasName()) {
            labelText.append("(");
            labelText.append(thisAlarm.getName());
            labelText.append(") ");
        }
        if (thisAlarm.isManualAlarm(Calendar.getInstance())) {
            labelText.append(context.getString(R.string.alarm_display_manual));
        }
        else {
            labelText.append("Every " + thisAlarm.getInterval().getValue() +
                            " " + thisAlarm.getInterval().getStrideText(this.context.getResources())
            );
        }
        label.setText(labelText.toString());

		TextView subLabel = (TextView) rowView.findViewById(R.id.alarm_row_sub_label);
		subLabel.setText("Measure " + thisAlarm.getMeasurementsNames());
		
		TextView status = (TextView) rowView.findViewById(R.id.alarm_row_status);
		// TODO work out how to inject an ISystemTime
        if (thisAlarm.isManualAlarm(Calendar.getInstance())) {
            status.setText(context.getString(R.string.alarm_display_next_trigger_manual));
        } else {
            status.setText("Next " + thisAlarm.getNextTriggerTimeFormatted(Calendar.getInstance()));
        }

		// TODO - this should really be in the activty rather than here
		final ImageButton manualTriggerButton = (ImageButton) rowView.findViewById(R.id.alarm_row_manual_trigger);
        manualTriggerButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				logger.getCurrentApplicationLogger().debug("clicked {}", thisAlarm.getId());
                if (trigger != null) {
                    trigger.onTriggerAlarm(thisAlarm);
                }
			}
		});

		return rowView;
	}
}
