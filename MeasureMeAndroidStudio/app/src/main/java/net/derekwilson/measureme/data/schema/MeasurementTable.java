package net.derekwilson.measureme.data.schema;

import net.derekwilson.measureme.data.MeasureMeDatabaseHelper;
import android.database.sqlite.SQLiteDatabase;

public class MeasurementTable {
	// Database table
	public static final String TABLE_MEASUREMENT = "measurement";
	public static final String COLUMN_ID = "_id";
	public static final String COLUMN_NAME = "name";
	public static final String COLUMN_EDITABLE = "editable";
	public static final String COLUMN_RECORD_MISSED = "recordMissed";
	public static final String COLUMN_CREATED = "created";
	public static final String COLUMN_RENDER_TYPE = "renderType";

	public static final String PAIN_MEASURE = "Pain"; 
	public static final String PAIN_MEASURE_NRS11 = "Pain - NRS11"; 
	public static final String ENERGY_MEASURE = "Energy Level"; 
	public static final String FALLING_ASLEEP_MEASURE = "Sleep - Falling Asleep"; 
	public static final String SLEEP_MEASURE = "Sleep - Quality"; 
	public static final String WAKE_UP_MEASURE = "Sleep - Waking Up"; 
	public static final String UNITS_MEASURE = "Units Consumed";
	public static final String SWL_MEASURE = "Satisfaction With Life";
    public static final String STOOL_MEASURE_BRISTOL = "Stool - Bristol Scale";
    public static final String DEPRESSION_MEASURE_ZUNG = "Depression - Zung Scale";
    public static final String ANXIETY_MEASURE_ZUNG = "Anxiety - Zung Scale";
    public static final String BLOOD_PRESSURE_1_SYSTOLIC = "Blood Pressure 1 - Systolic";
    public static final String BLOOD_PRESSURE_2_DIASTOLIC = "Blood Pressure 2 - Diastolic";

	// Database creation SQL statement
	private static final String DATABASE_CREATE = "create table " 
		+ TABLE_MEASUREMENT
		+ "(" 
		+ COLUMN_ID + " integer primary key autoincrement, " 
		+ COLUMN_NAME + " varchar(255) not null, " 
		+ COLUMN_EDITABLE + " int not null," 
		+ COLUMN_RECORD_MISSED + " int not null," 
		+ COLUMN_CREATED + " DATETIME DEFAULT CURRENT_TIMESTAMP,"
		+ COLUMN_RENDER_TYPE + " int not null default 0" 
		+ ");";

	public static void onCreate(SQLiteDatabase database) {
		database.execSQL(DATABASE_CREATE);
	}

	public static void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {
		// version 11 - render type added 
		if (MeasureMeDatabaseHelper.isSchemaUpgradeThroughVersion(oldVersion, newVersion, 11)) {
			database.execSQL(
					"ALTER TABLE " + TABLE_MEASUREMENT + " ADD COLUMN "
					+ COLUMN_RENDER_TYPE + " int not null default 0"
					);
		}
	}
	
	public static void onInitialiseData(SQLiteDatabase database) {
		database.execSQL("INSERT or REPLACE INTO " + TABLE_MEASUREMENT + "(" + COLUMN_ID + "," + COLUMN_NAME + "," + COLUMN_EDITABLE + "," + COLUMN_RECORD_MISSED + "," + COLUMN_RENDER_TYPE + ")" +
				" VALUES(1,'" + PAIN_MEASURE + "',0,1,0)");
		database.execSQL("INSERT or REPLACE INTO " + TABLE_MEASUREMENT + "(" + COLUMN_ID + "," + COLUMN_NAME + "," + COLUMN_EDITABLE + "," + COLUMN_RECORD_MISSED + "," + COLUMN_RENDER_TYPE + ")" +
				" VALUES(2,'" + ENERGY_MEASURE + "',0,1,0)");
		database.execSQL("INSERT or REPLACE INTO " + TABLE_MEASUREMENT + "(" + COLUMN_ID + "," + COLUMN_NAME + "," + COLUMN_EDITABLE + "," + COLUMN_RECORD_MISSED + "," + COLUMN_RENDER_TYPE + ")" +
				" VALUES(3,'" + FALLING_ASLEEP_MEASURE + "',0,1,0)");
		database.execSQL("INSERT or REPLACE INTO " + TABLE_MEASUREMENT + "(" + COLUMN_ID + "," + COLUMN_NAME + "," + COLUMN_EDITABLE + "," + COLUMN_RECORD_MISSED + "," + COLUMN_RENDER_TYPE + ")" +
				" VALUES(4,'" + SLEEP_MEASURE + "',0,1,0)");
		database.execSQL("INSERT or REPLACE INTO " + TABLE_MEASUREMENT + "(" + COLUMN_ID + "," + COLUMN_NAME + "," + COLUMN_EDITABLE + "," + COLUMN_RECORD_MISSED + "," + COLUMN_RENDER_TYPE + ")" +
				" VALUES(5,'" + WAKE_UP_MEASURE + "',0,1,0)");
		database.execSQL("INSERT or REPLACE INTO " + TABLE_MEASUREMENT + "(" + COLUMN_ID + "," + COLUMN_NAME + "," + COLUMN_EDITABLE + "," + COLUMN_RECORD_MISSED + "," + COLUMN_RENDER_TYPE + ")" +
				" VALUES(6,'" + PAIN_MEASURE_NRS11 + "',0,1,0)");
		database.execSQL("INSERT or REPLACE INTO " + TABLE_MEASUREMENT + "(" + COLUMN_ID + "," + COLUMN_NAME + "," + COLUMN_EDITABLE + "," + COLUMN_RECORD_MISSED + "," + COLUMN_RENDER_TYPE + ")" +
				" VALUES(7,'" + UNITS_MEASURE + "',0,1,1)");
		database.execSQL("INSERT or REPLACE INTO " + TABLE_MEASUREMENT + "(" + COLUMN_ID + "," + COLUMN_NAME + "," + COLUMN_EDITABLE + "," + COLUMN_RECORD_MISSED + "," + COLUMN_RENDER_TYPE + ")" +
				" VALUES(8,'" + SWL_MEASURE + "',0,1,2)");
        database.execSQL("INSERT or REPLACE INTO " + TABLE_MEASUREMENT + "(" + COLUMN_ID + "," + COLUMN_NAME + "," + COLUMN_EDITABLE + "," + COLUMN_RECORD_MISSED + "," + COLUMN_RENDER_TYPE + ")" +
                " VALUES(9,'" + STOOL_MEASURE_BRISTOL + "',0,1,0)");
        database.execSQL("INSERT or REPLACE INTO " + TABLE_MEASUREMENT + "(" + COLUMN_ID + "," + COLUMN_NAME + "," + COLUMN_EDITABLE + "," + COLUMN_RECORD_MISSED + "," + COLUMN_RENDER_TYPE + ")" +
                " VALUES(10,'" + DEPRESSION_MEASURE_ZUNG + "',0,1,3)");
        database.execSQL("INSERT or REPLACE INTO " + TABLE_MEASUREMENT + "(" + COLUMN_ID + "," + COLUMN_NAME + "," + COLUMN_EDITABLE + "," + COLUMN_RECORD_MISSED + "," + COLUMN_RENDER_TYPE + ")" +
                " VALUES(11,'" + ANXIETY_MEASURE_ZUNG + "',0,1,3)");
        database.execSQL("INSERT or REPLACE INTO " + TABLE_MEASUREMENT + "(" + COLUMN_ID + "," + COLUMN_NAME + "," + COLUMN_EDITABLE + "," + COLUMN_RECORD_MISSED + "," + COLUMN_RENDER_TYPE + ")" +
                " VALUES(12,'" + BLOOD_PRESSURE_1_SYSTOLIC + "',0,1,0)");
        database.execSQL("INSERT or REPLACE INTO " + TABLE_MEASUREMENT + "(" + COLUMN_ID + "," + COLUMN_NAME + "," + COLUMN_EDITABLE + "," + COLUMN_RECORD_MISSED + "," + COLUMN_RENDER_TYPE + ")" +
                " VALUES(13,'" + BLOOD_PRESSURE_2_DIASTOLIC + "',0,1,0)");
	}
}
