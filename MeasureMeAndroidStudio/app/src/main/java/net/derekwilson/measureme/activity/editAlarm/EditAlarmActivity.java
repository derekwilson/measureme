package net.derekwilson.measureme.activity.editAlarm;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import net.derekwilson.measureme.R;
import net.derekwilson.measureme.activity.BaseActivity;
import net.derekwilson.measureme.activity.editAlarm.MultiselectSpinner.MultiselectSpinnerListener;
import net.derekwilson.measureme.activity.main.MainActivity;
import net.derekwilson.measureme.activity.showAlarm.ShowAlarmActivity;
import net.derekwilson.measureme.data.interfaces.IAlarmRepository;
import net.derekwilson.measureme.data.interfaces.IMeasurementRepository;
import net.derekwilson.measureme.data.interfaces.IRepositoryAction;
import net.derekwilson.measureme.logging.ILoggerFactory;
import net.derekwilson.measureme.model.Alarm;
import net.derekwilson.measureme.model.DisplayValuePairs;
import net.derekwilson.measureme.model.Measurement;
import net.derekwilson.measureme.utility.ILogSender;
import net.derekwilson.measureme.utility.IPreferencesHelper;
import net.derekwilson.measureme.utility.ISystemTime;
import net.derekwilson.measureme.utility.LogSender.Result;

import android.app.DatePickerDialog.OnDateSetListener;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.inject.Inject;
import com.melnykov.fab.FloatingActionButton;
import com.melnykov.fab.ObservableScrollView;

import roboguice.RoboGuice;

//public class EditAlarmActivity extends RoboActionBarActivity implements OnDateSetListener, OnTimeSetListener {
public class EditAlarmActivity extends BaseActivity implements OnDateSetListener, OnTimeSetListener, MultiselectSpinnerListener, View.OnClickListener {
	@Inject
    private ILoggerFactory logger;

	@Inject
    private ISystemTime systemTime;

	@Inject
	private IAlarmRepository alarmRepo;

	@Inject
	private IMeasurementRepository measurementRepo;

	@Inject
    private IPreferencesHelper preferences;

	@Inject
	private ILogSender measurementLogSender;
	
	private Alarm alarm;
	private DisplayValuePairs measurementPairs;
	private List<Measurement> allMeasurements;

    //@InjectView(R.id.txtName)
    private EditText name;

    //@InjectView(R.id.txtStartDate)
	private TextView startDate;

	//@InjectView(R.id.txtStartTime)
	private TextView startTime;

	//@InjectView(R.id.txtRepeat)
	private EditText repeatNumber;

	//@InjectView(R.id.spnRepeatStride)
	private Spinner repeatSpinner;

	//@InjectView(R.id.spnMeasure)
	private MultiselectSpinner measureSpinner;

	//@InjectView(R.id.spnDayMask)
	private MultiselectSpinner dayMaskSpinner;

	//@InjectView(R.id.spnHourMask)
	private MultiselectSpinner hourMaskSpinner;

	//@InjectView(R.id.scrollView1)
	private ObservableScrollView scrollView;

	private void HookupControls() {
		// to get the ActionBar I have to give up the InjectView until RoboGuice 3
		// Inject is OK but its just too fiddley to get the view to work
		// we can override this for testing - cheesy but gets us out of a hole

        name = (EditText)findViewById(R.id.txtName);
		scrollView = (ObservableScrollView)findViewById(R.id.scrollView1);
		startDate = (TextView)findViewById(R.id.txtStartDate);
		startTime = (TextView)findViewById(R.id.txtStartTime);
		repeatNumber = (EditText)findViewById(R.id.txtRepeat);
		repeatSpinner = (Spinner)findViewById(R.id.spnRepeatStride);
		measureSpinner = (MultiselectSpinner)findViewById(R.id.spnMeasure);
		dayMaskSpinner = (MultiselectSpinner)findViewById(R.id.spnDayMask);
		hourMaskSpinner = (MultiselectSpinner)findViewById(R.id.spnHourMask);
	}

	@Override
	protected int getLayoutResource() {
		return R.layout.activity_edit_alarm;
	}

	private void SetupFab() {
		FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
		fab.attachToScrollView(scrollView);
		fab.setOnClickListener(this);
	}

	private void SetupRepeatSpinner() {
		// we need to do this so that we can style the spinner values
		Resources res = getResources();
		String[] values = res.getStringArray(R.array.repeat_stride_array);
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.spinner_item_simple, values );
		repeatSpinner.setAdapter(adapter);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		toolbarBackButtonNeeded = true;
		super.onCreate(savedInstanceState);
		//setActionBarIcon(R.drawable.ic_launcher);

		// this will inject non view dependencies
		RoboGuice.getInjector(this.getApplicationContext()).injectMembers(this);		
		HookupControls();
		SetupRepeatSpinner();

		long id = getIntent().getLongExtra(MainActivity.ACTIVITY_PARAM_ALARM_ID, 0);
		logger.getCurrentApplicationLogger().debug("EditAlarmActivity started id: {}", id);

		LoadMeasurements();
		LoadAlarm(id);
		CopyAlarmToLayout();
		repeatNumber.setSelection(repeatNumber.getText().length());
		SetupFab();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.action_edit_alarm, menu);
		return true;
	}

    @Override
    protected void onResume() {
        super.onResume();
		logger.getCurrentApplicationLogger().debug("EditAlarmActivity.onResume()");
		if (allMeasurements == null) {
			LoadMeasurements();
		}
		if (alarm == null) {
			long id = getIntent().getLongExtra(MainActivity.ACTIVITY_PARAM_ALARM_ID, 0);
			LoadAlarm(id);
		}
    }    

	private void SaveAlarmAndExit() {
		logger.getCurrentApplicationLogger().debug("Save Alarm - id {}", alarm.getId());
		if (CopyLayoutToAlarm(true)) {
			SaveAlarmToDB();
			// really we only need to do this is something in the alarm time has changed
			preferences.SetupAlarmInAlarmManager(alarm.getId());
			Intent returnIntent = new Intent();
			returnIntent.putExtra(MainActivity.ACTIVITY_PARAM_ALARM_ID,alarm.getId());
			setResult(RESULT_OK,returnIntent);
			finish();
		}
	}

	// Reaction to the menu selection
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.action_saveAlarm:
				SaveAlarmAndExit();
				break;
			case R.id.action_deleteAlarm:
				logger.getCurrentApplicationLogger().debug("Delete Alarm id {}", alarm.getId());
				if (alarm.getId() == -1) {
					Toast.makeText(this, "Alarm does not exist yet", Toast.LENGTH_SHORT).show();
					break;
				}
				DeleteAlarm(alarm.getId());
				preferences.SetupAlarmInAlarmManager(alarm.getId(), false);
				Intent returnIntent = new Intent();
				returnIntent.putExtra(MainActivity.ACTIVITY_PARAM_ALARM_ID,alarm.getId());
				setResult(RESULT_OK,returnIntent);     
				finish();
				break;
			case R.id.action_measureNow:
                CopyLayoutToAlarm(false);
				String selectedMeasurementIds = alarm.getMeasurementsAsValueString();
				if (selectedMeasurementIds != null) {
					logger.getCurrentApplicationLogger().debug("Measure Now - measurement ids {}", selectedMeasurementIds);
					Intent intent = new Intent(this, ShowAlarmActivity.class);
                    intent.putExtra(MainActivity.ACTIVITY_PARAM_MEASUREMENT_NAME, alarm.getName());
					intent.putExtra(MainActivity.ACTIVITY_PARAM_MEASUREMENT_IDS,selectedMeasurementIds);
					startActivity(intent);
				}
				break;
			case R.id.action_sendLog:
				Result result = measurementLogSender.generateSendLogfilesForMeasurementsIntent(alarm.getMeasurements(),alarm.getMeasurementsNames());
				if (result.NumberOfLogs < 1) {
					 Toast.makeText(this, "There are no recorded measurements", Toast.LENGTH_SHORT).show();
					 break;
				}
				else if (result.SendIntent == null) {
					 Toast.makeText(this, "Unable to send measurements", Toast.LENGTH_SHORT).show();
					 break;
				}
				startActivity(Intent.createChooser(result.SendIntent, "Send email"));
				break;
				
		}
		return super.onOptionsItemSelected(item);
	}

	private void DeleteAlarm(final long id)
	{
		alarmRepo.executeInOpenContext(new IRepositoryAction() {
            @Override
            public void execute() {
                alarmRepo.deleteById(id);
            }
        });
	}

	private void SetupNewAlarm()
	{
		alarm = new Alarm(systemTime.getCurrentTime());
		alarm.setId(-1);
	}

	private void LoadAlarm(final long id)
	{
		if (id == -1) {
			// we are creating a new alarm
			SetupNewAlarm();
		}
		else {
			alarmRepo.executeInOpenContext(new IRepositoryAction () {
				@Override public void execute() {
					alarm = alarmRepo.getById(id);
				}
			});
		}
	}

	private void SaveAlarmToDB() {
		alarmRepo.executeInOpenContext(new IRepositoryAction() {
            @Override
            public void execute() {
                if (alarm.getId() == -1) {
                    alarmRepo.create(alarm);
                } else {
                    // TODO optimise - spot when the measure has not changed
                    alarmRepo.updateWithMeasurement(alarm);
                }
            }
        });
	}

	private void LoadMeasurements() {
		measurementRepo.executeInOpenContext(new IRepositoryAction() {
            @Override
            public void execute() {
                measurementPairs = measurementRepo.getAllDisplayValuePairs();
                allMeasurements = measurementRepo.getAll();
            }
        });
		logger.getCurrentApplicationLogger().debug("{} measurements are in the DB.", allMeasurements.size());
	}
	
	
	private void CopyAlarmToLayout() {
		Calendar cal = alarm.getStartTime();

        name.setText(alarm.getName());

		SimpleDateFormat date_format = new SimpleDateFormat("EEE dd MMM yyyy");
		//DateFormat date_format = SimpleDateFormat.getDateInstance();
		startDate.setText(date_format.format(cal.getTime()));

		SimpleDateFormat time_format = new SimpleDateFormat(" HH:mm");
		//DateFormat time_format = SimpleDateFormat.getTimeInstance();
		startTime.setText(time_format.format(cal.getTime()));
		
		repeatNumber.setText(String.valueOf(alarm.getInterval().getValue()));
		repeatSpinner.setSelection(alarm.getInterval().getStride());
		
		PopulateMeasurementSpinner();
		PopulateMaskSpinners();
	}

	private void PopulateMeasurementSpinner() {
		String selectedMeasurements = alarm.getMeasurementsAsValueString();
		measureSpinner.setItemsWithValues(measurementPairs.getDisplay(), measurementPairs.getValue(), selectedMeasurements, "All Measures", this);
	}
	
	private void PopulateMaskSpinners() {
		Resources res = getResources();

		String[] days = res.getStringArray(R.array.day_mask_array);
		String[] days_values = res.getStringArray(R.array.day_mask_value_array);
		String selectedDayValues = alarm.getDayMaskAsValueString(days_values);
		dayMaskSpinner.setItemsWithValues(Arrays.asList(days), Arrays.asList(days_values), selectedDayValues, "Every day", this);

		String[] hours = res.getStringArray(R.array.hour_mask_array);
		String[] hour_values = res.getStringArray(R.array.hour_mask_value_array);
		String selectedHourValues = alarm.getHourMaskAsValueString(hour_values);
		hourMaskSpinner.setItemsWithValues(Arrays.asList(hours), Arrays.asList(hour_values), selectedHourValues, "Every hour", this);
	}

	private boolean CopyLayoutToAlarm(boolean showErrors)
	{
        if (alarm.getMeasurements().size() < 1) {
            if (showErrors) {
                Toast.makeText(this, "You need to select at least one measurement", Toast.LENGTH_SHORT).show();
            }
            return false;
        }

        int repeat = 0;
        try {
			repeat = Integer.parseInt(repeatNumber.getText().toString());
		} catch(NumberFormatException nfe) {
            if (showErrors) {
                Toast.makeText(this, "Invalid repeat number", Toast.LENGTH_SHORT).show();
            }
			return false;
		}

		alarm.getInterval().setValue(repeat);
		alarm.getInterval().setStride(repeatSpinner.getSelectedItemPosition());
        alarm.setName(name.getText().toString());

		return true;
	}

	public void onClick(View view) {
        FragmentManager fm = getSupportFragmentManager();
		switch(view.getId()) {
			case R.id.fab:
				SaveAlarmAndExit();
				break;
			case R.id.txtStartDate:
		        DatePickerFragment dateFragment = new DatePickerFragment(this,alarm.getStartTime());
		        dateFragment.show(fm, "date_picker");
		        break;
			case R.id.txtStartTime:
		        TimePickerFragment timeFragment = new TimePickerFragment(this,alarm.getStartTime());
		        timeFragment.show(fm, "time_picker");
		}
	}
	
	@Override
    public void onDateSet(DatePicker view, int year, int monthOfYear,int dayOfMonth) {
		CopyLayoutToAlarm(false);
		Calendar cal = alarm.getStartTime();
		cal.set(
				year,
				monthOfYear,
				dayOfMonth,
				cal.get(Calendar.HOUR_OF_DAY),
				cal.get(Calendar.MINUTE),
				0);
		CopyAlarmToLayout();
    }

	@Override
	public void onTimeSet(TimePicker view, int hour, int minute) {
		CopyLayoutToAlarm(false);
		Calendar cal = alarm.getStartTime();
		cal.set(
				cal.get(Calendar.YEAR),
				cal.get(Calendar.MONTH),
				cal.get(Calendar.DAY_OF_MONTH),
				hour,
				minute,
				0);
		CopyAlarmToLayout();
	}

	@Override
	public void onItemsSelected(MultiselectSpinner control, boolean[] selected) {
		logger.getCurrentApplicationLogger().debug("onItemsSelected {}", control.getId());
		switch (control.getId()) {
			case R.id.spnMeasure:
				setMeasurementsFromSelectedArray(selected);
				break;
			case R.id.spnDayMask:
				alarm.setDayMaskFromSelectedArray(selected);
				break;
			case R.id.spnHourMask:
				alarm.setHourMaskFromSelectedArray(selected);
				break;
		}
	}
	
	private void setMeasurementsFromSelectedArray(boolean[] selected) {
		boolean anySet = false;
		for (int index = 0; index < selected.length; index++) { 
			if (selected[index]) {
				anySet = true;
				break;
			}
		}
		if (!anySet) {
			 Toast.makeText(this, "No measurements have been selected", Toast.LENGTH_SHORT).show();
		}
		alarm.getMeasurements().clear();
		for (int index = 0; index < selected.length; index++) { 
			if (selected[index]) {
				alarm.getMeasurements().add(allMeasurements.get(index));
			}
		}
	}
}
