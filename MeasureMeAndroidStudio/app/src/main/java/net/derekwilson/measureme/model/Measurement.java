package net.derekwilson.measureme.model;

import java.util.ArrayList;
import java.util.List;

public class Measurement {
	
	public enum RenderType {
		SingleColumn,       // in DB 0
		TwoColumn,          // in DB 1
		Slider7,            // in DB 2
        Slider4             // in DB 3
	}
	
	private long id;
	private String name;
	private boolean editable;
	private boolean recordMissed;
	private RenderType renderType;
	private List<MeasurementScaleValue> scale;

	public Measurement() {
		scale = new ArrayList<MeasurementScaleValue>();
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean getEditable() {
		return editable;
	}

	public void setEditable(boolean editable) {
		this.editable = editable;
	}

	public boolean getRecordMissed() {
		return recordMissed;
	}

	public void setRecordMissed(boolean recordMissed) {
		this.recordMissed = recordMissed;
	}

	public RenderType getRenderType() {
		return renderType;
	}

	public void setRenderType(RenderType renderType) {
		this.renderType = renderType;
	}

	public List<MeasurementScaleValue> getScale() {
		return scale;
	}

	public void setScale(List<MeasurementScaleValue> scale) {
		this.scale = scale;
	}

	// Will be used by the ArrayAdapter in the ListView
	@Override
	public String toString() {
		return name;
	}	
}
