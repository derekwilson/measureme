package net.derekwilson.measureme.activity.settings;

import roboguice.activity.RoboPreferenceActivity;

import com.google.inject.Inject;

import net.derekwilson.measureme.MeasureMe;
import net.derekwilson.measureme.MeasureMeBuildConfig;
import net.derekwilson.measureme.R;
import net.derekwilson.measureme.logging.ILoggerFactory;
import net.derekwilson.measureme.utility.IPreferencesHelper;
import net.derekwilson.measureme.utility.PreferencesHelper;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.RingtonePreference;

// this is the old preferences screen without any toolbar or actionbar
// this activity is deprecated

public class SettingsActivity extends RoboPreferenceActivity implements OnSharedPreferenceChangeListener, OnPreferenceChangeListener {
	@Inject
    private ILoggerFactory logger;

	@Inject
    private IPreferencesHelper preferences;

	@Override
	public void onCreate(Bundle savedInstanceState) {    
	    super.onCreate(savedInstanceState);       
	    addPreferencesFromResource(R.xml.activity_settings);

	    findPreference(PreferencesHelper.PREFS_VERSION).setSummary(
	    		"Version " + MeasureMe.getVersionName(getApplicationContext()) +
	    		(MeasureMeBuildConfig.PRODUCTION ? " (prod)" : " (dev)")
	    		);
	}

	@Override
    protected void onResume(){
        super.onResume();
        getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);

        // A patch to overcome OnSharedPreferenceChange not being called by RingtonePreference bug 
        RingtonePreference pref = (RingtonePreference) findPreference(PreferencesHelper.PREFS_RINGTONE);
        pref.setOnPreferenceChangeListener(this);
        updateRingtoneDisplay(pref, preferences.getAlarmSound(true));
	}
 
    @Override
    protected void onPause() {
        super.onPause();
        getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
    }    
 
    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
		logger.getCurrentApplicationLogger().debug("onSharedPreferenceChanged key: {}", key);
        updatePreference(key);
    }   

    @Override
    public boolean onPreferenceChange(Preference preference, Object newValue) {
    	// just called for preference - and called before the preference is changed
		logger.getCurrentApplicationLogger().debug("onPreferenceChange: {}", (String) newValue);
        Ringtone ringtone = RingtoneManager.getRingtone(this, Uri.parse((String) newValue));
        updateRingtoneDisplay(preference, ringtone);
		return true;
    }
    
    private void updateRingtoneDisplay(Preference preference, Ringtone ringtone) {
        if (ringtone != null)
            preference.setSummary(ringtone.getTitle(this));
        else
            preference.setSummary("Silent");
    }
    
    private void updatePreference(String key){
        if (PreferencesHelper.PREFS_ALARMS_ENABLED.equals(key)) {
        	preferences.RefreshAllAlarmsInAlarmManager();
        }
    }
}
