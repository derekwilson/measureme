package net.derekwilson.measureme.fragment.navigation;

import android.graphics.drawable.Drawable;

public class NavigationItem {
	public static final int NAVIGATION_SETTINGS = 1;
    public static final int NAVIGATION_HELP = 2;
    public static final int NAVIGATION_PRIVACY = 3;

	private int id;
	private String text;
	private Drawable drawable;

	public NavigationItem(int id, String text, Drawable drawable) {
		this.id = id;
		this.text = text;
		this.drawable = drawable;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Drawable getDrawable() {
		return drawable;
	}

	public void setDrawable(Drawable drawable) {
		this.drawable = drawable;
	}
}
