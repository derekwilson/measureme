package net.derekwilson.measureme.data.repository;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.google.inject.Inject;

import net.derekwilson.measureme.data.interfaces.IAlarmMeasurementRepository;
import net.derekwilson.measureme.data.interfaces.IMeasurementRepository;
import net.derekwilson.measureme.data.schema.AlarmMeasurementTable;
import net.derekwilson.measureme.model.Measurement;

public class AlarmMeasurementRepository  extends BaseRepository implements IAlarmMeasurementRepository {

	@Inject
    private IMeasurementRepository measurementRepo;

	private String[] allColumns = {
			AlarmMeasurementTable.COLUMN_ID,
			AlarmMeasurementTable.COLUMN_ALARM_ID,
			AlarmMeasurementTable.COLUMN_MEASUREMENT_ID
			};

	@Inject
	public AlarmMeasurementRepository(Context context) {
		super(context);
	}

	public long create(long alarmId, long measurementId) {
		ContentValues values = new ContentValues();
		values.put(AlarmMeasurementTable.COLUMN_ALARM_ID, alarmId);
		values.put(AlarmMeasurementTable.COLUMN_MEASUREMENT_ID, measurementId);
		
		long insertId = getDatabase().insert(AlarmMeasurementTable.TABLE_ALARM_MEASUREMENT, null,values);

		_logger.getCurrentApplicationLogger().debug("Alarm measure created with id {}.", insertId);

		return insertId;
	}
	
	@Override
	public List<Measurement> getByAlarmId(long id, boolean getScale) {
		List<Measurement> measurements = new ArrayList<Measurement>();

		Cursor cursor = getDatabase().query(AlarmMeasurementTable.TABLE_ALARM_MEASUREMENT, allColumns, AlarmMeasurementTable.COLUMN_ALARM_ID + " = " + id, null, null, null, null);

		try {
			cursor.moveToFirst();
			while (!cursor.isAfterLast()) {
				int measurementId = cursor.getInt((cursor.getColumnIndex(AlarmMeasurementTable.COLUMN_MEASUREMENT_ID)));
				// only get the measurement not the scale
				measurements.add(measurementRepo.getById(measurementId,getScale));
				cursor.moveToNext();
			}
		}
		finally {
			// make sure to close the cursor
			if (cursor != null) {
				cursor.close();
			}
		}
		
		return measurements;
	}

	@Override
	public void deleteByAlarmId(long id) {
		int deleted = getDatabase().delete(AlarmMeasurementTable.TABLE_ALARM_MEASUREMENT, AlarmMeasurementTable.COLUMN_ALARM_ID + " = " + id, null);
		_logger.getCurrentApplicationLogger().debug("Alarms Meaurements deleted with alarm id {}, {} rows deleted.", id, deleted);
	}

	@Override
	public void deleteAll() {
		_logger.getCurrentApplicationLogger().debug("Delete all alarm measures.");
		getDatabase().delete(AlarmMeasurementTable.TABLE_ALARM_MEASUREMENT, null, null);
	}
}
