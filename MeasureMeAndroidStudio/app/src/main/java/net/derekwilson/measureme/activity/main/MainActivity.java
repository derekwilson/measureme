package net.derekwilson.measureme.activity.main;

import java.util.ArrayList;
import java.util.List;

import net.derekwilson.measureme.R;
import net.derekwilson.measureme.activity.BaseActivity;
import net.derekwilson.measureme.activity.editAlarm.EditAlarmActivity;
import net.derekwilson.measureme.activity.help.HelpActivity;
import net.derekwilson.measureme.activity.settings.PreferencesActivity;
import net.derekwilson.measureme.activity.showAlarm.ShowAlarmActivity;
import net.derekwilson.measureme.data.interfaces.IAlarmRepository;
import net.derekwilson.measureme.data.interfaces.IMeasurementRepository;
import net.derekwilson.measureme.data.interfaces.IRepositoryAction;
import net.derekwilson.measureme.fragment.navigation.INavigationDrawerCallback;
import net.derekwilson.measureme.fragment.navigation.NavigationItem;
import net.derekwilson.measureme.logging.ILoggerFactory;
import net.derekwilson.measureme.model.Alarm;
import net.derekwilson.measureme.model.Measurement;
import net.derekwilson.measureme.utility.ILogSender;
import net.derekwilson.measureme.utility.IPreferencesHelper;
import net.derekwilson.measureme.utility.ISystemTime;
import net.derekwilson.measureme.utility.LogSender.Result;

import roboguice.RoboGuice;
import com.google.inject.Inject;
import com.melnykov.fab.FloatingActionButton;

import android.net.Uri;
import android.os.Bundle;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.View.OnCreateContextMenuListener;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity extends BaseActivity implements View.OnClickListener, INavigationDrawerCallback, AlarmArrayAdapter.IManualTrigger {
	public static final String BROADCAST_ALARMS_UPDATED = "net.derekwilson.measureme.intent.action.ALARMS_UPDATED";
	public static final String BROADCAST_PARAM_RELOAD_NEEDED = "reload";

	public static final String ACTIVITY_PARAM_ALARM_ID = "alarmId";
    public static final String ACTIVITY_PARAM_MEASUREMENT_IDS = "measurementIds";
    public static final String ACTIVITY_PARAM_MEASUREMENT_NAME = "measurementName";
	public static final String ACTIVITY_PARAM_STATE = "state";

    private static final String URL_PRIVACY = "https://bitbucket.org/derekwilson/measureme/src/54ae8f10b1479b48443b15e1a17c87ed63d986c3/PIRVACY.md?at=master&fileviewer=file-view-default";

	@Inject
	private ILoggerFactory logger;

	@Inject
    private ISystemTime systemTime;

	@Inject
    private IPreferencesHelper preferences;
	
	@Inject
	private IAlarmRepository alarmRepo;

	@Inject
	private IMeasurementRepository measurementRepo;
	
	@Inject
	private ILogSender measurementLogSender;

	private AlarmArrayAdapter adapter;

	//@InjectView(R.id.lvAlarms)
	private ListView lvAlarms;
	//@InjectView(R.id.lvAlarms)
	private LinearLayout layoutNoData;;

	private List<Alarm> alarms;
	private List<Measurement> allMeasurements;
	
	private BroadcastReceiver alarmUpdateReceiver;

	@Override
	protected int getLayoutResource() {
		return R.layout.activity_main;
	}

	private void HookupControls() {
		// to get the ActionBar I have to give up the InjectView until RoboGuice 3
		// Inject is OK but its just too fiddley to get the view to work
		// we can override this for testing - cheesy but gets us out of a hole
		
		lvAlarms = (ListView)findViewById(R.id.lvAlarms);
		layoutNoData = (LinearLayout)findViewById(R.id.layNoData);
	}

	private void setupFab() {
		FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
		fab.attachToListView(lvAlarms);
		fab.setOnClickListener(this);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		initNavigationDrawer(R.id.drawer, R.id.fragment_drawer);

		// this will inject non view dependencies
		RoboGuice.getInjector(this.getApplicationContext()).injectMembers(this);		
		logger.getCurrentApplicationLogger().debug("MainActivity started");
		HookupControls();

		alarms = new ArrayList<Alarm>();
		LoadAlarms();

		// there should be no need for this
		preferences.RefreshAllAlarmsInAlarmManager();
		
    	adapter = new AlarmArrayAdapter(this, alarms, logger, this);

    	lvAlarms.setAdapter(adapter);
    	lvAlarms.setLongClickable(true);
    	lvAlarms.setEmptyView(layoutNoData);
    	lvAlarms.setOnItemClickListener(new AdapterView.OnItemClickListener() {
    		@Override
    		public void onItemClick(AdapterView<?> l, View v, int position, long id) {
    			// Don't let the adapter change the order of the items
    			long alarmId = alarms.get(position).getId();
    			logger.getCurrentApplicationLogger().debug("click on item {} id {}",position,alarmId);
				EditAlarm(alarmId);
			};
    	});
    	lvAlarms.setOnCreateContextMenuListener(new OnCreateContextMenuListener() {
            @Override 
            public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) { 
                MenuInflater inflater = getMenuInflater();
                inflater.inflate(R.menu.alarm_row, menu);
            }
        });

		setupFab();
	}

	@Override
    protected void onResume() {
        super.onResume();
		logger.getCurrentApplicationLogger().debug("MainActivity.onResume()");
		if (alarmUpdateReceiver == null) {
			CreateAlarmUpdateReceiver();
		}
    }    

	@Override
	public void onDestroy() {
		DestroyAlarmUpdateReceiver();
		super.onDestroy();
	}

	@Override
	public void onBackPressed() {
		if (navigationDrawerFragment != null && navigationDrawerFragment.isDrawerOpen())
			navigationDrawerFragment.closeDrawer();
		else
			super.onBackPressed();
	}

	private void DestroyAlarmUpdateReceiver() {
		if (alarmUpdateReceiver != null) {
			unregisterReceiver(alarmUpdateReceiver);
		}
	}
	
	private void CreateAlarmUpdateReceiver() {
		alarmUpdateReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				// do not unregister as this is not a one shot
				logger.getCurrentApplicationLogger().debug("AlarmUpdateReceiver - received");
				boolean reload = intent.getBooleanExtra(BROADCAST_PARAM_RELOAD_NEEDED,false);
				if (reload) {
					RefreshData();
				}
				else {
					adapter.notifyDataSetChanged();
				}
			}
		};

		IntentFilter filter = new IntentFilter(BROADCAST_ALARMS_UPDATED);
        filter.addCategory(Intent.CATEGORY_DEFAULT);
        registerReceiver(alarmUpdateReceiver, filter);
		logger.getCurrentApplicationLogger().debug("Set up AlarmUpdateReceiver");
	}
	
	private void EditAlarm(long id)
	{
		Intent intent = new Intent(this, EditAlarmActivity.class);
		intent.putExtra(MainActivity.ACTIVITY_PARAM_ALARM_ID, id);
		startActivityForResult(intent, 1);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (data != null) {
	        final long alarmId = data.getLongExtra(MainActivity.ACTIVITY_PARAM_ALARM_ID, 0);
			logger.getCurrentApplicationLogger().debug("Activity Result code {} ID {}", resultCode, alarmId);
			if (resultCode==RESULT_OK) {
				RefreshData();
			}
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.action_main, menu);
		return true;
	}

	private void LoadAlarms()
	{
		alarms.clear();
		alarmRepo.executeInOpenContext(new IRepositoryAction () {
			@Override public void execute() {
				List<Alarm> loaded_alarms = alarmRepo.getAll();
				alarms.addAll(loaded_alarms);
			}
		});
		logger.getCurrentApplicationLogger().debug("{} alarms are in the DB.", alarms.size());
	}
	
	private void RefreshData()
	{
		LoadAlarms();
    	adapter.notifyDataSetChanged();
	}

	// reaction to the navigation drawer
	@Override
	public void onItemSelected(int position, NavigationItem selectedItem) {
		if (selectedItem != null) {
			switch (selectedItem.getId()) {
				case NavigationItem.NAVIGATION_SETTINGS:
					Intent intent = new Intent(this, PreferencesActivity.class);
					startActivity(intent);
					break;
				case NavigationItem.NAVIGATION_HELP:
					Intent help_intent = new Intent(this, HelpActivity.class);
					startActivity(help_intent);
					break;
                case NavigationItem.NAVIGATION_PRIVACY:
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(URL_PRIVACY));
                    startActivity(browserIntent);
                    break;
            }
		}
	}

	// Reaction to the menu selection
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.action_deleteAll:
				// disable them before we delete them
		    	preferences.SetupAlarmInAlarmManager(-1,false);
				alarmRepo.executeInOpenContext(new IRepositoryAction () {
					@Override public void execute() {
					    alarmRepo.deleteAll();
					}
				});
				RefreshData();
				return true;
			case R.id.action_addAlarm:
				EditAlarm(-1);
				RefreshData();
				return true;
			case R.id.action_sendAll:
				measurementRepo.executeInOpenContext(new IRepositoryAction () {
					@Override public void execute() {
						allMeasurements = measurementRepo.getAll();
					}
				});
				startSendMeasurementLogsActivity(measurementLogSender.generateSendLogfilesForMeasurementsIntent(allMeasurements,"All Measurements"));
				return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public void onClick(View view) {
		switch (view.getId()) {
			case R.id.fab:
				EditAlarm(-1);
				RefreshData();
				break;
		}
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		AdapterContextMenuInfo menuInfo = (AdapterContextMenuInfo) item.getMenuInfo(); 
		// Don't let the adapter change the order of the items
		Alarm alarm = alarms.get(menuInfo.position);
	    switch (item.getItemId()) {
	    	case R.id.alarm_row_delete:
    			final long alarmId = alarm.getId();
				preferences.SetupAlarmInAlarmManager(alarmId,false);
    			logger.getCurrentApplicationLogger().debug("context delete at pos {} id {}", menuInfo.position, alarmId);
				alarmRepo.executeInOpenContext(new IRepositoryAction () {
					@Override public void execute() {
					    alarmRepo.deleteById(alarmId);
					}
				});
				RefreshData();
	    		return true;
	    	case R.id.alarm_row_sendLog:
				startSendMeasurementLogsActivity(measurementLogSender.generateSendLogfilesForMeasurementsIntent(alarm.getMeasurements(),alarm.getMeasurementsNames()));
	    		return true;
	    	case R.id.alarm_row_measureNow:
                triggerMeasurement(alarm);
	    		return true;
	    	default:
	    		return super.onContextItemSelected(item);
	    }
	}

    private void triggerMeasurement(Alarm alarm) {
        String selectedMeasurementIds = alarm.getMeasurementsAsValueString();
        if (selectedMeasurementIds != null) {
            logger.getCurrentApplicationLogger().debug("Measure Now - measurement ids {}", selectedMeasurementIds);
            Intent intent = new Intent(this, ShowAlarmActivity.class);
            intent.putExtra(MainActivity.ACTIVITY_PARAM_MEASUREMENT_NAME, alarm.getName());
            intent.putExtra(MainActivity.ACTIVITY_PARAM_MEASUREMENT_IDS,selectedMeasurementIds);
            startActivity(intent);
        }
    }

	private void startSendMeasurementLogsActivity(Result sendingIntent) {
		if (sendingIntent.NumberOfLogs < 1) {
			Toast.makeText(this, "There are no recorded measurements", Toast.LENGTH_SHORT).show();
            return;
		}
		else if (sendingIntent.SendIntent == null) {
			Toast.makeText(this, "Unable to send measurements", Toast.LENGTH_SHORT).show();
            return;
		}
		startActivity(Intent.createChooser(sendingIntent.SendIntent, "Send email"));
	}

    @Override
    public void onTriggerAlarm(Alarm alarm) {
        triggerMeasurement(alarm);
    }
}
