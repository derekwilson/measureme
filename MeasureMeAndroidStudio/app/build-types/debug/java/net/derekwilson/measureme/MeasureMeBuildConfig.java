package net.derekwilson.measureme;

public class MeasureMeBuildConfig {
    /** Whether or not to include logging statements in the application. */
    public final static boolean PRODUCTION = false;
}