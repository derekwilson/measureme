package net.derekwilson.measureme.model.alarm;

import net.derekwilson.measureme.AndroidTestRunner;
import net.derekwilson.measureme.model.Alarm;

import org.junit.Test;
import org.junit.runner.RunWith;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

@RunWith(AndroidTestRunner.class)
public class HasNameTests extends BaseAlarmTester {
    @Test
    public void There_is_no_name() {
        Alarm alarm = GetSUT();
        assertThat(alarm.hasName(), is(false));
    }

    @Test
    public void There_is_a_name() {
        Alarm alarm = GetSUT();
        alarm.setName("test");
        assertThat(alarm.hasName(), is(true));
    }
}
