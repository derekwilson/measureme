package net.derekwilson.measureme.model.alarm;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import net.derekwilson.measureme.AndroidTestRunner;
import net.derekwilson.measureme.model.Alarm;
import net.derekwilson.measureme.model.Interval;

import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(AndroidTestRunner.class)
public class NextTriggerTests extends BaseAlarmTester {
	private final String alarmDisabledText = "alarm is effectively disabled";
	
	@Test
	public void Next_Trigger_When_Days_All_Masked() {
		Alarm alarm = GetSUT();
		alarm.setDayMask(0x0);
		assertThat(alarm.getNextTriggerTimeFormatted(GetNow()), is(alarmDisabledText));		
	}
	
	@Test
	public void Next_Trigger_When_Hours_All_Masked() {
		Alarm alarm = GetSUT();
		alarm.setHourMask(0x0);
		assertThat(alarm.getNextTriggerTimeFormatted(GetNow()), is(alarmDisabledText));		
	}
	
	@Test
	public void Next_Trigger_When_Days_And_Hours_All_Masked() {
		Alarm alarm = GetSUT();
		alarm.setDayMask(0x0);
		alarm.setHourMask(0x0);
		assertThat(alarm.getNextTriggerTimeFormatted(GetNow()), is(alarmDisabledText));		
	}
	
	@Test
	public void Next_Trigger_After_1_Day() {
		Alarm alarm = GetSUT();
		assertThat(alarm.getNextTriggerTimeFormatted(GetNow()), is("Fri 18 Apr at 20:00"));		
	}

	@Test
	public void Next_Trigger_After_1_Hour() {
		Alarm alarm = GetSUT();
		alarm.setInterval(new Interval(1,Interval.STRIDE_HOUR));
		assertThat(alarm.getNextTriggerTimeFormatted(GetNow()), is("Thu 17 Apr at 21:00"));		
	}

	@Test
	public void Next_Trigger_After_1_Hour_With_Hour_Mask_Of_1_Hour() {
		Alarm alarm = GetSUT();
		alarm.setInterval(new Interval(1,Interval.STRIDE_HOUR));
		alarm.setHourMask(0x6FFFC0);
		// mask forces the trigger onto 10pm
		assertThat(alarm.getNextTriggerTimeFormatted(GetNow()), is("Thu 17 Apr at 22:00"));		
	}

	@Test
	public void Next_Trigger_After_1_Hour_With_Hour_Mask_Of_Tomorrow() {
		Alarm alarm = GetSUT();
		alarm.setInterval(new Interval(1,Interval.STRIDE_HOUR));
		alarm.setHourMask(0x0FFFC0);
		// mask forces the trigger onto the next day
		assertThat(alarm.getNextTriggerTimeFormatted(GetNow()), is("Fri 18 Apr at 07:00"));		
	}

	@Test
	public void Next_Trigger_After_1_Day_With_Today_Masked() {
		Alarm alarm = GetSUT();
		alarm.setDayMask(0x6F);
		// thu is masked
		assertThat(alarm.getNextTriggerTimeFormatted(GetNow()), is("Fri 18 Apr at 20:00"));		
	}

	@Test
	public void Next_Trigger_After_1_Hour_With_Today_Masked() {
		Alarm alarm = GetSUT();
		alarm.setInterval(new Interval(1,Interval.STRIDE_HOUR));
		alarm.setDayMask(0x6F);
		// thu is masked
		assertThat(alarm.getNextTriggerTimeFormatted(GetNow()), is("Fri 18 Apr at 07:00"));		
	}

	@Test
	public void Next_Trigger_After_1_Week() {
		Alarm alarm = GetSUT();
		alarm.setInterval(new Interval(1,Interval.STRIDE_WEEK));
		assertThat(alarm.getNextTriggerTimeFormatted(GetNow()), is("Thu 24 Apr at 20:00"));		
	}

	@Test
	public void Next_Trigger_After_1_Week_With_Today_Masked() {
		Alarm alarm = GetSUT();
		alarm.setInterval(new Interval(1,Interval.STRIDE_WEEK));
		alarm.setDayMask(0x6F);
		// thu is masked
		assertThat(alarm.getNextTriggerTimeFormatted(GetNow()), is(alarmDisabledText));		
	}

	@Test
	public void Next_Trigger_After_4_Weeks() {
		Alarm alarm = GetSUT();
		alarm.setInterval(new Interval(4,Interval.STRIDE_WEEK));
		assertThat(alarm.getNextTriggerTimeFormatted(GetNow()), is("Thu 15 May at 20:00"));		
	}
}

