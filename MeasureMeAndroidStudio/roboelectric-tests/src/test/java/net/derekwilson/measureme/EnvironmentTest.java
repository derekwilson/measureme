package net.derekwilson.measureme;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import net.derekwilson.measureme.activity.main.MainActivity;

import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(AndroidTestRunner.class)
public class EnvironmentTest {
	@Test
	public void Ensure_Junit_Roboelectric_Android_App_And_Resources_All_Attached() {
		// this is really an integration test to ensure that the test runner chain is all hooked up
		// Junit -> Roboelectric -> Android -> App APK -> resources
		
		MainActivity activity = new MainActivity();
		String resourceValue = activity.getResources().getString(R.string.placeholder);
		assertThat(resourceValue, is("placeholder"));
	}
}
