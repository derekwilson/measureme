package net.derekwilson.measureme.model.alarm;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import net.derekwilson.measureme.AndroidTestRunner;
import net.derekwilson.measureme.model.Alarm;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(AndroidTestRunner.class)
public class MaskConverterTests extends BaseAlarmTester {
	private final String[] values7 = {"1","2","3","4","5","6","7"};
	private final String[] values24 = {"1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24"};

	@Test
	public void Day_Mask_As_Value_String() {
		Alarm alarm = GetSUT();
		assertThat(alarm.getDayMaskAsValueString(values7), is("1,2,3,4,5,6,7"));		
	}

	@Test
	public void Day_Mask_As_Value_String_Nothing_Selected() {
		Alarm alarm = GetSUT();
		alarm.setDayMask(0x0);
		assertThat(alarm.getDayMaskAsValueString(values7), is(""));		
	}

	@Test
	public void Day_Mask_As_Value_String_Some_Selected() {
		Alarm alarm = GetSUT();
		alarm.setDayMask(0x6F);
		assertThat(alarm.getDayMaskAsValueString(values7), is("1,2,3,4,6,7"));		
	}

	@Test
	public void Hour_Mask_As_Value_String() {
		Alarm alarm = GetSUT();
		assertThat(alarm.getHourMaskAsValueString(values24), is("7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23"));		
	}

	@Test
	public void Hour_Mask_As_Value_String_Nothing_Selected() {
		Alarm alarm = GetSUT();
		alarm.setHourMask(0x00);
		assertThat(alarm.getHourMaskAsValueString(values24), is(""));		
	}
	@Test
	public void Hour_Mask_As_Value_String_Some_selected() {
		Alarm alarm = GetSUT();
		alarm.setHourMask(0x6FFFC0);
		assertThat(alarm.getHourMaskAsValueString(values24), is("7,8,9,10,11,12,13,14,15,16,17,18,19,20,22,23"));		
	}
}
