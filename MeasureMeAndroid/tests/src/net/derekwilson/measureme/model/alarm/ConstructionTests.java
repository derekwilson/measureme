package net.derekwilson.measureme.model.alarm;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.util.Calendar;

import net.derekwilson.measureme.AndroidTestRunner;
import net.derekwilson.measureme.model.Alarm;
import net.derekwilson.measureme.model.Interval;

import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(AndroidTestRunner.class)
public class ConstructionTests extends BaseAlarmTester {
	@Test
	public void Ensure_New_Alarm_Has_Good_StartTime() {
		Alarm alarm = GetSUT();
		Calendar startTime = alarm.getStartTime();
		assertThat(startTime.get(Calendar.SECOND), is(0));		
		assertThat(startTime.get(Calendar.MINUTE), is(0));		
		assertThat(startTime.get(Calendar.MILLISECOND), is(0));		
		assertThat(startTime.get(Calendar.HOUR), is(8));		
	}
	
	@Test
	public void Ensure_New_Alarm_Has_Good_Stride() {
		Alarm alarm = GetSUT();
		Interval interval = alarm.getInterval();
		assertThat(interval.getStride(), is(Interval.STRIDE_DAY));		
		assertThat(interval.getValue(), is(1));		
	}
	
	@Test
	public void Ensure_New_Alarm_Has_Good_Formatted_Starttime() {
		Alarm alarm = GetSUT();
		assertThat(alarm.getStartTimeFormatted(), is("Thu 17 Apr at 20:00"));		
	}
}
