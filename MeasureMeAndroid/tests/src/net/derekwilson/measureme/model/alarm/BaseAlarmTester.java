package net.derekwilson.measureme.model.alarm;

import java.util.Calendar;

import net.derekwilson.measureme.model.Alarm;

public class BaseAlarmTester {
	protected Alarm GetSUT() {
		return new Alarm(GetNow());
	}
	
	protected Calendar GetNow() {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.AM_PM,2);
		cal.set(Calendar.YEAR,2014);
		cal.set(Calendar.MONTH,3);
		cal.set(Calendar.DAY_OF_MONTH,16);
		cal.set(Calendar.HOUR,20);
		cal.set(Calendar.MINUTE,15);
		return cal;
	}
}
