package net.derekwilson.measureme;

import java.io.File;

import org.junit.runners.model.InitializationError;
import org.robolectric.AndroidManifest;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;
import org.robolectric.res.Fs;

public class AndroidTestRunner extends RobolectricTestRunner {

	public AndroidTestRunner(Class<?> testClass) throws InitializationError {
		super(testClass);
	}

	// The name of the Android manifest file.
	private static final String MANIFEST_FILE_NAME = "AndroidManifest.xml";

	// The name of the Android resource folder.
	private static final String RES_FOLDER_NAME = "res";
	
	// the name of the Android assets folder
	private static final String ASSET_FOLDER_NAME = "assets";

	// Find the root of the Android project. This depends on how the unit test runner
	// is executed: from within an IDE, or from the Ant build file. If it's executed
	// from within an IDE (so from inside the `test/` directory), the root is located
	// one directory "up" (`..`), and when executed from the Ant build file, it's just
	// the present working dir: `.`
	private static final File ROOT_ANDROID_PROJECT = new File(MANIFEST_FILE_NAME).exists() ? new File(".") : new File("..");

	//The manifest file of the Android project that is to be tested.
	public static final File MANIFEST_ANDROID_PROJECT = new File(ROOT_ANDROID_PROJECT, MANIFEST_FILE_NAME);

	//The resource folder of the Android project that is to be tested.
	public static final File RES_FOLDER_ANDROID_PROJECT = new File(ROOT_ANDROID_PROJECT, RES_FOLDER_NAME);

	//The asset folder of the Android project that is to be tested.
	public static final File ASSET_FOLDER_ANDROID_PROJECT = new File(ROOT_ANDROID_PROJECT, ASSET_FOLDER_NAME);

	@Override protected AndroidManifest getAppManifest(Config config) {
		return createAppManifest(
				Fs.fileFromPath(MANIFEST_ANDROID_PROJECT.getPath()),
				Fs.fileFromPath(RES_FOLDER_ANDROID_PROJECT.getPath()),
				Fs.fileFromPath(ASSET_FOLDER_ANDROID_PROJECT.getPath())
				);
	}
}
