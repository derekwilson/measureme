package net.derekwilson.measureme.model;

import android.annotation.SuppressLint;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class RecordedMeasurement {
	private Measurement measurement;
	private Calendar timeRecorded;
	private long measurementScaleValue;
	private String label;
	private String note;
	private int displayOrder;

	public Measurement getMeasurement() {
		return measurement;
	}

	public void setMeasurement(Measurement measurement) {
		this.measurement = measurement;
	}
	
	public long getMeasurementScaleValue() {
		return measurementScaleValue;
	}

	public void setMeasurementScaleValueId(long value) {
		this.measurementScaleValue = value;
	}
	
	public Calendar getRecordedTime() {
		return timeRecorded;
	}

	public void setRecordedTime(Calendar time) {
		this.timeRecorded = time;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public int getDisplayOrder() {
		return displayOrder;
	}

	public void setDisplayOrder(int displayOrder) {
		this.displayOrder = displayOrder;
	}

	public String getHeaders() {
		return "Time,Value,MeasureId,Label,Note,DisplayOrder";
	}

	// Will be used to render the recorded measurement
	@SuppressLint("SimpleDateFormat")
	@Override
	public String toString() {
	    StringBuffer buffer = new StringBuffer();

		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		buffer.append(format.format(this.timeRecorded.getTime()));
		buffer.append(",");
		buffer.append(this.measurementScaleValue);
		buffer.append(",");
		if (this.measurement != null) {
			buffer.append(this.measurement.getId());
		} else {
			buffer.append("-1");
		}
		buffer.append(",\"");
		if (this.measurement != null) {
			buffer.append(this.measurement.getName());
			buffer.append(": ");
		}
		buffer.append(this.label);
		buffer.append("\",\"");
		buffer.append(this.note);
		buffer.append("\"");
		buffer.append(",");
		buffer.append(this.displayOrder);

		return buffer.toString();
	}	
}
