package net.derekwilson.measureme.activity.settings;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import com.google.inject.Inject;

import net.derekwilson.measureme.R;
import net.derekwilson.measureme.logging.ILoggerFactory;
import android.content.Context;
import android.os.Bundle;
import android.widget.TextView;
import roboguice.activity.RoboActivity;
import roboguice.inject.InjectView;

public class OpenSourceLicensesActivity extends RoboActivity {

	@Inject
    private ILoggerFactory logger;

	@InjectView(R.id.txtAndroiSupportLibraryLicense)
	private TextView v7license;

	@InjectView(R.id.txtRoboguiceLicense)
	private TextView roboguicelicense;

	@InjectView(R.id.txtSlf4jLicense)
	private TextView slf4jlicense;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_open_source_licenses);

		logger.getCurrentApplicationLogger().debug("OSLicensesActivity.onCreate()");
		
		getTextFromFile(this.getApplicationContext(),"license/roboguice.txt",roboguicelicense);
		getTextFromFile(this.getApplicationContext(),"license/androidsupportlibraryv7.txt",v7license);
		getTextFromFile(this.getApplicationContext(),"license/slf4j.txt",slf4jlicense);
	}
	
	private void getTextFromFile(Context context, String filename, TextView view) {

		InputStream inputStream = null;
		BufferedReader reader = null;
	    StringBuffer buf = new StringBuffer();
		try {
			inputStream = context.getAssets().open(filename);
			logger.getCurrentApplicationLogger().debug("Opened {} Available {}", filename, inputStream.available());
			reader = new BufferedReader(new InputStreamReader(inputStream));

			String str;
			if (inputStream != null) {							
				while ((str = reader.readLine()) != null) {	
					buf.append(str + "\n");
				}				
			}		
		} catch (IOException e) {
			logger.getCurrentApplicationLogger().error("Error reading {}", filename, e);
		}
		finally {
			try {
				if (reader != null) {
					reader.close();
				}
				if (inputStream != null) {
					inputStream.close();
				}
			} catch (IOException e) {
				logger.getCurrentApplicationLogger().error("Error closing {}", filename, e);
			}
		}
		
		view.setText(buf.toString());
	}
}
