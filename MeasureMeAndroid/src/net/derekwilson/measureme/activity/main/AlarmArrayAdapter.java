package net.derekwilson.measureme.activity.main;

import java.util.Calendar;
import java.util.List;

import com.google.inject.Inject;

import net.derekwilson.measureme.R;
import net.derekwilson.measureme.model.Alarm;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class AlarmArrayAdapter extends ArrayAdapter<Alarm> {
	private final Context context;
	private final List<Alarm> values;

	@Inject
	public AlarmArrayAdapter(Context context, List<Alarm> alarms) {
		super(context, R.layout.list_item_alarm, alarms);
		this.context = context;
		this.values = alarms;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = inflater.inflate(R.layout.list_item_alarm, parent, false);

		Alarm thisAlarm = values.get(position);
		
		TextView label = (TextView) rowView.findViewById(R.id.alarm_row_label);
		label.setText("Every " + thisAlarm.getInterval().getValue() +
					" " + thisAlarm.getInterval().getStrideText(this.context.getResources())
				);

		TextView subLabel = (TextView) rowView.findViewById(R.id.alarm_row_sub_label);
		subLabel.setText("Measure " + thisAlarm.getMeasurementsNames());
		
		TextView status = (TextView) rowView.findViewById(R.id.alarm_row_status);
		// TODO work out how to inject an ISystemTime
		status.setText("Next " + thisAlarm.getNextTriggerTimeFormatted(Calendar.getInstance()));
		
		return rowView;
	}
}
