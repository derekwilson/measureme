package net.derekwilson.measureme.activity.showAlarm;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.google.inject.Inject;

import roboguice.RoboGuice;

import net.derekwilson.measureme.R;
import net.derekwilson.measureme.logging.ILoggerFactory;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TableLayout;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

public class SeekbarWithIntervals extends LinearLayout {
	@Inject
    private ILoggerFactory logger;
	
	private List<String> DisplayValues;
	private RelativeLayout IntervalsParent = null;
	private SeekBar Seekbar = null;
	private TextView CurrentDisplayValue = null;

	private boolean ProgressUpdatedByUser = false;
	
	private int WidthMeasureSpec = 0;
	private int HeightMeasureSpec = 0;

	public SeekbarWithIntervals(Context context) {
		super(context);
		inflateControls(context);
		// this will inject non view dependencies
		RoboGuice.getInjector(context.getApplicationContext()).injectMembers(this);		
	}

	public SeekbarWithIntervals(Context context, AttributeSet attributeSet) {
		super(context, attributeSet);
		inflateControls(context);
		// this will inject non view dependencies
		RoboGuice.getInjector(context.getApplicationContext()).injectMembers(this);		
	}
	
	private Activity getActivity() {
		return (Activity) getContext();
	}

	private void inflateControls(Context context) {
		setOrientation(LinearLayout.VERTICAL);
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		inflater.inflate(R.layout.control_seekbar_with_intervals, this, true);
	}
	
	@Override
	protected void onLayout(boolean changed, int l, int t, int r, int b) {
		super.onLayout(changed, l, t, r, b);

		if (changed) {			
			alignIntervals();

			// We've changed the intervals layout, we need to refresh.
			IntervalsParent.measure(WidthMeasureSpec, HeightMeasureSpec);
			IntervalsParent.layout(IntervalsParent.getLeft(), IntervalsParent.getTop(), IntervalsParent.getRight(), IntervalsParent.getBottom());
		}
	}

	private void alignIntervals() {
		int widthOfSeekbarThumb = getSeekbarThumbWidth();
		
		int thumbOffset = widthOfSeekbarThumb / 2;

		int widthOfSeekbar = getSeekbar().getWidth();
		int numberOfIntervals = getSeekbar().getMax();
		int stepSize = (widthOfSeekbar-(2*thumbOffset)) / numberOfIntervals; 
		
		logger.getCurrentApplicationLogger().debug("alignIntervals, thumb offset {}, number {}, stepSize {}, seekbarWidth {}",
							thumbOffset, numberOfIntervals, stepSize, widthOfSeekbar);

		alignFirstInterval(thumbOffset);
		alignIntervalsInBetween(thumbOffset, stepSize);
		alignLastInterval(thumbOffset, widthOfSeekbar);
	}
	
	private int getSeekbarThumbWidth() {
		// this is the real way to do it but it requires API 16
		// return Seekbar.getThumb().getMinimumWidth();
		return getResources().getDimensionPixelOffset(R.dimen.seekbar_thumb_width);
	}
	
	private void alignFirstInterval(int offset) {
		TextView firstInterval = (TextView) getIntervalsParent().getChildAt(0);
		logger.getCurrentApplicationLogger().debug("alignFirstInterval, left padding {}", offset);
		firstInterval.setPadding(offset, 0, 0, 0);
	}

	private void alignIntervalsInBetween(int offset, int stepSize) {
		// Don't align the first or last interval.
		for (int index = 1; index < (getIntervalsParent().getChildCount() - 1); index++) {
			TextView textViewInterval = (TextView) getIntervalsParent().getChildAt(index);
			int widthOfText = textViewInterval.getWidth();
  	
			// This works out how much left padding is needed to center the current interval.
			int leftPadding = Math.round((stepSize * index) - (widthOfText / 2) + (offset));
			logger.getCurrentApplicationLogger().debug("alignIntervalsInBetween, interval {}, left padding {}, widthOfText {}",index, leftPadding, widthOfText);
			textViewInterval.setPadding(leftPadding, 0, 0, 0);
		}
	}
	
	private void alignLastInterval(int offset, int widthOfSeekbar) {
		int lastIndex = getIntervalsParent().getChildCount() - 1;
		TextView lastInterval = (TextView) getIntervalsParent().getChildAt(lastIndex);
		int widthOfText = lastInterval.getWidth();
		
		// right align at the last interval position
		int leftPadding = Math.round(widthOfSeekbar - offset - widthOfText);
		logger.getCurrentApplicationLogger().debug("alignLastInterval, left padding {}", leftPadding);
		lastInterval.setPadding(leftPadding, 0, 0, 0);
	}
	
	protected synchronized void onMeasure(final int widthMeasureSpec, final int heightMeasureSpec) {
		WidthMeasureSpec = widthMeasureSpec;
		HeightMeasureSpec = heightMeasureSpec;
	  
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
	}
	
	public int getProgress() {
		return getSeekbar().getProgress();
	}
	
	public int getProgressIfSetByUser() {
		if (ProgressUpdatedByUser) {
			return getSeekbar().getProgress();
		}
		return -1;
	}
	
	public void setProgress(int progress) {
		getSeekbar().setProgress(progress);
	}
	
	public void setIntervals(List<String> values, List<String> display, boolean showIntervals) {
		DisplayValues = new ArrayList<String>(display);
		displayIntervals(values);
		getSeekbar().setMax(values.size() - 1);
		
		getSeekbar().setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

		    @Override
		    public void onStopTrackingTouch(SeekBar seekBar) {
		    }

			@Override
			public void onProgressChanged(SeekBar seekBar,  int progress, boolean fromUser) {
				if (fromUser && progress >= 0 && progress < DisplayValues.size()) {
					getCurrentDisplayValue().setText(DisplayValues.get(progress));
					ProgressUpdatedByUser = true;
				}
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
			}
		});
	}
	
	public void setPrompt(String promptText, boolean showPrompt) {
		TextView prompt = (TextView) findViewById(R.id.prompt);
		prompt.setText(promptText);
	}
	
	private void displayIntervals(List<String> intervals) {
		int idOfInterval = 1;
		RelativeLayout parent = getIntervalsParent();
		
		if (getIntervalsParent().getChildCount() == 0) {
			for (String interval : intervals) {
				TextView textViewInterval = createInterval(interval,idOfInterval);
				parent.addView(textViewInterval);
				idOfInterval++;
		  }
		}
	}

	private TextView createInterval(String interval, int idOfInterval) {
		TextView textView = new TextView(getContext());
		textView.setId(idOfInterval);
		textView.setText(interval);
		textView.setTextAppearance(getContext(), android.R.style.TextAppearance_Small);

		RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
		
		if (idOfInterval > 1) {
			params.addRule(RelativeLayout.RIGHT_OF, idOfInterval);
		}
		
		textView.setLayoutParams(params);

		return textView;
	}
	
	public void setOnSeekBarChangeListener(OnSeekBarChangeListener onSeekBarChangeListener) {
		getSeekbar().setOnSeekBarChangeListener(onSeekBarChangeListener);
	}

	private RelativeLayout getIntervalsParent() {
		if (IntervalsParent == null) {
			IntervalsParent = (RelativeLayout) findViewById(R.id.intervals);
		}
		return IntervalsParent;
	}
	
	private SeekBar getSeekbar() {
		if (Seekbar == null) {
			Seekbar = (SeekBar) findViewById(R.id.seekbar);
		}
		return Seekbar;
	}

	private TextView getCurrentDisplayValue() {
		if (CurrentDisplayValue == null) {
			CurrentDisplayValue = (TextView) findViewById(R.id.displayValue);
		}
		return CurrentDisplayValue;
	}
}