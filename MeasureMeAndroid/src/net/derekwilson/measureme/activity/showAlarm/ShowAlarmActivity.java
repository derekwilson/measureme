package net.derekwilson.measureme.activity.showAlarm;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.google.inject.Inject;

import roboguice.activity.RoboActivity;
import roboguice.inject.InjectView;
import net.derekwilson.measureme.AlarmService;
import net.derekwilson.measureme.R;
import net.derekwilson.measureme.activity.main.MainActivity;
import net.derekwilson.measureme.data.interfaces.IAlarmRepository;
import net.derekwilson.measureme.data.interfaces.IMeasurementRepository;
import net.derekwilson.measureme.data.interfaces.IRecordedMeasurementWriter;
import net.derekwilson.measureme.data.interfaces.IRepositoryAction;
import net.derekwilson.measureme.logging.ILoggerFactory;
import net.derekwilson.measureme.model.Alarm;
import net.derekwilson.measureme.model.Measurement;
import net.derekwilson.measureme.model.MeasurementScaleValue;
import net.derekwilson.measureme.model.RecordedMeasurement;
import net.derekwilson.measureme.model.Measurement.RenderType;
import net.derekwilson.measureme.utility.IPreferencesHelper;
import net.derekwilson.measureme.utility.ISystemTime;
import android.content.Intent;
import android.content.res.Resources;
import android.media.Ringtone;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class ShowAlarmActivity extends RoboActivity implements OnClickListener {

	enum AlarmState {
		New,		// the alarm has not been displayed yet
		Rang,		// the alarm has run but the measurement has not been taken yet
		Dismissed	// measurement has been taken and the alarm dismissed
	}
	
	class MeasurementType {
		public AlarmState state;
		public boolean alarm;
		public long alarmid;
		public boolean measurementsContainsASlider;
		public long[] measurementids;
	}

	@Inject
    private ILoggerFactory logger;

	@Inject
    private IPreferencesHelper preferences;

	@Inject
    private ISystemTime systemTime;

	@Inject
	private IAlarmRepository alarmRepo;

	@Inject
	private IMeasurementRepository measurmentRepo;

	@Inject
	private IRecordedMeasurementWriter recordedMeasurementWriter;
	
	private Alarm _alarm;
	private Measurement _measure;
	private Ringtone _ringtone;

	public static final String SCALE_GROUP_TAG = "MeasureMe.ScaleGroup";
	public static final String SCALE_GROUP2_TAG = "MeasureMe.ScaleGroup2";
	public static final String SCALE_BUTTON_TAG = "MeasureMe.ScaleButton";
	public static final String SCALE_SLIDER_TAG = "MeasureMe.ScaleSlider";

	@InjectView(R.id.txtNote)
	private TextView txtNote;

	/************* DB *************************/
	
	private void LoadAlarm(final long id)
	{
		alarmRepo.executeInOpenContext(new IRepositoryAction () {
			@Override public void execute() {
				_alarm = alarmRepo.getById(id,true);
			}
		});
	}

	private void LoadMeasurement(final long id)
	{
		measurmentRepo.executeInOpenContext(new IRepositoryAction () {
			@Override public void execute() {
				_measure = measurmentRepo.getById(id,true);
			}
		});
	}

	/*************** Render ***************************/

	private void RenderSingleButton(MeasurementScaleValue scaleValue, boolean hasMultipleMeasures, LinearLayout parent) {
		Button btnScaleValue = null;
		if (hasMultipleMeasures) {
			btnScaleValue = new RadioButton(this);
		}
		else {
	        btnScaleValue = new Button(this);
		}
        btnScaleValue.setText(scaleValue.getDisplayText());
        btnScaleValue.setId((int) scaleValue.getId());	// the DB ID is unique
        btnScaleValue.setTag(SCALE_BUTTON_TAG);
        btnScaleValue.setOnClickListener(this);

        TableRow.LayoutParams itemParams = new TableRow.LayoutParams(TableRow.LayoutParams.FILL_PARENT, TableRow.LayoutParams.FILL_PARENT, 1f);

        btnScaleValue.setLayoutParams(itemParams);
        parent.addView(btnScaleValue);
	}

	private void RenderSingleSlider(MeasurementScaleValue scaleValue, LinearLayout parent) {
		logger.getCurrentApplicationLogger().debug("RenderSingleSlider, With {} scale values", scaleValue.getValue());

		SeekbarWithIntervals control = new SeekbarWithIntervals(this);

		Resources res = getResources();
		String[] values = res.getStringArray(R.array.slider_scale_value_array);
		String[] display = res.getStringArray(R.array.slider_scale_display_array);
		
		control.setPrompt(scaleValue.getDisplayText(),true);
		control.setId((int) scaleValue.getId());	// the DB ID is unique
		control.setTag(SCALE_SLIDER_TAG);
		control.setIntervals(Arrays.asList(values),Arrays.asList(display),true);
		control.setProgress(0);
		
		parent.addView(control);
	}
	
	private void RenderSingleColumnMeasurementScale(LinearLayout layout, Measurement measure, boolean hasMultipleMeasures) {
		RadioGroup radioGroup = null;
        if (hasMultipleMeasures) {
        	// if we are laying out multiple scales then we dont want to use buttons
        	// we want to use radio buttons and each scale needs its own group
        	radioGroup = new RadioGroup(this);
        	radioGroup.setTag(SCALE_GROUP_TAG);
	        layout.addView(radioGroup);
        }

        for (MeasurementScaleValue scaleValue : measure.getScale()) {
        	// we can either add the button straight to the layout or to the radio group
        	RenderSingleButton(scaleValue,hasMultipleMeasures,hasMultipleMeasures ? radioGroup : layout);
		}
	}
	
	private void RenderTwoColumnMeasurementScale(LinearLayout layout, Measurement measure, boolean hasMultipleMeasures) {
		// just for simplicity we are going to put the table down for the 2 column layout for buttons and radiobuttons
		// its just simpler and the Buttons dont mind the unneeded RadioGroupTableLayout

		// we need to use the old layout
		@SuppressWarnings("deprecation")
		TableLayout.LayoutParams tableParams = new TableLayout.LayoutParams(TableLayout.LayoutParams.FILL_PARENT, TableLayout.LayoutParams.FILL_PARENT, 1f);
		@SuppressWarnings("deprecation")
		TableLayout.LayoutParams rowParams = new TableLayout.LayoutParams(TableLayout.LayoutParams.FILL_PARENT, TableLayout.LayoutParams.FILL_PARENT,1f);

		RadioGroupTableLayout radioGroup = null;
		radioGroup = new RadioGroupTableLayout(this);
		radioGroup.setTag(SCALE_GROUP2_TAG);
		radioGroup.setLayoutParams(tableParams);
		layout.addView(radioGroup);
        
        for (int scaleValueIndex=0; scaleValueIndex<measure.getScale().size();) {
        	TableRow row = new TableRow(this);
        	row.setLayoutParams(rowParams);

        	MeasurementScaleValue scaleValue = measure.getScale().get(scaleValueIndex++);
        	RenderSingleButton(scaleValue,hasMultipleMeasures,row);

        	if (measure.getRenderType() == Measurement.RenderType.TwoColumn) {
        		if (scaleValueIndex<measure.getScale().size()) {
                	scaleValue = measure.getScale().get(scaleValueIndex++);
                	RenderSingleButton(scaleValue,hasMultipleMeasures,row);
            	}
        	}

        	radioGroup.addView(row);
		}
	}

	private void RenderSliderMeasurementScale(LinearLayout layout, Measurement measure) {
        for (MeasurementScaleValue scaleValue : measure.getScale()) {
        	RenderSingleSlider(scaleValue,layout);
		}
	}
	
	private void RenderMeasurementScale(LinearLayout layout, Measurement measure, boolean hasMultipleMeasures) {
		logger.getCurrentApplicationLogger().debug("RenderMeasurement Name: {}, With {} scale values", measure.getName(), measure.getScale().size());

		TextView txtTitle = new TextView(this);
		txtTitle.setText("Measure " + measure.getName());
        layout.addView(txtTitle);

		switch (measure.getRenderType()) {
		case TwoColumn:
			RenderTwoColumnMeasurementScale(layout,measure,hasMultipleMeasures);
			break;
		case Slider:
			RenderSliderMeasurementScale(layout,measure);
			break;
		default:
			RenderSingleColumnMeasurementScale(layout,measure,hasMultipleMeasures);
			break;
		}
	}

	private void KillAlarmInManager(long id) {
		// we need to remove the pending intent so the broken alarms stops ringing
    	Intent operation = new Intent(this, AlarmService.class);
    	operation.putExtra("alarmId",id);
    	operation.setAction(AlarmService.CANCEL);
        startService(operation);
	}
	
	private void SetupDismissButton(boolean multipleInputs) {
		Button dismiss = (Button) findViewById(R.id.dismissAlarm);
		String buttonString;
		if (multipleInputs) {
			buttonString = getResources().getString(R.string.done_alarm);
		}
		else {
			buttonString = getResources().getString(R.string.dismiss_alarm);
		}
		dismiss.setText(buttonString);
	}
	
	private void RenderAlarm(AlarmState state,final long id) {
        logger.getCurrentApplicationLogger().debug("RenderAlarm id {}, state {}", id, state);

		if (!preferences.getAlarmsEnabled()) {
			// belt and braces 
			// for some reason the alarm has been triggered despite them being switched off
	        logger.getCurrentApplicationLogger().warn("ShowAlarmActivity alarm triggered when they are off id {}", id);
	        KillAlarmInManager(id);
	        // ignore alarm
			finish();
			return;
		}

		LoadAlarm(id);

		if (_alarm == null) {
	        logger.getCurrentApplicationLogger().debug("ShowAlarmActivity cannot find alarm with id {}", id);

	        KillAlarmInManager(id);

			// if the alarm data is damaged in the DB lets try and delete it
			alarmRepo.executeInOpenContext(new IRepositoryAction () {
				@Override public void execute() {
				    alarmRepo.deleteById(id);
				}
			});

			finish();
			return;
		}

		if (_alarm.getMeasurements() == null || _alarm.getMeasurements().size() < 1) {
			logger.getCurrentApplicationLogger().debug("ShowAlarmActivity no measurments specified for alarm id {}", id);

			finish();
			return;
		}

		SetupDismissButton(_alarm.needsMultipleInputs());
		LinearLayout layout = (LinearLayout) findViewById(R.id.layButtonParent);
		// remove any existing layout
		layout.removeAllViews();
		
		for (Measurement measure : _alarm.getMeasurements()) {
			RenderMeasurementScale(layout,measure,_alarm.hasMultipleMeasurements());
		}
		if (state == AlarmState.New) {
			RenderNonvisualPrompt();
		}
	}

	private void RenderMeasurments(AlarmState state,long[] measurementIds) {
		boolean needsMultipleInputs = false;
		if (measurementIds.length > 1) {
			needsMultipleInputs = true;
		}

		LinearLayout layout = (LinearLayout) findViewById(R.id.layButtonParent);
		// remove any existing layout
		layout.removeAllViews();

		for (long id : measurementIds) {
			LoadMeasurement(id);
			if (_measure == null) {
				logger.getCurrentApplicationLogger().debug("ShowAlarmActivity cannot find measurement with id {}", id);
				finish();
				return;
			}
			if (_measure.getRenderType() == RenderType.Slider) {
				needsMultipleInputs = true;
			}
			RenderMeasurementScale(layout,_measure,measurementIds.length > 1);
			//RenderNonvisualPrompt();
		}
		SetupDismissButton(needsMultipleInputs);
	}

	private void RenderNonvisualPrompt() {
		_ringtone = preferences.getAlarmSound(false);
		if (_ringtone != null) {
			_ringtone.play();
		}
		preferences.AlarmVibrate();
	}
	
	private void Render(MeasurementType measurement) {
		if (measurement.alarm) {
			RenderAlarm(measurement.state,measurement.alarmid);
		}
		else  {
			RenderMeasurments(measurement.state,measurement.measurementids);
		}
		SetState(AlarmState.Rang);
	}

	/*********** Lifecycle ******************************/
	
	MeasurementType ProcessIntent(Intent intent) {
		String measurmentIds = intent.getStringExtra(MainActivity.ACTIVITY_PARAM_MEASUREMENT_IDS);
		logger.getCurrentApplicationLogger().debug("ProcessIntent, MeasurmentId: {}", measurmentIds);

		long alarmId = intent.getLongExtra(MainActivity.ACTIVITY_PARAM_ALARM_ID, 0);
		logger.getCurrentApplicationLogger().debug("ProcessIntent, AlarmId {}", alarmId);

		int state = intent.getIntExtra(MainActivity.ACTIVITY_PARAM_STATE, 0);
		logger.getCurrentApplicationLogger().debug("ProcessIntent, State {}", state);

		MeasurementType returnValue = new MeasurementType();
		returnValue.alarm = true;
		returnValue.alarmid = -1;
		returnValue.state = AlarmState.New;
		
		if (alarmId != 0) {
			returnValue.alarm = true;
			returnValue.alarmid = alarmId;
		} else if (measurmentIds != null) {
			String[] ids = measurmentIds.trim().split(",");
			returnValue.alarm = false;
			returnValue.measurementids = new long[ids.length];
			int index = 0;
			for (String id : ids) {
				returnValue.measurementids[index++] = Long.parseLong(id);
			}
		}

		switch (state) {
		case 1:
			returnValue.state = AlarmState.Rang;
			break;
		case 2:
			returnValue.state = AlarmState.Dismissed;
			break;
		default:
			returnValue.state = AlarmState.New;
			break;
		}
		
		return returnValue;
	}

	private void SetState(AlarmState newState) {
		Intent currentIntent = getIntent();
		int state = 0;
		switch (newState) {
		case New:
			state = 0;
			break;
		case Rang:
			state = 1;
			break;
		case Dismissed:
			state = 2;
			break;
		}
		currentIntent.putExtra(MainActivity.ACTIVITY_PARAM_STATE, state);
		setIntent(currentIntent);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		logger.getCurrentApplicationLogger().debug("ShowAlarmActivity.onCreate()");
		MeasurementType measurementType = ProcessIntent(getIntent());
		if (measurementType.state == AlarmState.Dismissed) {
			logger.getCurrentApplicationLogger().debug("intent has already been dismissed");
			finish();
		}

		setContentView(R.layout.activity_show_alarm);
		Render(measurementType);
	}

    @Override
    protected void onResume() {
        super.onResume();
		logger.getCurrentApplicationLogger().debug("ShowAlarmActivity.onResume()");
    }    

    @Override
    protected void onPause() {
        super.onPause();
		logger.getCurrentApplicationLogger().debug("ShowAlarmActivity.onPause()");
    }    

	@Override
	public void onDestroy() {
		logger.getCurrentApplicationLogger().debug("ShowAlarmActivity.onDestroy()");
		if (_ringtone != null) {
			_ringtone.stop();
		}
		super.onDestroy();
	}
    
	@Override
	public void onAttachedToWindow() {
		logger.getCurrentApplicationLogger().debug("ShowAlarmActivity - setting window flags");
		int flags = WindowManager.LayoutParams.FLAG_FULLSCREEN | 
	            WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED | 
	            WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON;
	    this.getWindow().setFlags(flags, flags);
	}

	@Override
	protected void onNewIntent (Intent intent) {
		super.onNewIntent(intent);
		logger.getCurrentApplicationLogger().debug("ShowAlarmActivity.onNewIntent()");
		MeasurementType originalParameter = ProcessIntent(getIntent());
		MeasurementType thisParameter = ProcessIntent(intent);

		if (thisParameter.state == AlarmState.Dismissed) {
			logger.getCurrentApplicationLogger().debug("new intent has already been dismissed");
			finish();
		}
		
		RecordSkip(originalParameter);
		
		if (originalParameter.alarm == thisParameter.alarm &&
				originalParameter.alarmid == thisParameter.alarmid) {
			// cannot do multiple MeasureNow requests so we do not need to check for them
			logger.getCurrentApplicationLogger().debug("new intent is the same as the old one");
			return;
		}
		
		// the new alarm is different from the old one we need to rerender
		setIntent(intent);
		Render(ProcessIntent(getIntent()));
	}

	/***************** Record measurement ************************/
	
	public void onClick(View view) {
		if (_ringtone != null) {
			_ringtone.stop();
		}
		
		MeasurementType measureType = ProcessIntent(getIntent());
		boolean multipleInputs = false;
		String dismissString = getResources().getString(R.string.dismiss_alarm);
		Button dismiss = (Button) findViewById(R.id.dismissAlarm);
		if (!dismissString.equals(dismiss.getText())) {
			// we have replaced the text in the button so it must need multiple inputs
			logger.getCurrentApplicationLogger().debug("onClick - multipleInputs");
			multipleInputs = true;
		}
		
		switch(view.getId()) {
			case R.id.dismissAlarm:
				if (multipleInputs) {
					// we need to record any checked items
					LinearLayout layout = (LinearLayout) findViewById(R.id.layButtonParent);
					int maxGroups = layout.getChildCount();
					for (int groupIndex = 0; groupIndex < maxGroups; groupIndex++) {
						View thisGroup = layout.getChildAt(groupIndex);
						int selectedControlId = -1;
						int selectedValue = -1;
						boolean hasValue = false;	// true if the selection can have multiple values (a slider) false if its just a button
						if (SCALE_GROUP_TAG.equals(thisGroup.getTag())) {
							// we have found a group
							selectedControlId = ((RadioGroup) thisGroup).getCheckedRadioButtonId();
						}
						else if (SCALE_GROUP2_TAG.equals(thisGroup.getTag())) {
							// we have found a multi column group
							selectedControlId = ((RadioGroupTableLayout) thisGroup).getCheckedRadioButtonId();
						}
						else if (SCALE_SLIDER_TAG.equals(thisGroup.getTag())) {
							// a slider
							selectedControlId = thisGroup.getId();
							selectedValue = ((SeekbarWithIntervals) thisGroup).getProgressIfSetByUser();
							hasValue = true;
						}

						if (selectedControlId != -1) {
							if (_alarm != null) {
								RecordAlarmMeasurement(selectedControlId, hasValue, selectedValue);
							}
							else {
								RecordNowMeasurement(selectedControlId, hasValue, selectedValue, measureType.measurementids);
							}
						}
					}
				}
				logger.getCurrentApplicationLogger().debug("ShowAlarmActivity - dismissing");
				SetState(AlarmState.Dismissed);
				finish();
				break;
		}
		if (SCALE_BUTTON_TAG.equals(view.getTag())) {
			// we only process and exit for single measurements
			// this can never be a slider as they always require multiple inputs
			if (!multipleInputs) {
				if (_alarm != null) {
					RecordAlarmMeasurement(view.getId(),false,-1);
				}
				else {
					RecordNowMeasurement(view.getId(),false,-1,measureType.measurementids);
				}
				SetState(AlarmState.Dismissed);
				finish();
			}
		}
	}

	private void RecordSkip(MeasurementType type) {
		if (!type.alarm || _alarm == null) {
			logger.getCurrentApplicationLogger().debug("RecordSkip: cannot record as there is no alarm");
			return;
		}
		List<Measurement> measurements = _alarm.getMeasurements();
		if (measurements == null || measurements.size() < 1) {
			logger.getCurrentApplicationLogger().debug("RecordSkip: cannot record as there are no measurements alarm id {}",_alarm.getId());
			return;
		}
		
		for (Measurement measure : measurements) {
			logger.getCurrentApplicationLogger().debug("RecordSkip: {}",measure.getName());
			RecordedMeasurement data = new RecordedMeasurement();
			data.setMeasurement(measure);
			data.setMeasurementScaleValueId(-1);
			data.setLabel("skipped");
			data.setNote("skipped");
			data.setRecordedTime(systemTime.getCurrentTime());
			recordedMeasurementWriter.Write(data);
		}
	}

	private void RecordNowMeasurement(int buttonId, boolean hasValue, int value, long[] measurementIds) {
		for (long id : measurementIds) {
			LoadMeasurement(id);
			if (_measure == null) {
				logger.getCurrentApplicationLogger().debug("RecordNowMeasurement: cannot find measurement with id {}", id);
			}
			else {
				// try all the measures that we rendered
				RecordMeasurement(_measure,buttonId,hasValue,value);
			}
		}
	}

	private void RecordAlarmMeasurement(int buttonId, boolean hasValue, int value) {
		if (_alarm == null) {
			logger.getCurrentApplicationLogger().debug("RecordAlarmMeasurement: cannot record as there is no alarm");
			return;
		}
		List<Measurement> measurements = _alarm.getMeasurements();
		if (measurements == null || measurements.size() < 1) {
			logger.getCurrentApplicationLogger().debug("RecordAlarmMeasurement: cannot record as there are no measurements alarm id {}",_alarm.getId());
			return;
		}

		for (Measurement measure : measurements) {
			RecordMeasurement(measure,buttonId,hasValue,value);
		}
	}
	
	// see if the button ID is in the measure scale and if it is record it otherwise ignore it
	private void RecordMeasurement(Measurement measure, int buttonId, boolean hasValue, int value) {
		for (MeasurementScaleValue scaleValue : measure.getScale()) {
			if ((int) scaleValue.getId() == buttonId) {
				String measurementNote = txtNote.getText().toString();
				// we have a hit
				logger.getCurrentApplicationLogger().debug("RecordMeasurement: {} {} Value {} {} HasValue {} value {}",measure.getName(),scaleValue.getDisplayText(),scaleValue.getValue(),measurementNote,hasValue,value);
				RecordedMeasurement data = new RecordedMeasurement();
				data.setMeasurement(measure);
				if (hasValue) {
					// take the value from the UI - its a slider
					data.setMeasurementScaleValueId(value);
				}
				else {
					// use the value that is in the DB for this button
					data.setMeasurementScaleValueId(scaleValue.getValue());
				}
				data.setLabel(scaleValue.getDisplayText());
				data.setRecordedTime(systemTime.getCurrentTime());
				data.setNote(measurementNote);
				data.setDisplayOrder(scaleValue.getDisplayOrder());
				recordedMeasurementWriter.Write(data);
			}
		}
	}

}
