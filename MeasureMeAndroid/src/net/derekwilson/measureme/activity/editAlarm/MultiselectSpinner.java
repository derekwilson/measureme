package net.derekwilson.measureme.activity.editAlarm;

import java.util.List;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnMultiChoiceClickListener;
import android.util.AttributeSet;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

public class MultiselectSpinner extends Spinner implements OnMultiChoiceClickListener, OnCancelListener {

	private List<String> items;
	private boolean[] selected_initial;
	private boolean[] selected;
	private String defaultText;
	private MultiselectSpinnerListener listener;

	public MultiselectSpinner(Context context) {
		super(context);
	}

	public MultiselectSpinner(Context arg0, AttributeSet arg1) {
		super(arg0, arg1);
	}

	public MultiselectSpinner(Context arg0, AttributeSet arg1, int arg2) {
		super(arg0, arg1, arg2);
	}

	public void onClick(DialogInterface dialog, int which, boolean isChecked) {
	if (isChecked)
	    selected[which] = true;
	else
	    selected[which] = false;
	}

	public void onCancel(DialogInterface dialog) {
		// restore the original position or the new position if OK was pressed
		System.arraycopy(selected_initial, 0, selected, 0, selected.length);
	}

	private void onOk() {
		// update our state
		System.arraycopy(selected, 0, selected_initial, 0, selected_initial.length);
		UpdateSelectionFromLayout();
		listener.onItemsSelected(this,selected);
	}
	
	private void UpdateSelectionFromLayout() {
		
		// refresh text on spinner
		StringBuffer spinnerBuffer = new StringBuffer();
		boolean someUnselected = false;
		for (int i = 0; i < items.size(); i++) {
			if (selected[i] == true) {
				spinnerBuffer.append(items.get(i));
				spinnerBuffer.append(", ");
			} else {
				someUnselected = true;
			}
		}
		String spinnerText;
		if (someUnselected) {
			spinnerText = spinnerBuffer.toString();
			if (spinnerText.length() > 2)
				spinnerText = spinnerText.substring(0, spinnerText.length() - 2);
			} else {
				spinnerText = defaultText;
		}
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(),
				android.R.layout.simple_spinner_item,
				new String[] { spinnerText });
		setAdapter(adapter);
	}
	
	@Override
	public boolean performClick() {
		// keep a copy of the initial state
		System.arraycopy(selected, 0, selected_initial, 0, selected_initial.length);

		AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
		builder.setMultiChoiceItems(items.toArray(new CharSequence[items.size()]), selected, this);
		builder.setPositiveButton(android.R.string.ok,
				new DialogInterface.OnClickListener() {
	
				@Override
				public void onClick(DialogInterface dialog, int which) {
					onOk();
				}
			});
		builder.setNegativeButton(android.R.string.cancel,
				new DialogInterface.OnClickListener() {
				@Override
            	public void onClick(DialogInterface dialog, int whichButton) {
					onCancel(dialog);
            	}
        	});
		builder.setOnCancelListener(this);
		builder.show();
		return true;
	}

	public void setItemsWithValues(List<String> items, List<String> itemValues, String selectedList, String allText, MultiselectSpinnerListener listener) {
		this.items = items;
		this.defaultText = allText;
		this.listener = listener;

		String spinnerText = allText;

		selected_initial = new boolean[itemValues.size()];
		// Set false by default
		selected = new boolean[itemValues.size()];
		for (int j = 0; j < itemValues.size(); j++) {
			selected[j] = false;
		}

		if (selectedList != null) {
			spinnerText = "";
			// Extract selected items
			String[] selectedItems = selectedList.trim().split(",");

			// Set selected items to true
			for (int i = 0; i < selectedItems.length; i++) {
				for (int j = 0; j < itemValues.size(); j++) {
					if (selectedItems[i].trim().equals(itemValues.get(j))) {
						selected[j] = true;
						spinnerText += (spinnerText.equals("")?"":", ") + items.get(j);
						break;
					}
				}
			}
		}

		boolean someUnselected = false;
		for (int j = 0; j < selected.length; j++) { 
			if (!selected[j]) {
				someUnselected = true;
			}
		}

		// Text for the spinner
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(),
				android.R.layout.simple_spinner_item, new String[] { (someUnselected ? spinnerText : allText) });
		setAdapter(adapter);
	}
	
	public void setItems(List<String> items, List<Boolean> itemsSelected, String allText,MultiselectSpinnerListener listener) {
		this.items = items;
		this.defaultText = allText;
		this.listener = listener;
	
		selected_initial = new boolean[items.size()];
		selected = new boolean[items.size()];

		for (int index = 0; index < selected.length; index++) {
			if (index < itemsSelected.size()) {
				selected[index] = itemsSelected.get(index);
			}
			else {
				selected[index] = false;
				
			}
		}
	
		// all text on the spinner
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(),
				android.R.layout.simple_spinner_item, new String[] { allText });
		setAdapter(adapter);
	}

	public interface MultiselectSpinnerListener {
		public void onItemsSelected(MultiselectSpinner control, boolean[] selected);
	}
}
