package net.derekwilson.measureme.activity.editAlarm;

import java.util.Calendar;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

public class TimePickerFragment extends DialogFragment {
	private TimePickerDialog.OnTimeSetListener listener;
	private Calendar calendar;

    public TimePickerFragment(TimePickerDialog.OnTimeSetListener listener, Calendar calendar) {
        this.listener=listener;
        this.calendar=calendar;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);

        // Create a new instance of TimePickerDialog and return it
        return new TimePickerDialog(getActivity(), listener, hour,minute,true);
    }
}

