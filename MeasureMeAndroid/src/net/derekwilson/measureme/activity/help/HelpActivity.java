package net.derekwilson.measureme.activity.help;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import net.derekwilson.measureme.R;
import net.derekwilson.measureme.logging.ILoggerFactory;
import android.content.Context;
import android.os.Bundle;
import android.text.Html;
import android.widget.TextView;

import com.google.inject.Inject;

import roboguice.activity.RoboActivity;
import roboguice.inject.InjectView;

public class HelpActivity  extends RoboActivity {

	@Inject
    private ILoggerFactory logger;

	@InjectView(R.id.txtHelp)
	private TextView helpTextView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_help);

		logger.getCurrentApplicationLogger().debug("HelpActivity.onCreate()");
		
		getHtmlFromFile(this.getApplicationContext(),"help/help.html",helpTextView);
	}

	private void getHtmlFromFile(Context context, String filename, TextView view) {

		InputStream inputStream = null;
		BufferedReader reader = null;
	    StringBuffer buf = new StringBuffer();
		try {
			inputStream = context.getAssets().open(filename);
			logger.getCurrentApplicationLogger().debug("Opened {} Available {}", filename, inputStream.available());
			reader = new BufferedReader(new InputStreamReader(inputStream));

			String str;
			if (inputStream != null) {							
				while ((str = reader.readLine()) != null) {	
					buf.append(str);
				}				
			}		
		} catch (IOException e) {
			logger.getCurrentApplicationLogger().error("Error reading {}", filename, e);
		}
		finally {
			try {
				if (reader != null) {
					reader.close();
				}
				if (inputStream != null) {
					inputStream.close();
				}
			} catch (IOException e) {
				logger.getCurrentApplicationLogger().error("Error closing {}", filename, e);
			}
		}
		
		view.setText(Html.fromHtml(buf.toString()));
	}
}
