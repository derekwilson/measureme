package net.derekwilson.measureme.data.interfaces;

import net.derekwilson.measureme.model.DisplayValuePairs;
import net.derekwilson.measureme.model.Measurement;

public interface IMeasurementRepository extends IRepository<Measurement> {
	Measurement getById(long id, boolean getScale);
	DisplayValuePairs getAllDisplayValuePairs();
}

