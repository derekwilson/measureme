package net.derekwilson.measureme.data.interfaces;

import java.util.List;

import net.derekwilson.measureme.model.Alarm;

public interface IAlarmRepository extends IRepository<Alarm> {
	Alarm getById(long id, boolean getScale);
	List<Alarm> getAll(boolean getScales);
	Alarm updateWithMeasurement(Alarm item);
}
