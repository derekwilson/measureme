package net.derekwilson.measureme.data.interfaces;



public interface IBaseRepository {
	void open();
	void close();

	void executeInOpenContext(IRepositoryAction iRepositoryAction);
}

