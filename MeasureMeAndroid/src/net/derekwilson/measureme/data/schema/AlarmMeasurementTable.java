package net.derekwilson.measureme.data.schema;

import android.database.sqlite.SQLiteDatabase;

public class AlarmMeasurementTable {
	// Database table
	public static final String TABLE_ALARM_MEASUREMENT = "alarmMeasurement";
	public static final String COLUMN_ID = "_id";
	public static final String COLUMN_ALARM_ID = "alarmId";
	public static final String COLUMN_MEASUREMENT_ID = "measurementId";
	public static final String COLUMN_CREATED = "created";

	// Database creation SQL statement
	private static final String DATABASE_CREATE = "create table " 
		+ TABLE_ALARM_MEASUREMENT
		+ "(" 
		+ COLUMN_ID + " integer primary key autoincrement, " 
		+ COLUMN_ALARM_ID + " integer not null CONSTRAINT fk_alarmMmeasurment_alarm REFERENCES " + AlarmTable.TABLE_ALARM + "(" + AlarmTable.COLUMN_ID  + "), " 
		+ COLUMN_MEASUREMENT_ID + " integer not null CONSTRAINT fk_alarmMmeasurment_measurment REFERENCES " + MeasurementTable.TABLE_MEASUREMENT + "(" + MeasurementTable.COLUMN_ID  + "), " 
		+ COLUMN_CREATED + " DATETIME DEFAULT CURRENT_TIMESTAMP"
		+ ");";

	public static void onCreate(SQLiteDatabase database) {
		database.execSQL(DATABASE_CREATE);
	}

	public static void onUpgrade(SQLiteDatabase database, int oldVersion,int newVersion) {
		// schema changes go here
	}	

}
