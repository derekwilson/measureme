package net.derekwilson.measureme.data.schema;

import android.database.sqlite.SQLiteDatabase;

public class MeasurementScaleTable {
	// Database table
	public static final String TABLE_MEASUREMENT_SCALE = "measurementScale";
	public static final String COLUMN_ID = "_id";
	public static final String COLUMN_MEASUREMENT_ID = "measurementId";
	public static final String COLUMN_DISPLAY = "displayText";
	public static final String COLUMN_VALUE = "value";
	public static final String COLUMN_ORDER = "displayOrder";
	public static final String COLUMN_CREATED = "created";


	// Database creation SQL statement
	private static final String DATABASE_CREATE = "create table " 
		+ TABLE_MEASUREMENT_SCALE
		+ "(" 
		+ COLUMN_ID + " integer primary key autoincrement, " 
		+ COLUMN_MEASUREMENT_ID + " integer not null CONSTRAINT fk_measurmentScale_measurment REFERENCES " + MeasurementTable.TABLE_MEASUREMENT + "(" + MeasurementTable.COLUMN_ID  + "), " 
		+ COLUMN_DISPLAY + " varchar(255) not null," 
		+ COLUMN_VALUE + " int not null," 
		+ COLUMN_ORDER + " int not null," 
		+ COLUMN_CREATED + " DATETIME DEFAULT CURRENT_TIMESTAMP"
		+ ");";

	public static void onCreate(SQLiteDatabase database) {
		database.execSQL(DATABASE_CREATE);
	}

	public static void onUpgrade(SQLiteDatabase database, int oldVersion,int newVersion) {
		// schema changes go here
	}	

	public static void onInitialiseData(SQLiteDatabase database) {
		// pain
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(1,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.PAIN_MEASURE +"')," +
						"'No pain',0,0)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE + 
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(2,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.PAIN_MEASURE +"')," +
						"'Mild pain I can cope with',1,1)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE + 
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(3,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.PAIN_MEASURE +"')," +
						"'Moderate pain that affects my life',2,2)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE + 
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(4,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.PAIN_MEASURE +"')," +
						"'Severe disabling pain',3,3)");
		
		// energy
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(5,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.ENERGY_MEASURE +"')," +
						"'Sleepy / Sluggish',0,0)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(6,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.ENERGY_MEASURE +"')," +
						"'Just right',1,1)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(7,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.ENERGY_MEASURE +"')," +
						"'Wired but coping',2,2)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(8,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.ENERGY_MEASURE +"')," +
						"'Overwhelmed',3,3)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(9,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.ENERGY_MEASURE +"')," +
						"'Shut down',4,4)");

		// falling asleep
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(10,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.FALLING_ASLEEP_MEASURE +"')," +
						"'Immediate',0,0)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(11,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.FALLING_ASLEEP_MEASURE +"')," +
						"'Within 20 Minutes',1,1)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(12,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.FALLING_ASLEEP_MEASURE +"')," +
						"'Up to an hour',2,2)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(13,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.FALLING_ASLEEP_MEASURE +"')," +
						"'Over an hour',3,3)");

		// sleep
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(14,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.SLEEP_MEASURE +"')," +
						"'Continuous',0,0)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(15,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.SLEEP_MEASURE +"')," +
						"'Interrupted once',1,1)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(16,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.SLEEP_MEASURE +"')," +
						"'Interrupted twice',2,2)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(17,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.SLEEP_MEASURE +"')," +
						"'Interrupted three times',3,3)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(18,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.SLEEP_MEASURE +"')," +
						"'More interrupted',4,4)");

		// wake up
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(19,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.WAKE_UP_MEASURE +"')," +
						"'Run over by a train',0,0)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(20,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.WAKE_UP_MEASURE +"')," +
						"'Groggy slow to wake up',1,1)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(21,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.WAKE_UP_MEASURE +"')," +
						"'Rested & refreshed',2,2)");

		// pain NRS 11
		// pain
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(22,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.PAIN_MEASURE_NRS11 +"')," +
						"'0, No pain',0,0)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(23,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.PAIN_MEASURE_NRS11 +"')," +
						"'1, Mild pain, nagging',1,1)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(24,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.PAIN_MEASURE_NRS11 +"')," +
						"'2, Mild pain, nagging',2,2)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(25,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.PAIN_MEASURE_NRS11 +"')," +
						"'3, Mild pain, nagging',3,3)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(26,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.PAIN_MEASURE_NRS11 +"')," +
						"'4, Moderate pain, interferes',4,4)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(27,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.PAIN_MEASURE_NRS11 +"')," +
						"'5, Moderate pain, interferes',5,5)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(28,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.PAIN_MEASURE_NRS11 +"')," +
						"'6, Moderate pain, interferes',6,6)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(29,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.PAIN_MEASURE_NRS11 +"')," +
						"'7, Severe pain, disabling',7,7)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(30,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.PAIN_MEASURE_NRS11 +"')," +
						"'8, Severe pain, disabling',8,8)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(31,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.PAIN_MEASURE_NRS11 +"')," +
						"'9, Severe pain, disabling',9,9)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(32,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.PAIN_MEASURE_NRS11 +"')," +
						"'10, Severe pain, disabling',10,10)");

		// Units Consumed
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(33,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.UNITS_MEASURE +"')," +
						"'0',0,0)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(34,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.UNITS_MEASURE +"')," +
						"'0.5',1,1)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(35,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.UNITS_MEASURE +"')," +
						"'1',2,2)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(36,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.UNITS_MEASURE +"')," +
						"'1.5',3,3)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(37,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.UNITS_MEASURE +"')," +
						"'2',4,4)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(38,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.UNITS_MEASURE +"')," +
						"'2.5',5,5)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(39,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.UNITS_MEASURE +"')," +
						"'3',6,6)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(40,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.UNITS_MEASURE +"')," +
						"'3.5',7,7)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(41,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.UNITS_MEASURE +"')," +
						"'4',8,8)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(42,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.UNITS_MEASURE +"')," +
						"'4.5',9,9)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(43,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.UNITS_MEASURE +"')," +
						"'5',10,10)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(44,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.UNITS_MEASURE +"')," +
						"'5.5',11,11)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(45,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.UNITS_MEASURE +"')," +
						"'6',12,12)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(46,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.UNITS_MEASURE +"')," +
						"'6.5',13,13)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(47,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.UNITS_MEASURE +"')," +
						"'7',14,14)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(48,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.UNITS_MEASURE +"')," +
						"'7.5',15,15)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(49,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.UNITS_MEASURE +"')," +
						"'8',16,16)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(50,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.UNITS_MEASURE +"')," +
						"'8.5',17,17)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(51,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.UNITS_MEASURE +"')," +
						"'9',18,18)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(52,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.UNITS_MEASURE +"')," +
						"'9.5',19,19)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(53,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.UNITS_MEASURE +"')," +
						"'10',20,20)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(54,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.UNITS_MEASURE +"')," +
						"'11',22,21)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(55,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.UNITS_MEASURE +"')," +
						"'12',24,22)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(56,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.UNITS_MEASURE +"')," +
						"'13',26,23)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(57,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.UNITS_MEASURE +"')," +
						"'14',28,24)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(58,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.UNITS_MEASURE +"')," +
						"'15',30,25)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(59,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.UNITS_MEASURE +"')," +
						"'16',32,26)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(60,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.UNITS_MEASURE +"')," +
						"'17',34,27)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(61,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.UNITS_MEASURE +"')," +
						"'18',36,28)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(62,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.UNITS_MEASURE +"')," +
						"'19',38,29)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(63,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.UNITS_MEASURE +"')," +
						"'20+',40,30)");

		// SWL
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(64,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.SWL_MEASURE +"')," +
						"'In most ways my life is close to my ideal.',7,0)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(65,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.SWL_MEASURE +"')," +
						"'The conditions of my life are excellent.',7,1)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(66,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.SWL_MEASURE +"')," +
						"'I am satisfied with my life.',7,2)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(67,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.SWL_MEASURE +"')," +
						"'So far I have gotten the important things I want in life.',7,3)");
		database.execSQL("INSERT or REPLACE INTO " + 
				TABLE_MEASUREMENT_SCALE +
					"(" + COLUMN_ID + "," + COLUMN_MEASUREMENT_ID + "," + COLUMN_DISPLAY + "," + COLUMN_VALUE + "," + COLUMN_ORDER + ")" +
					" VALUES(68,(select " + MeasurementTable.COLUMN_ID + " from " + MeasurementTable.TABLE_MEASUREMENT + 
					" where " + MeasurementTable.COLUMN_NAME + " = '" + MeasurementTable.SWL_MEASURE +"')," +
						"'If I could live my life over, I would change almost nothing.',7,4)");
	}	
}
