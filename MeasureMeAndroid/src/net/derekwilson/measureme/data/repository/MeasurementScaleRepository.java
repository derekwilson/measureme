package net.derekwilson.measureme.data.repository;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.database.Cursor;

import com.google.inject.Inject;

import net.derekwilson.measureme.data.interfaces.IMeasurementScaleRepository;
import net.derekwilson.measureme.data.schema.MeasurementScaleTable;
import net.derekwilson.measureme.model.MeasurementScaleValue;

public class MeasurementScaleRepository extends BaseRepository implements IMeasurementScaleRepository {

	private String[] allColumns = {
			MeasurementScaleTable.COLUMN_ID,
			MeasurementScaleTable.COLUMN_DISPLAY,
			MeasurementScaleTable.COLUMN_VALUE,
			MeasurementScaleTable.COLUMN_ORDER
			};

	@Inject
	public MeasurementScaleRepository(Context context) {
		super(context);
	}

	@Override
	public MeasurementScaleValue create(MeasurementScaleValue newItem) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public MeasurementScaleValue update(MeasurementScaleValue item) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void delete(MeasurementScaleValue item) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteById(long id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteAll() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public MeasurementScaleValue getById(long id) {
		MeasurementScaleValue scaleValue = null;
		Cursor cursor = getDatabase().query(MeasurementScaleTable.TABLE_MEASUREMENT_SCALE, allColumns, MeasurementScaleTable.COLUMN_ID + " = " + id, null, null, null, null);
		try {
			cursor.moveToFirst();
			scaleValue = cursorToScaleValue(cursor);
		}
		finally {
			if (cursor != null) {
				cursor.close();
			}
		}
		return scaleValue;
	}

	@Override
	public List<MeasurementScaleValue> getAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<MeasurementScaleValue> getByMeasurementId(long id) {
		List<MeasurementScaleValue> scaleValues = new ArrayList<MeasurementScaleValue>();
		Cursor cursor = getDatabase().query(MeasurementScaleTable.TABLE_MEASUREMENT_SCALE, allColumns, MeasurementScaleTable.COLUMN_MEASUREMENT_ID + " = " + id, null, null, null, null);
		try {
			cursor.moveToFirst();
			while (!cursor.isAfterLast()) {
				MeasurementScaleValue scaleValue = cursorToScaleValue(cursor);
				scaleValues.add(scaleValue);
				cursor.moveToNext();
			}
		}
		finally {
			if (cursor != null) {
				cursor.close();
			}
		}
		return scaleValues;
	}

	private MeasurementScaleValue cursorToScaleValue(Cursor cursor) {
		MeasurementScaleValue scaleValue = new MeasurementScaleValue();
		scaleValue.setId(cursor.getInt((cursor.getColumnIndex(MeasurementScaleTable.COLUMN_ID))));
		scaleValue.setDisplayText(cursor.getString(cursor.getColumnIndex(MeasurementScaleTable.COLUMN_DISPLAY)));
		scaleValue.setValue(cursor.getInt(cursor.getColumnIndex(MeasurementScaleTable.COLUMN_VALUE)));
		scaleValue.setDisplayOrder(cursor.getInt(cursor.getColumnIndex(MeasurementScaleTable.COLUMN_ORDER)));
		return scaleValue;
	}
}
