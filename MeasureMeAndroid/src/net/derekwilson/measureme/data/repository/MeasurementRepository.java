package net.derekwilson.measureme.data.repository;

import java.util.ArrayList;
import java.util.List;

import com.google.inject.Inject;

import android.content.Context;
import android.database.Cursor;

import net.derekwilson.measureme.data.interfaces.IMeasurementRepository;
import net.derekwilson.measureme.data.interfaces.IMeasurementScaleRepository;
import net.derekwilson.measureme.data.schema.MeasurementTable;
import net.derekwilson.measureme.model.DisplayValuePairs;
import net.derekwilson.measureme.model.Measurement;

public class MeasurementRepository extends BaseRepository implements IMeasurementRepository {

	@Inject
    private IMeasurementScaleRepository measurementScaleRepo;

	private String[] allColumns = {
			MeasurementTable.COLUMN_ID,
			MeasurementTable.COLUMN_NAME,
			MeasurementTable.COLUMN_EDITABLE,
			MeasurementTable.COLUMN_RECORD_MISSED,
			MeasurementTable.COLUMN_RENDER_TYPE
			};

	@Inject
	public MeasurementRepository(Context context) {
		super(context);
	}

	@Override
	public Measurement create(Measurement newItem) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Measurement update(Measurement item) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void delete(Measurement item) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteById(long id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteAll() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Measurement getById(long id, boolean getScale) {
		Measurement measurement = null;
		Cursor cursor = getDatabase().query(MeasurementTable.TABLE_MEASUREMENT, allColumns, MeasurementTable.COLUMN_ID + " = " + id, null, null, null, null);
		try {
			cursor.moveToFirst();
			measurement = cursorToMeasurement(cursor);
		}
		finally {
			if (cursor != null) {
				cursor.close();
			}
		}

		if (getScale) {
			measurement.setScale(measurementScaleRepo.getByMeasurementId(measurement.getId()));
		}
		
		return measurement;
	}

	@Override
	public Measurement getById(long id) {
		return getById(id, true);
	}

	@Override
	public List<Measurement> getAll() {
		List<Measurement> measures = new ArrayList<Measurement>();

		Cursor cursor = getDatabase().query(MeasurementTable.TABLE_MEASUREMENT, allColumns, null, null, null, null,MeasurementTable.COLUMN_NAME);

		try {
			cursor.moveToFirst();
			while (!cursor.isAfterLast()) {
				Measurement measure = cursorToMeasurement(cursor);
				measures.add(measure);
				cursor.moveToNext();
			}
		}
		finally {
			// make sure to close the cursor
			if (cursor != null) {
				cursor.close();
			}
		}
		
		// note that we do not get the scales when we get all the measures
		return measures;
	}

	public DisplayValuePairs getAllDisplayValuePairs() {
		DisplayValuePairs pairs = new DisplayValuePairs();
		List<String> display = new ArrayList<String>();
		List<String> value = new ArrayList<String>();

		Cursor cursor = getDatabase().query(MeasurementTable.TABLE_MEASUREMENT, allColumns, null, null, null, null,MeasurementTable.COLUMN_NAME);
		
		try {
			cursor.moveToFirst();
			while (!cursor.isAfterLast()) {
				display.add(cursor.getString((cursor.getColumnIndex(MeasurementTable.COLUMN_NAME))));
				value.add(cursor.getString((cursor.getColumnIndex(MeasurementTable.COLUMN_ID))));
				cursor.moveToNext();
			}
		}
		finally {
			// make sure to close the cursor
			if (cursor != null) {
				cursor.close();
			}
		}

		pairs.setDisplay(display);
		pairs.setValue(value);
		return pairs;
	}
	
	private Measurement cursorToMeasurement(Cursor cursor) {
		Measurement measurement = new Measurement();
		measurement.setId(cursor.getInt((cursor.getColumnIndex(MeasurementTable.COLUMN_ID))));
		measurement.setName(cursor.getString((cursor.getColumnIndex(MeasurementTable.COLUMN_NAME))));
		measurement.setEditable((cursor.getInt((cursor.getColumnIndex(MeasurementTable.COLUMN_EDITABLE))) == 0 ? false : true ));
		measurement.setRecordMissed((cursor.getInt((cursor.getColumnIndex(MeasurementTable.COLUMN_RECORD_MISSED))) == 0 ? false : true ));
		int type = cursor.getInt((cursor.getColumnIndex(MeasurementTable.COLUMN_RENDER_TYPE)));
		Measurement.RenderType renderType = Measurement.RenderType.SingleColumn;
		switch (type) {
		case 1:
			renderType = Measurement.RenderType.TwoColumn;
			break;
		case 2:
			renderType = Measurement.RenderType.Slider;
			break;
		}
		measurement.setRenderType(renderType);
		return measurement;
	}
}
