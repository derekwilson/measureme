package net.derekwilson.measureme.data.repository;

import com.google.inject.Inject;

import net.derekwilson.measureme.MeasureMe;
import net.derekwilson.measureme.data.interfaces.IBaseRepository;
import net.derekwilson.measureme.data.interfaces.IRepositoryAction;
import net.derekwilson.measureme.logging.ILoggerFactory;
import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public abstract class BaseRepository implements IBaseRepository {
	@Inject
    protected ILoggerFactory _logger;

	@Inject
	public BaseRepository(Context context) {
	}

	protected SQLiteDatabase getDatabase() {
		return MeasureMe.getDatabaseHelper().getDatabase();
	}
	
	public void open() throws SQLException {
		MeasureMe.getDatabaseHelper().open();
	}

	public void close() {
		MeasureMe.getDatabaseHelper().close();
	}

	// lack of generic delegates means that this function must be declared here rather than in the base class
	public void executeInOpenContext(IRepositoryAction action) {
		MeasureMe.getDatabaseHelper().executeInOpenContext(action);
	}
}

