package net.derekwilson.measureme.utility;

import net.derekwilson.measureme.AlarmService;
import net.derekwilson.measureme.activity.main.MainActivity;
import net.derekwilson.measureme.logging.ILoggerFactory;
import com.google.inject.Inject;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Vibrator;
import android.preference.PreferenceManager;

public class PreferencesHelper implements IPreferencesHelper {
	public static final String PREFS_ALARMS_ENABLED = "prefs_alarms_enabled";
	public static final String PREFS_ALARM_VIBRATE_ENABLED = "prefs_alarm_vibrate_enabled";
	public static final String PREFS_ALARM_SOUND_ENABLED = "prefs_alarm_sound_enabled";
	public static final String PREFS_RINGTONE = "prefs_ringtone";
	public static final String PREFS_VERSION = "prefs_version";

	@Inject
    private ILoggerFactory logger;

	@Inject
	private Vibrator vibrator;

	@Inject
	private SharedPreferences sharedPrefs;

	// this is the application context
	@Inject
	private Context context;

	@Inject
	public PreferencesHelper() {
	}

	public void RefreshAllAlarmsInAlarmManager() {
		boolean alarmsEnabled = getAlarmsEnabled();
		logger.getCurrentApplicationLogger().debug("Prefs: Alarms enabled {}",alarmsEnabled);

		SetupAlarmInAlarmManager((long)-1,alarmsEnabled);
	}
	
	public void SetupAlarmInAlarmManager(long alarmId) {
		boolean alarmsEnabled = getAlarmsEnabled();
		SetupAlarmInAlarmManager(alarmId,alarmsEnabled);
	}
	
	public void SetupAlarmInAlarmManager(long alarmId, boolean enabled) {
    	Intent operation = new Intent(this.context, AlarmService.class);
    	operation.putExtra(MainActivity.ACTIVITY_PARAM_ALARM_ID,(alarmId));
		if (enabled) {
	    	operation.setAction(AlarmService.CREATE);
	    } else {
	    	operation.setAction(AlarmService.CANCEL);
	    }
        context.startService(operation);
	}
	
	public boolean getAlarmsEnabled() {
        return sharedPrefs.getBoolean(PREFS_ALARMS_ENABLED,true);
    }     

	public boolean getAlarmSoundEnabled() {
        return sharedPrefs.getBoolean(PREFS_ALARM_SOUND_ENABLED,true);
    }     

	public boolean getAlarmVibrateEnabled() {
        return sharedPrefs.getBoolean(PREFS_ALARM_VIBRATE_ENABLED,true);
    }

	public void AlarmVibrate() {
		if (getAlarmVibrateEnabled()) {
			vibrator.vibrate(1000);
		}
	}

	public Uri getRingtone() {
		String strRingtonePreference = sharedPrefs.getString(PREFS_RINGTONE, "DEFAULT_SOUND");        
		return Uri.parse(strRingtonePreference);
	}
	
	public Ringtone getAlarmSound(boolean ignoreEnabled) {
		if (getAlarmSoundEnabled() || ignoreEnabled) {
			Uri notification = getRingtone();
			if (notification == null) {
				notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
			}
			logger.getCurrentApplicationLogger().debug("Prefs: sound: {}",notification.toString());
			Ringtone r = RingtoneManager.getRingtone(context, notification);
			if (r == null) {
				logger.getCurrentApplicationLogger().warn("Cannot play sound {}",notification.toString());
			}
			return r;
		}
		return null;
	}
}
