package net.derekwilson.measureme.utility;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import net.derekwilson.measureme.MeasureMe;
import net.derekwilson.measureme.data.interfaces.IRecordedMeasurementWriter;
import net.derekwilson.measureme.logging.ILoggerFactory;
import net.derekwilson.measureme.model.Measurement;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.widget.Toast;

import com.google.inject.Inject;

public class LogSender implements ILogSender {
	public class Result {
		public int NumberOfLogs;
		public Intent SendIntent;
	}
	
	@Inject
    private ILoggerFactory logger;

	@Inject
	private IRecordedMeasurementWriter measurementWriter;

	@Inject
	private Context context;

	@Inject
	public LogSender() {
	}

	public Result generateSendLogfilesForMeasurementsIntent(List<Measurement> measurements, String title) {
		Result result = new Result();
		result.NumberOfLogs = 0;

		if (measurements != null && measurements.size() > 0) {
			
			ArrayList<Uri> attachmentUris = new ArrayList<Uri>();
			for (Measurement measure : measurements) {
				String filename = measurementWriter.getFileFullPathname(measure);
				File recordingFile = new File(filename);
				logger.getCurrentApplicationLogger().debug("SendLog attaching file {}, exists {}", filename, recordingFile.exists());
				if (recordingFile.exists()) {
					// we only want to attach it if it exists
					Uri fileURI = Uri.fromFile(recordingFile);
					attachmentUris.add(fileURI);
				}
			}

			result.NumberOfLogs = attachmentUris.size();
			
			if (result.NumberOfLogs > 0) {
				//Intent sharingIntent = new Intent(Intent.ACTION_SEND);
				Intent sharingIntent = new Intent(Intent.ACTION_SEND_MULTIPLE);
				sharingIntent.setType("vnd.android.cursor.dir/email");
				sharingIntent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, attachmentUris);
				//sharingIntent.putExtra(Intent.EXTRA_STREAM, fileURI);
				sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "MeasureMe Log Recording - " + title);
				sharingIntent.putExtra(Intent.EXTRA_TEXT, MeasureMe.getSystemInformation(context.getApplicationContext()));
				result.SendIntent = sharingIntent;
			}
		}
		
		return result;
	}
}
