package net.derekwilson.measureme.utility;

import java.util.List;

import net.derekwilson.measureme.model.Measurement;
import net.derekwilson.measureme.utility.LogSender.Result;

public interface ILogSender {
	Result generateSendLogfilesForMeasurementsIntent(List<Measurement> measurements, String title);
}
