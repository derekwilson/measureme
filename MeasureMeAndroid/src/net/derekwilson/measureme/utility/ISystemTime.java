package net.derekwilson.measureme.utility;

import java.util.Calendar;

public interface ISystemTime {
	Calendar getCurrentTime();
}
