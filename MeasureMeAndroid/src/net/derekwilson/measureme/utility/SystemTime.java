package net.derekwilson.measureme.utility;

import java.util.Calendar;

public class SystemTime implements ISystemTime {

	@Override
	public Calendar getCurrentTime() {
		return Calendar.getInstance();
	}

}
