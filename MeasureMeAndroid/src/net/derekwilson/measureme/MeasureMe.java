package net.derekwilson.measureme;

import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import roboguice.RoboGuice;

import net.derekwilson.measureme.data.MeasureMeDatabaseHelper;
import net.derekwilson.measureme.ioc.IoCModule;
import net.derekwilson.measureme.logging.SlfLoggerFactory;
import android.app.Application;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.preference.PreferenceManager;

public class MeasureMe extends Application {
	public static final String MEASUREME_FOLDER = "MeasureMe";
	
	private static MeasureMeDatabaseHelper _dbHelper; 
	
	// Deliberately not using IoC for this - as this class is where IoC is setup
	final private Logger _logger = LoggerFactory.getLogger(MeasureMe.class);

	@Override
    public void onCreate() {
		_logger.warn("Application started v{}, debug {}, production build {}", getVersionName(getApplicationContext()),BuildConfig.DEBUG,MeasureMeBuildConfig.PRODUCTION);

		super.onCreate();
		PreferenceManager.setDefaultValues(this, R.xml.activity_settings, false);
		RoboGuice.setBaseApplicationInjector(this, RoboGuice.DEFAULT_STAGE, RoboGuice.newDefaultRoboModule(this), new IoCModule());

		_logger.debug("Application IoC bound");
		
		_dbHelper = new MeasureMeDatabaseHelper(this, new SlfLoggerFactory());
	}

    @Override
    public void onTerminate() {
        super.onTerminate();
        _dbHelper = null;
    }

	public static MeasureMeDatabaseHelper getDatabaseHelper() {
		return _dbHelper;
	}

	public static String getVersionName(Context applicationContext) {
	    String versionName;
	    final PackageManager packageManager = applicationContext.getPackageManager();
	    if (packageManager != null) {
	        try {
	            PackageInfo packageInfo = packageManager.getPackageInfo(applicationContext.getPackageName(), 0);
	            versionName = packageInfo.versionName;
	        } catch (PackageManager.NameNotFoundException e) {
	            versionName = "UNKNOWN";
	        }
	        return versionName;
	    }
	    return "UNKNOWN";
	}
	
	public static String getSystemInformation(Context applicationContext) {
		StringBuilder sysInfo = new StringBuilder();
		sysInfo.append("\n\n")
		.append("App Version: ")
		.append(getVersionName(applicationContext)).append('\n')
		.append("System Information").append('\n')
		.append("Device model: ").append(android.os.Build.MODEL).append(" (").append(android.os.Build.DISPLAY).append(')').append('\n')
		.append("OS version: ").append(android.os.Build.VERSION.RELEASE).append(" (").append(android.os.Build.VERSION.INCREMENTAL).append(')').append('\n')
		.append("UI language: ").append(Locale.getDefault().getLanguage()).append('\n');
		return sysInfo.toString();
	}
}

