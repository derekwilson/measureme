package net.derekwilson.measureme;

import roboguice.receiver.RoboBroadcastReceiver;
import net.derekwilson.measureme.logging.ILoggerFactory;
import net.derekwilson.measureme.utility.IPreferencesHelper;
import com.google.inject.Inject;

import android.content.Context;
import android.content.Intent;

public class RebootAlarmSetter extends RoboBroadcastReceiver {

	@Inject
    private ILoggerFactory logger;

	@Inject
    private IPreferencesHelper preferences;
	
    // handleReceive is the RoboGuice equivalent of onReceive 
	@Override
    public void handleReceive(Context context, Intent intent) {
		logger.getCurrentApplicationLogger().debug("RebootAlarmSetter handleReceive");
		preferences.RefreshAllAlarmsInAlarmManager();
    }
}