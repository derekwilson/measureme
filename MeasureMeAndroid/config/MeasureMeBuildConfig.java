package net.derekwilson.measureme;

// WARNING templated file ONLY edit this file in the CONFIG folder    

public class MeasureMeBuildConfig {
    /** Whether or not to include logging statements in the application. */
    public final static boolean PRODUCTION = @CONFIG.PRODUCTION@;
}