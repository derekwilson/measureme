# MeasureMe Privacy Policy #

MeasureMe is an application written by Derek Wilson.

This policy describes the data that is collected by Derek Wilson.

## What information is collected? ##

None.

MeasureMe has no access to the internet connection on your phone. It is not possible for MeasureMe to collect and transmit any data.

I would not normally include a privacy policy however it is a requirement of being on the Google Play Store.


## How is this information processed? ##

No information is collected.


## How do I know that this is true? ##

MeasureMe is open source and this repository contains the source code for the application. You can check the code.

MeasureMe [manifest](https://bitbucket.org/derekwilson/measureme/src/master/MeasureMeAndroidStudio/app/src/main/AndroidManifest.xml?at=master&fileviewer=file-view-default) identifies all the permissions requested by the app.


